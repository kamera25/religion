﻿
*License of some source, images, models. Made by CrowSullCore( Hiroki Yamane, Takahiro Nakaoku) 

CrowSullCore's data licensed under CC BY-NC-SA 2.1 JP.
For more detail this license, please see follow a link.

http://creativecommons.org/licenses/by-nc-sa/2.1/jp/

If you want use on business, indies or dojin, please contact us.


*License of MMD4Mecanim.

Copyright (c) 2013-2015, Nora
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, 
  this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, 
  this list of conditions and the following disclaimer in the documentation 
  and/or other materials provided with the distribution.
* Neither the name of the Nora nor the　names of its contributors 
  may be used to endorse or promote products derived from this software 
  without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL NORA BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*License of CGTextures's textures. (Using some textures.)

CGTextures offers photographs of materials ("Textures") on its website (www.cgtextures.com) 
for game developers, special effects artists, graphic designers and other professions. No payment or
royalties are required to use these Textures. The use of Textures is non-exclusive, royalty free,
and you have the right to modify them for the uses permitted under the clause Conditions of Use.
Most Textures on the CGTextures website are photographed by CGTextures. A small part is photographed by
third party contributors ("Contributors"). CGTextures has separate agreements with these Contributors in which
they grant CGTextures permission to add their Textures to the website. All terms and conditions in this License
apply to both groups of Textures.

All Textures and materials remain in ownership of CGTextures or the Contributor and the Textures and materials are
licensed to you, not sold.

The license granted for the use of the textures gives you no lawful right to submit, use or 
otherwise make available the textures in Second Life or otherwise to Linden Research, Inc. 
Use of the textures in such manner will be an infringement of the intellectual property rights
if CGTextures and CGTextures reserves its right to take action to prevent any such infringement.


*License of MoonAndEarth.

https://github.com/keijiro/MoonAndEarth

The images of the Moon are made by John van Vliet. These images are provided under the Creative Commons license (cc-by-sa).
The images of the Earth are provided from NASA, so basically you can use these images freely.


*License of some sound.

SE - THE MATCH-MAKERS 2nd (http://osabisi.sakura.ne.jp/m2/)

Sound - Classical Music Sound Library (http://andotowa.quu.cc/)
Classical Music Sound Library by ANDO TOWA is licensed under a Creative Commons BY 2.1 JP License

