# Religion #

Religion はオーソドックスなオンラインTPSゲームです。主人公のヴィル(アンジェリカ・エトナー)を操作し、銃やグレネードを駆使して戦ってください。

### リポジトリについて ###

* 再配布禁止の素材以外、全てのゲームデータ入っています。
* ライセンスを遵守の上、利用してください。(License.txt 参照 / かなり複雑です。 / 商用利用は要相談)
* 開発中のゲームですので、予期しない動作をする可能性があります。
* 無保証です。利用の際にはご自身で責任を取ってご使用ください。

### どうやって開発するの？ ###

* Unity 5.0.0 で開発されています。それ未満のバージョンでは動作しません。
* 開発は Windos8.1 で行われています。(Mac では確認していません。)
* このリポジトリは Git で管理されています。Git クライアントを利用して、下記のようにクローンしてください。
```
	git clone git@bitbucket.org:kamera25/religion.git
```

### 手伝って頂ける方へ ###

* 申し訳ございませんが、Unity の仕様上 プルリクエスト は受け付けられません。
* 手伝いをして頂ける方は、ファイルを UnityPackage にしてお送りください。
* バグレポートや質問も受け付けています。以下のURLから送信してください。
https://bitbucket.org/kamera25/religion/issues?status=new&status=open

### 制作 ###

* Crow Sull Core 2006 - 2015
* 原作・原案・3Dモデル全般 [F19](http://serpentandf19.web.fc2.com/f19/top/no.nineteen_modeling_second_factory.html)
* プログラム・一部3Dモデル [kamera25](http://religion.indiesj.com/)
* 原画 mimoli

### お借りしたもの と ご紹介 ###
* [MMD4Mecanim](http://stereoarts.jp/) - Noraさん
* [Detonator Explosion Framework](http://u3d.as/content/ben-throop/detonator-explosion-framework/1qK) - Ben Throopさん
* [Photon Realtime](http://photoncloud.jp/) - GMOクラウドさん と ExitGamesさん
* [CGTextures](http://www.cgtextures.com/)
* [MoonAndEarth](https://github.com/keijiro/MoonAndEarth) - keijiroさん
* [ザ・マッチメイカァズ2nd](http://osabisi.sakura.ne.jp/m2/) - OSAさん
* [クラシック無料音楽配信](http://andotowa.quu.cc/) - ANDO TOWAさん
* [Unity-Excel-Importer-Maker](https://github.com/tsubaki/Unity-Excel-Importer-Maker) - tsubakiさん
* [和田研細丸ゴシック](http://sourceforge.jp/projects/jis2004/wiki/FrontPage) - rareearthさん