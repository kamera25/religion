﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum TYPEICON
{
    NONE,
    WEAPON_RECOVERY,
    HP_RECOVERY,
    UNLOCK
}

public enum CGUSETYPE
{
    NONE,
    RECOVERYCIRCLE
}

public class CommonGaugeController : MonoBehaviour 
{
    private GameObject commonGage;
    private RectTransform gaugeBar;
    private Image iconImage;
    private float dump = 6F;
    private CGUSETYPE useType = CGUSETYPE.NONE;

    public TYPEICON icon = TYPEICON.NONE;
    public float value = 0F;

	// Use this for initialization
	void Start () 
    {
        commonGage = GameObject.Find("CommonGauge");
        gaugeBar = commonGage.transform.FindChild("GaugeBar").GetComponent<RectTransform>();
        iconImage = commonGage.transform.FindChild("Icon").GetComponent<Image>();

        ChangeIcon(TYPEICON.WEAPON_RECOVERY);

        commonGage.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () 
    {
        Vector3 scale = gaugeBar.transform.localScale;
        scale.y = Mathf.Lerp( scale.y, Mathf.Clamp01( value), Time.deltaTime * dump);
        gaugeBar.transform.localScale = scale;
	}

    public void ChangeIcon( TYPEICON newIcon)
    {
        icon = newIcon;
        if (newIcon != TYPEICON.NONE)
        {
            Sprite sprite = Resources.Load<Sprite>("ingame/CommonIcon/" + newIcon.ToString());
            iconImage.sprite = sprite;
        }
    }

    public void UseGauge( CGUSETYPE gaugeType, float defaultScaleY)
    {
        if( useType != CGUSETYPE.NONE)
        {
            return;
        }

        useType = gaugeType;
        commonGage.SetActive(true);

        // Reset Y scale of a gauge.
        Vector3 scale = gaugeBar.transform.localScale;
        scale.y = Mathf.Clamp01(defaultScaleY);
        gaugeBar.transform.localScale = scale;
    }

    public void DropGauge( CGUSETYPE gaugeType)
    {
        if( useType != gaugeType)
        {
            return;
        }
        
        useType = CGUSETYPE.NONE;
        commonGage.SetActive(false);
    }
}
