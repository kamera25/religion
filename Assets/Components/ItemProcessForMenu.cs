﻿using UnityEngine;
using System.Collections;

public class ItemProcessForMenu : ItemDataProcess 
{

    protected string ItemKindName( ItemControl.ITEMKIND kind)
    {
        switch (kind)
        {
            case ItemControl.ITEMKIND.AMMO:
                return "Ammo/";
            case ItemControl.ITEMKIND.EQUIPMENT:
                return "Equipment/";
            case ItemControl.ITEMKIND.KEY:
                return "Key/";
            case ItemControl.ITEMKIND.WEAPON:
                return "";
            case ItemControl.ITEMKIND.RECOVERY:
                return "Recovery/";
        }
        
        return "";
    }
    
    protected string GetItemName( int itemID, ItemControl.ITEMKIND kind)
    {
        
        switch (kind)
        {
            case ItemControl.ITEMKIND.AMMO:
                return GetAmmoName( itemID);
            case ItemControl.ITEMKIND.EQUIPMENT:
                return GetEquipmentName( itemID);
            case ItemControl.ITEMKIND.KEY:
                return GetKeyName( itemID);
            case ItemControl.ITEMKIND.WEAPON:
                return GetWeaponName( itemID);
            case ItemControl.ITEMKIND.RECOVERY:
                return GetRecoveryName( itemID);
        }
        
        return "";
    }
    
    protected string GetItemDetail( int itemID, ItemControl.ITEMKIND kind)
    {
        switch (kind)
        {
            case ItemControl.ITEMKIND.RECOVERY:
                return GetRecoveryBrief( itemID);
            case ItemControl.ITEMKIND.WEAPON:
                return GetWeaponBrief( itemID);
        }
        
        return "";
    }


    protected Sprite GetItemSprite( int index, ItemControl itemCtr, ItemControl.ITEMKIND itemKind)
    {
        int wpIndex = itemCtr.GetListData(index, itemKind);
        string itemName = GetItemName( wpIndex, itemKind);

        return GetItemSpriteByName(itemName, itemKind);
    }

    protected Sprite GetItemSpriteByName( string name, ItemControl.ITEMKIND itemKind)
    {
        string fileName = ItemKindName(itemKind) + name + "/pict";

        return Resources.Load<Sprite>(fileName);
    }
}
