﻿using UnityEngine;
using System.Collections;

public enum SOUNDTYPE
{
    SE,
    BGM,
    SE_BATTLEFIELD
}

public class AudioSourceController : MonoBehaviour 
{
    public static int bgmVolume = 100;
    public static int seVolume = 100;

    private int beforeBgmVolume = -1;
    private int beforeSeVolume = -1;
    private AudioSource audioSource;
    private bool sleepSound = false;

    public SOUNDTYPE soundType;

    public static void InitializeASController()
    {
        //bgmVolume = PlayerPrefs.GetInt("BGMValue");
        //seVolume = PlayerPrefs.GetInt("SEValue");
    }

	// Use this for initialization
	void Start () 
    {
	    if (bgmVolume == 100)
        {
            AudioSourceController.InitializeASController();
        }

        audioSource = this.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if( Time.frameCount % 2 == 0)
        { 
            return; 
        }

        if (beforeSeVolume != AudioSourceController.seVolume || beforeBgmVolume != AudioSourceController.bgmVolume)
        {
            ChangeVolume();
        }


        beforeSeVolume = AudioSourceController.seVolume;
        beforeBgmVolume = AudioSourceController.bgmVolume;

        if (Time.timeScale == 0 && soundType == SOUNDTYPE.SE_BATTLEFIELD)
        {
            audioSource.volume = 0F;
            sleepSound = true;
        }

        if (Time.timeScale != 0 && sleepSound)
        {
            SetSE();
            sleepSound = false;
        }

	}

    void ChangeVolume()
    {
        if (audioSource == null)
        {
            return;
        }

        if (soundType == SOUNDTYPE.BGM)
        {
            audioSource.volume = (float)AudioSourceController.bgmVolume / 100F;
        }
        if (soundType == SOUNDTYPE.SE || soundType == SOUNDTYPE.SE_BATTLEFIELD)
        {
            SetSE();
        }
    }

    void SetSE()
    {
        audioSource.volume = (float)AudioSourceController.seVolume / 100F;
    }
}
