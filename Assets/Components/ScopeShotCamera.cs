﻿using UnityEngine;
using System.Collections;

public class ScopeShotCamera : Actor
{

	public Transform weapon;
	public Transform eyeCam;
    public bool usePlayerScope = false;

    private StatusControl statusCtr;
    private PlayerWeaponControl playerWpCtr;
	//private WeaponChangeControl wpChangeCtr;
	private float sensitivityX = 3F;
	private float sensitivityY = 0.4F;
	private Animator anim;
	private int beforeWeaponNo;

    public float defaultFogDist;

    void Awake()
    {
        defaultFogDist = RenderSettings.fogDensity;
    }

	void Start()
	{
        GameObject controller = GameObject.FindWithTag("GameController");
        playerWpCtr = controller.GetComponent<PlayerWeaponControl>();

		beforeWeaponNo = playerWpCtr.NowWeaponNo;

        anim = GetAnimator();

        statusCtr = this.gameObject.GetComponent<StatusControl>();
	}



	// Update is called once per frame
	void Update () 
	{

		/* Camera LookAt */
        if( Time.deltaTime != 0F && 0.2 < Time.timeScale && !statusCtr.isDead)
		{
			float moveCurX = Input.GetAxis("Mouse X") * sensitivityX;
			float rotationX = this.transform.localEulerAngles.y + moveCurX;

			this.transform.localEulerAngles = new Vector3(0, rotationX, 0);
			anim.SetFloat( "AimY", Mathf.Clamp( anim.GetFloat( "AimY") + Input.GetAxis("Mouse Y") * sensitivityY, -1, 1));
		}
		
	}

	void LateUpdate ()
	{		
		/* Set Camera if haven't weapon */
		if ( playerWpCtr.NowWeaponNo == 0 || usePlayerScope || anim.GetBool("isDash")) 
		{
			Camera.main.transform.position = eyeCam.transform.position;
			Camera.main.transform.rotation = eyeCam.transform.rotation;	
		}

		beforeWeaponNo = playerWpCtr.NowWeaponNo;
	}

	public void EnableUsePlayerScope()
	{
		usePlayerScope = true;
		return;
	}

	public void DisableUsePlayerScope()
	{
		usePlayerScope = false;
		return;
	}


	public void EnableScopeCamera()
	{
        ControlScopeCamera(true);
	}

	public void DisableScopeCamera()
	{
        ControlScopeCamera(false);

		// Reset camera's View of field. 
		Camera.main.fieldOfView = 60F;
	}

    private void ControlScopeCamera( bool isEnable)
    {
        // Traverse WeaponCameraControl
        foreach( Transform child in weapon)
        {
            WeaponCameraControl wpCamCtr = child.GetComponent<WeaponCameraControl>();
            
            if( wpCamCtr == null) continue;
            wpCamCtr.enabled = isEnable;
        }
        
        this.enabled = isEnable;
    }
}
