﻿using UnityEngine;
using System.Collections;

public class DamageBang : Photon.MonoBehaviour {
	
	public float MaxHP = 1000;
	public GameObject explosion = null;
	public GameObject breakItem = null;
	public bool isTimeBang = false;
	public bool isTouchBang = false;
	public float timeBangTime = 0;
    public int occurrence = 1;
	
    public float putBreakPosY = 3F;

	private float HP;
	private bool  isplayers = false;
	private float Itemdamage;
	
    private GameObject controller;
    private GameObject damageArea;

	void  Start ()
	{
        int ID;
        HP = MaxHP;
        DeleteCloneString();

        controller = GameObject.FindWithTag("GameController");
        WeaponDataProcess wpDataProc = controller.GetComponent<WeaponDataProcess>();

        ID = wpDataProc.FindIdFromWeaponName(this.name);
        if (ID != -1)
        {
            Itemdamage = wpDataProc.GetMinAttack( ID);
        } else
        {
            Itemdamage = 200;// Not such weapon of this name. so, Damage add Default Value. 
        }

        if (explosion != null)
        {
            damageArea = Resources.Load<GameObject>( "ingame/model/Damage_area");
        }
    }

	/* Delete '(Clone)' of "[Weapon](Clone)". */
	void  DeleteCloneString ()
	{
		int Index;
		
		Index = this.name.IndexOf("(");
		if( Index != -1)
		{
			this.name = this.name.Substring( 0, Index);
		}
	}
	
	void  Update ()
	{
		
		if( HP <= 0  || ( timeBangTime <= 0 && isTimeBang == true) )
		{	

            GameObject clone = null;
			/* Hit object Setting.*/
            if( explosion != null)
            {
                clone = Instantiate( damageArea, this.transform.position, Quaternion.identity) as GameObject;
			    clone.SendMessage( "SetDamage", Itemdamage);
            }

			if( breakItem != null)
			{
                for( int i = 0; i < occurrence; i++)
                {
				    clone = Instantiate( breakItem, this.transform.position + Vector3.up * putBreakPosY, Quaternion.identity) as GameObject;
				    clone.transform.localScale = this.transform.localScale;
				    clone.transform.rotation = this.transform.rotation;
			    }
            }
			if( explosion != null)
			{
                if( PhotonNetwork.offlineMode) Instantiate(explosion, this.transform.position, Quaternion.identity);
                else PhotonNetwork.Instantiate( "Explosion/" + explosion.name, this.transform.position, Quaternion.identity, 0);
            }

			
			if( isplayers == true)
			{
				controller.SendMessage("ResetC4Flag");
			}

            if( PhotonNetwork.offlineMode) Destroy( this.gameObject);
			else PhotonNetwork.Destroy(this.gameObject);
		}
		
		timeBangTime -= Time.deltaTime;
	}
	
	public void DamageReciver (  float damage  )
	{
		HP -= damage;
	}
	
	/* use c4(weapon), M14Claymore... */
	public void ForceBang ()
	{
		HP = 0;
	}

	void OnCollisionEnter( Collision col)
	{
		if( isTouchBang) ForceBang();
	}

	void  Set_isplayers ()
	{
		isplayers = true;
	}

}