using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum AMBIENT
{
	NONE = 0,
	PATROL,
	ANNIHILATION,
	RETURN,
	SURVIVE
}

public enum PLAN
{
	NONE = 0,
	COUNTER,
	ESCAPE,
    RETREAT,
	SEARCH,
	HIDE,
	WAIT,
	MOVE,
    STOP
}

public enum EXPLICIT
{
	NONE = 0,
	WALK,
	RUN,
	WEAPONATTACK,
	DAMAGERECIVE,
	FINDENEMY,
    ARRIVE_POINT,
    STOP
}

public class AIBehavior : MonoBehaviour 
{
    private Transform aimObject;
    private NavMeshAgent navAgent;
    private StatusControl statusCtrl;

	public List<AMBIENT> Ambient = new List<AMBIENT>();
	public List<PLAN> Reactive = new List<PLAN>();
	public List<EXPLICIT> Immediate = new List<EXPLICIT>();

    public List<Transform> PatrolRote = new List<Transform>();
    private GameObject player;


    // Following is parameta 
    public float eyeSight = 1000F;
    public bool noConter = false;
    [Range(0,180)]public float eyeAngle = 90F;

    [Range(0,100)] public int warLike = 0;
    [Range(0,100)] public int minutely = 0;
    [Range(0,100)] public int reckless = 0;

    void Awake()
    {
        aimObject = PatrolRote[0];
        navAgent = this.GetComponent<NavMeshAgent>();
    }

	// Use this for initialization
	void Start () 
	{
        statusCtrl = this.GetComponent<StatusControl>();
		
		player = GameObject.FindWithTag("Player");

		AIThink();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
		GatherInfomation();
		AIThink();
		InputContoroller();
        CheckDead();
	}

	void GatherInfomation()
	{
        if( isLook(player) && PeekAmbient() != AMBIENT.ANNIHILATION)
		{
			Immediate.Add(EXPLICIT.FINDENEMY);
		}
	}

	void AIThink()
	{
        /* *********************************** */
        /* Follow Process is in Ambient Relation.
        /* *********************************** */
		if( PeekAmbient() == AMBIENT.PATROL && PeekReactive() != PLAN.STOP)
		{
            PushReactive( PLAN.SEARCH);
		}

		if( PeekAmbient() == AMBIENT.RETURN)
		{
            PushReactive( PLAN.RETREAT);
		}

        /* ************************************* */
        /* Follow Process is in Reactive Relation.
        /* ************************************* */
        if (PeekReactive() == PLAN.COUNTER && PeekAmbient() != AMBIENT.ANNIHILATION)
        {
            PushAmbient(AMBIENT.ANNIHILATION);
        }

        if ( PeekReactive() == PLAN.SEARCH && PeekImmediate() == EXPLICIT.NONE)
        {
            PushImmidiate( EXPLICIT.WALK);
        }

        if (PeekReactive() == PLAN.RETREAT)
        {
            PushImmidiate( EXPLICIT.WALK);
            PushImmidiate( EXPLICIT.RUN);
        }

        if (PeekReactive() == PLAN.COUNTER && !noConter)
        {
            if (Random.Range(0, 100) < reckless && PeekImmediate() != EXPLICIT.RUN && isLook(player))
            {
                PushImmidiate( EXPLICIT.WEAPONATTACK);
            }
            else if( !isNear( 2000F))
            {
                PushImmidiate( EXPLICIT.RUN);
            }
        }



        /* ************************************* */
        /* Follow Process is in Immediate Relation.
        /* ************************************* */


        if( PeekImmediate() == EXPLICIT.DAMAGERECIVE)
        {
            PopImmidiate();
            PushAmbient( AMBIENT.ANNIHILATION);

            if( (float)(statusCtrl.hp) / (float)(statusCtrl.myData.maxhp) * 100 < warLike)
            {
                PushImmidiate(EXPLICIT.FINDENEMY);
            }
            else
            {
                PushReactive(PLAN.RETREAT);
            }
        }

        if( PeekImmediate() == EXPLICIT.FINDENEMY)
        {
            PopImmidiate();
            PushAmbient(AMBIENT.ANNIHILATION);
            PushReactive( PLAN.COUNTER);
        }

        // Fire weapon.
        if (PeekImmediate() == EXPLICIT.WEAPONATTACK && Random.Range(0, 5) == 1)
        {
            PopImmidiate();
        }

        // Stop running.
        if (PeekImmediate() == EXPLICIT.RUN && Random.Range(0, 30) == 1)
        {
            PopImmidiate();
        }

	}

	void InputContoroller()
	{
        if ( !navAgent.enabled)
        {
            navAgent.enabled = true;
        }

		if( PeekReactive() == PLAN.RETREAT)
		{
			navAgent.SetDestination( GameObject.FindGameObjectWithTag("HQPoint").transform.position);
		}

		if( PeekReactive() == PLAN.SEARCH )
		{
			navAgent.SetDestination( aimObject.position);
		}

		if( PeekReactive() == PLAN.COUNTER)
		{
			navAgent.SetDestination( player.transform.position);
		}

        if (PeekReactive() == PLAN.STOP)
        {
            navAgent.Stop();
        }
        

	}

	void OnTriggerEnter( Collider other)
	{
		if (!other.CompareTag("PatrolPoint") || PeekReactive() != PLAN.SEARCH)
        {
            return;
        }

        float pointDist = (this.transform.position - other.transform.position).sqrMagnitude;
		if (other.transform == aimObject && pointDist < 1000)
        {
            if (PatrolRote.Count <= 1)
            {
                PushImmidiate(EXPLICIT.ARRIVE_POINT);
            } 
            else
            {
                SetNextAimPoint(other.transform);
            }
        }

	}

    void SetNextAimPoint( Transform nowPointTrans)
    {

        bool nextUse = false;
        Transform useObj = null;
        
        // Search Next Step Distination.
        foreach( Transform point in PatrolRote)
        {
            if( nextUse)
            {
                useObj = point;
                break;
            }
            
            if( point == nowPointTrans)
            {
                nextUse = true;
                continue;
            }
        }


        // if Loop.
        if( useObj == null)
        {
            useObj = PatrolRote[0];
        }
        
        aimObject = useObj;
    }

    public void RandamSetNextAimPoint()
    {
        aimObject = GetRandamPatrolPoint();

        PopImmidiate();
    }

    public void RespawnRandamPositon()
    {
        navAgent.enabled = false;
        Vector3 pos = GetRandamPatrolPoint().position;
        this.transform.position = pos;
    }

    // Get transform from one of patrolpoints.
    Transform GetRandamPatrolPoint()
    {
        GameObject[] patrolPoints = GameObject.FindGameObjectsWithTag("PatrolPoint");
        
        int index = Random.Range( 0, patrolPoints.Count());
        
        return patrolPoints.ElementAt(index).transform;
    }

    public void RebornAI()
    {
        this.enabled = true;
        navAgent.enabled = true;
        PushAmbient(AMBIENT.PATROL);
    }

    public void StopAI()
    {
        PushReactive(PLAN.STOP);
    }

    public void RunAI()
    {
        if (PeekReactive() == PLAN.STOP)
        {
            PopReactive();
        }
    }

    void DamageReciver()
    {
        if (!noConter)
        {
            PushImmidiate(EXPLICIT.DAMAGERECIVE);
        }
    }

    /*
     * Boolean functions.
     */
	bool isLook( GameObject lookObj)
	{
        Vector3 eyeVec = lookObj.transform.position - this.transform.position;

		if( Vector3.Angle( eyeVec, this.transform.forward) < eyeAngle && eyeVec.sqrMagnitude < eyeSight)
		{
			return true;
		}

		return false;
	}

    bool isNear( float maxDist)
    {
        return Vector3.SqrMagnitude( player.transform.position - this.transform.position) < maxDist;
    }


    /* ************************ */
    /* Generic List Accsessors. */
    /* ************************ */
    public AMBIENT PeekAmbient()
	{
        if (Ambient.Count == 0)
        {
            return AMBIENT.NONE;
        }

		return Ambient [Ambient.Count - 1];
	}

	PLAN PeekReactive()
	{
        if (Reactive.Count == 0)
        {
            return PLAN.NONE;
        }

		return Reactive [Reactive.Count - 1];
	}

	public EXPLICIT PeekImmediate()
	{
        if (Immediate.Count == 0)
        {
            return EXPLICIT.NONE;
        }

		return Immediate [Immediate.Count - 1];
	}

    void PopAmbient()
    {
        Ambient.RemoveAt( Ambient.Count -1);
        return;
    }

    void PopReactive()
    {
        Reactive.RemoveAt( Reactive.Count - 1);
        return;
    }

    void PopImmidiate()
    {
        if (Immediate.Count != 0)
        {
            Immediate.RemoveAt(Immediate.Count - 1);
        }
        return;
    }

    void PopAllThink()
    {
        Ambient.Clear();
        Reactive.Clear();
        Immediate.Clear();
    }

    void PushAmbient( AMBIENT amb)
    {
        if( PeekAmbient() == amb) return;
        Ambient.Add( amb);
    }

    void PushReactive( PLAN plan)
    {
        if( PeekReactive() == plan) return;
        Reactive.Add(plan);
    }

    void PushImmidiate( EXPLICIT exp)
    {
        if (PeekImmediate() == exp) return;
        Immediate.Add(exp);
    }


    /* END */

    void CheckDead()
    {
        if (statusCtrl.isDead)
        {
            this.enabled = false;
            navAgent.enabled = false;
            PopAllThink();
        }
    }

}
