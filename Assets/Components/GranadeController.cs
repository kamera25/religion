﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class GranadeController : MonoBehaviour 
{

    private OrditGuideController orditCtl;
    private float waitTime = 1.6F;
    private bool modelHide = false;
    public GameObject granadeModel;

    // 
    // Follow process is related an ordit guide.
    // 

    public void InvisibleOrdit()
    {
        GetOrditController();

        orditCtl.DisableSprites();
        Invoke("VisibleOrdit", waitTime);
    }

    private void VisibleOrdit()
    {
        orditCtl.EnableSprites();
    }

    public void SetThrowPosture(bool isUp)
    {
        GetOrditController();

        if (isUp)
        {
            orditCtl.gravity = 0.13F;
            orditCtl.posYDumping = 1.9F;
            orditCtl.xyPeriod = 5.5F;
        } 
        else
        {
            orditCtl.gravity = -0.02F;
            orditCtl.posYDumping = 0F;
            orditCtl.xyPeriod = 2.0F;
        }
    }

    private void GetOrditController()
    {
        if (orditCtl == null)
        {
            orditCtl = this.transform.FindChild("OrditGuide").GetComponent<OrditGuideController>();
        }
    }

    public void HideGrenade()
    {
        if( !modelHide)
        {
            granadeModel.SetActive(false);
            modelHide = true;
        }
    }
	
}
