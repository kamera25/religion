﻿using UnityEngine;
using System.Collections;

public class ChangeWeaponMenuExitButton : MonoBehaviour 
{
    [SerializeField] GameObject changeWeaponMenuUI;


    public bool isDestory; 

	public void PushExitButton()
    {
        if (isDestory)
        {
            GameObject.Destroy(changeWeaponMenuUI);
        }
    }
}
