﻿using UnityEngine;
using System.Collections;

public class RPG7BulletBehavior : Photon.MonoBehaviour
{

    private WeaponControl wpCtr;

	// Use this for initialization
	void Start () 
    {
        if( !photonView.isMine && !PhotonNetwork.offlineMode)
        {
            this.enabled = false;
            return;
        }

        wpCtr = GameObject.FindWithTag("GameController").GetComponent<WeaponControl>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if ( wpCtr.IsLeftAmmo() && wpCtr.IsWeaponUse())
        {
            this.GetComponent<Renderer>().enabled = true;
        } 
        else
        {
            this.GetComponent<Renderer>().enabled = false;
        }
	}
}
