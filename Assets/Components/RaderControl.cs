﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class RaderElement
{
	public RectTransform dotChip;
	public Transform originObjTransform = null;
	public Vector3 position;
}

public enum RADERELEMENT
{
	NONE = 0,
	BASECAMP_BLUE,
	BASECAMP_RED,
	BASECAMP_GREEN,
	BASECAMP_YELLOW
}


public class RaderControl : MonoBehaviour 
{
   
	public List<RaderElement> reactThingsList = new List<RaderElement>();
    public STAGE nowStage;
    public RectTransform newsTrans;
    public RectTransform mapTrans;
    public RectTransform zeroTran;

    public float mapSize = 800F;

    const float markDump = 0.55F;

	void Start ()
	{
        LoadMapData();
		//RegisterElementWithRader( trans, RADERELEMENT.NONE);
	}

    void Update()
    {
        RotateDirectionImage();
        PutReaderElements();
        MoveMapImage();
    }

    void RotateDirectionImage()
    {
        float angleY = Camera.main.transform.localEulerAngles.y;
        Vector3 q = newsTrans.localEulerAngles;
        q.z = angleY;
        newsTrans.localEulerAngles = q;
    }

    void MoveMapImage()
    {

        //Camera.main
        Vector2 pivot = new Vector2( Camera.main.transform.position.x / mapSize, Camera.main.transform.position.z / mapSize);
        mapTrans.pivot = pivot;

        float angleY = Camera.main.transform.localEulerAngles.y;
        Vector3 q = mapTrans.localEulerAngles;
        q.z = angleY;
        mapTrans.localEulerAngles = q;

    }



    void PutReaderElements()
    {
        RaderElement[] elements = reactThingsList
                                    .Where(element => element.originObjTransform != null)
                                    .ToArray();

        foreach( RaderElement ele in elements)
        {
                NormalizeCircle();
                ele.dotChip.position = new Vector3( zeroTran.position.x + ele.originObjTransform.position.x * markDump, zeroTran.position.y + ele.originObjTransform.position.z * markDump, 0);
        }
    }



	public void RegisterElementWithRader( Transform trans, RADERELEMENT element)
	{
		RaderElement rds = new RaderElement();
		rds.originObjTransform = trans;
        rds.dotChip = GetRaderIcon(element, trans.position);
		
		reactThingsList.Add( rds);

		return;
	}

    public void RegisterElementWithRaderAsVec3( Vector3 pos, RADERELEMENT element)
	{
		RaderElement rds = new RaderElement();
		rds.position = pos;
        rds.dotChip = GetRaderIcon(element, pos);

		reactThingsList.Add( rds);

		return;
	}

    RectTransform GetRaderIcon( RADERELEMENT element, Vector3 pos)
    {
        GameObject obj = Instantiate<GameObject>( Resources.Load<GameObject>( "ingame/RaderIcon/" + element.ToString()));
        RectTransform rct = obj.GetComponent<RectTransform>();

        NormalizeCircle();

        rct.SetParent(mapTrans);
        rct.position = new Vector3( zeroTran.position.x + pos.x * markDump, zeroTran.position.y + pos.z * markDump, 0);
        
        return rct;
    }

    void NormalizeCircle()
    {
        Vector3 q = mapTrans.localEulerAngles;
        q.z = 0F;
        mapTrans.localEulerAngles = q;

    }

    void LoadMapData()
    {
        GameObject obj = Instantiate<GameObject>( Resources.Load<GameObject>( "Map/" + nowStage.ToString() + "Map"));
        RectTransform rct = obj.GetComponent<RectTransform>();

        rct.SetParent(mapTrans);
    }

	void  TraverseEnemy ()
	{
		
	}
	
}
