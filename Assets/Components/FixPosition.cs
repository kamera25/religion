﻿using UnityEngine;
using System.Collections;

public class FixPosition : MonoBehaviour 
{


	void LateUpdate()
	{
		this.transform.position = this.transform.parent.transform.position + Vector3.down * 0.3f;
        this.transform.rotation = this.transform.parent.transform.rotation;
	}
}
