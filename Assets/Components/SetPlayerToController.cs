﻿using UnityEngine;
using System.Collections;

/* !!! THIS SCRIPT SHOULD SET DOWN OPERATION ORDER !!! (otherwise make bug.) */

public class PlayerData
{
    public GameObject player;
    public Animator anim;
}

public class SetPlayerToController : Actor
{
   
    private GameObject controller;
    private StatusControl statusCtl;
    private PlayerMoveControl plyMoveCtr;
    private bool isdeadVoicePlay = false;

    private CharactorYoke.STATE beforeState;

    [SerializeField] AudioSource mouthAS;
    [SerializeField] AudioClip damageVoice;
    [SerializeField] AudioClip deadVoice;
    [SerializeField] AudioClip jumpVoice;
    [SerializeField] AudioClip ladderUpVoice;

	// Use this for initialization
	void Start () 
    {
        statusCtl = this.GetComponent<StatusControl>();

		if( !PhotonView.Get(this).isMine && !PhotonNetwork.offlineMode)
		{
			this.enabled = false;
			return;
		}

        controller = GameObject.FindWithTag("GameController");

        // Send player data to GameController.
        PlayerData data = new PlayerData();
        data.player = this.gameObject;
        data.anim = GetAnimator();
        controller.SendMessage( "RegisterPlayer", data);


        plyMoveCtr = this.GetComponent<PlayerMoveControl>(); 

        beforeState = plyMoveCtr.state;
	}

    void Update()
    {

        // Play charactor voice when jump.
        if ( beforeState == CharactorYoke.STATE.RUN && plyMoveCtr.state == CharactorYoke.STATE.AIR)
        {
            mouthAS.PlayOneShot(jumpVoice);
        }

        if ( CheckNowStateIsNew(CharactorYoke.STATE.ONLADDER) || CheckNowStateIsNew(CharactorYoke.STATE.FINISHLADDER))
        {
            mouthAS.PlayOneShot(ladderUpVoice);
        }


        beforeState = plyMoveCtr.state;
    }

    bool CheckNowStateIsNew( CharactorYoke.STATE state)
    {
        return beforeState != state && plyMoveCtr.state == state;
    }

    void DamageReciver( float damage)
    {
        if (damageVoice == null)
            return;

        if ( statusCtl.isDead)
        {
            if( !isdeadVoicePlay)
            {
                mouthAS.PlayOneShot(deadVoice);
                isdeadVoicePlay = true;
            }
        } 
        else
        {
            mouthAS.PlayOneShot(damageVoice);
        }
    }

	[RPC]
	void AddLogMesseage( string str)
	{
		controller.SendMessage( "AddLogMesseage", str);
	}
}
