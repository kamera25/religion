﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SliderText : MonoBehaviour {

    private Text text;

	// Use this for initialization
	void Awake () 
    {
        text = this.GetComponent<Text>();
	}
	
    public void ChangeTextValue( float val)
    {
        int vali = (int)val;
        text.text = vali.ToString();
    }
}
