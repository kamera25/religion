﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIBlinkText : MonoBehaviour 
{

    private float alpha = 1F;
    private int acceleration = -1;
    private Text text;

    void Start()
    {
        text = this.GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () 
    {
        if( alpha < 0.0 && acceleration == -1)
        {
            acceleration = 1;
        }
        if( 1.0 < alpha && acceleration == 1)
        {
            acceleration = -1;
        }
        alpha = alpha + ( acceleration * Time.deltaTime);

        Color col = text.color;
        col.a = alpha;
        text.color = col;

	}
}
