﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemControl : MonoBehaviour 
{
    [SerializeField] private List<int> AmmoList= new List<int>();
    [SerializeField] private List<int> RecoveryList= new List<int>();
    [SerializeField] private List<int> KeyList= new List<int>();
    [SerializeField] private List<int> EquipmentList= new List<int>();

	public int MaxAmmo = 1;
	public int MaxRecovery = 1;
	public int MaxWeapon = 1;
	public int MaxEquipment = 1;

    private WeaponDataProcess wpDataProc;

	/*
     * Define Item's Constant Number
     */
	public enum ITEMKIND
	{
		AMMO = 0,
		RECOVERY,
		KEY,
		WEAPON,
		EQUIPMENT
	}
	
	
    void Awake()
    {
        wpDataProc = this.GetComponent<WeaponDataProcess>();
    }


	/* Adding Item Lists */
	public void AddAmmoList (  int ID   )
	{
		AmmoList.Add(ID);
	}
	
	public void AddRecoveryList (  int ID   )
	{
		RecoveryList.Add(ID);
	}
	
	public void AddKeyList (  int ID   )
	{
		KeyList.Add(ID);
	}
	
	/*public void AddWeaponList (  int ID   )
	{
		WeaponList.Add(ID);
	}*/
	
	public void AddEquipmentList (  int ID   )
	{
		EquipmentList.Add(ID);
	}

	public int RemoveIDFromKey( int ID)
	{
        int cnt = Get_KeyIndexCount(ID);
        KeyList.RemoveAll(checkFlag);

		return cnt;
	}

    private bool checkFlag( int i)
    {
        return i == 0;
    }
	
	/*
     * Accseccors 
     */
	public int  Get_AmmoCount ()
	{
		return AmmoList.Count;
	}
	
	public int Get_RecoveryCount ()
	{
		return RecoveryList.Count;
	}
	
	public int Get_KeyCount ()
	{
		return KeyList.Count;
	}
	
	public int Get_WeaponCount ()
	{
        return wpDataProc.Get_WeaponListCount();// 
	}
	
	public int Get_EquipmentCount ()
	{
		return EquipmentList.Count;
	}
	

    public void RemoveRecovery( int ID)
    {
        RecoveryList.RemoveAt(ID);
    }

    public bool UseRecovery( int element)
    {
        int index = RecoveryList.IndexOf(element);
        if( index == -1)
        {
            return false;
        }

        RemoveRecovery( index);

        return true;
    }

    public int Get_KeyIndexCount( int index)
    {
        int cnt = 0;

        for( int i = 0 ; i < KeyList.Count; i++)
        {
            if( KeyList[i] == index)
            {
                cnt++;
            }
        }

        return cnt;
    }
	
    public int GetListData( int ID, ITEMKIND itemKind)
    {
        switch (itemKind)
        {
            case ItemControl.ITEMKIND.AMMO:
                return AmmoList[ID];
            case ItemControl.ITEMKIND.EQUIPMENT:
                return EquipmentList[ID];
            case ItemControl.ITEMKIND.KEY:
                return KeyList[ID];
            case ItemControl.ITEMKIND.WEAPON:
                return wpDataProc.GetWeaponIDFromList(ID);
            case ItemControl.ITEMKIND.RECOVERY:
                return RecoveryList[ID];
        }
        
        return -1;
    }

    public int MaxElemetNum( ITEMKIND itemKind)
    {
        switch (itemKind)
        {
            case ItemControl.ITEMKIND.AMMO:
                return Get_AmmoCount();
            case ItemControl.ITEMKIND.EQUIPMENT:
                return Get_EquipmentCount();
            case ItemControl.ITEMKIND.KEY:
                return Get_KeyCount();
            case ItemControl.ITEMKIND.WEAPON:
                return Get_WeaponCount();
            case ItemControl.ITEMKIND.RECOVERY:
                return Get_RecoveryCount();
        }
        
        return 0;
    }

	/* Check the number of Item equals the number of MaxItem*/
	public bool Is_NowRecoveryMax ()
	{
		return RecoveryList.Count == MaxRecovery;
	}
	
	public bool Is_NowAmmoMax ()
	{
		return AmmoList.Count == MaxAmmo;
	}
	
	
}
