﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FlagCounter : MonoBehaviour 
{
    [SerializeField] Text flagCountText;
    [SerializeField] Text flagCountTextShadow;

    private ItemControl itemCtr;

	// Use this for initialization
	void Start () 
    {
        itemCtr = GameObject.FindWithTag("GameController").GetComponent<ItemControl>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Time.frameCount % 2 == 0)
        {
            int flagCount = itemCtr.Get_KeyIndexCount(0);
            flagCountText.text = flagCount.ToString();
            flagCountTextShadow.text = flagCount.ToString();
        }
	}
}
