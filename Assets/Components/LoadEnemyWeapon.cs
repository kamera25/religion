﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoadEnemyWeapon : MonoBehaviour 
{

    public List<string> Weapons= new List<string>();
    
    // Use this for initialization
    void Start () 
    {
        
        //if( !photonView.isMine) return;
        
        WeaponDataProcess WpDataProc = GameObject.FindWithTag("GameController").GetComponent<WeaponDataProcess>();
        
        foreach( string weapon in Weapons)
        {
            WpDataProc.SetWeaponDataToList( weapon, this.transform, false, this.gameObject.GetInstanceID());
        }
        
    }
}
