﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PutWeaponList : ItemProcessForMenu
{
    public List<string> weaponList = new List<string>();

    [SerializeField] RectTransform BPWeaponList;
    [SerializeField] GameObject nodePref;
    private List<GameObject> BPnodeList = new List<GameObject>();

    private FlagManager flagMgr;

    void Start()
    {
        flagMgr = this.GetComponent<FlagManager>();

        flagMgr.flagDictionary ["REFINE_HAND"]      = true;
        flagMgr.flagDictionary ["REFINE_ASSAULT"]   = true;
        flagMgr.flagDictionary ["REFINE_SMG"]       = true;
        flagMgr.flagDictionary ["REFINE_SHOT"]      = true;
        flagMgr.flagDictionary ["REFINE_MACHINE"]   = true;
        flagMgr.flagDictionary ["REFINE_RIFLE"]     = true;
        flagMgr.flagDictionary ["REFINE_GRENADE"]   = true;
        flagMgr.flagDictionary ["REFINE_SHIELD"]    = true;
        flagMgr.flagDictionary ["REFINE_SUPPORT"]   = true;
        flagMgr.flagDictionary ["REFINE_SORD"]      = true;

        UpdatePuttingWeaponList();
    }

    private void UpdatePuttingWeaponList()
    {
        DestroyNodes();

        int i = 0;
        GameObject nowNode = null;
        foreach (string wpName in weaponList)
        {
            GameObject nowNodeSprite = null;
            int rest = i % 2;
 
            // Check refine weapon by Weapon Kind.
            string wpKind = GetWeaponWeaponKind( wpDict.FindIdFromWeaponName(wpName));
            string key = "REFINE_" + wpKind;
            if( !(bool)flagMgr.flagDictionary [key] )
            {
                continue;
            }


            if( rest == 0)
            {
                nowNode = Instantiate<GameObject>(nodePref);
                BPnodeList.Add(nowNode);
                RectTransform nowNodeTrans = nowNode.GetComponent<RectTransform>();
                nowNodeTrans.SetParent( BPWeaponList);
                
                nowNodeSprite = nowNodeTrans.FindChild("Weapon0").gameObject;
            }
            else if( rest == 1)
            {
                nowNodeSprite = nowNode.GetComponent<RectTransform>().FindChild("Weapon1").gameObject;
            }

            nowNodeSprite.GetComponent<ItemButtonBehavior>().pram = GetWeaponPrama(wpDict.FindIdFromWeaponName(wpName));
            
            nowNodeSprite.GetComponent<Image>().sprite = GetItemSpriteByName( wpName, ItemControl.ITEMKIND.WEAPON);
            
            i++;
        }

    }


    /// <summary>
    /// Toggle Process, execute when weapon type toggle button.
    /// </summary>
    public void PushRefineToggle( string weaponType)
    {
        string key = "REFINE_" + weaponType;
        flagMgr.flagDictionary [key] = !(bool)flagMgr.flagDictionary [key];

        UpdatePuttingWeaponList();
    }

    private void DestroyNodes()
    {
        foreach( GameObject node in BPnodeList)
        {
            GameObject.Destroy(node);
        }
    }

}
