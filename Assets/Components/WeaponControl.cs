﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class WeaponControl : Photon.MonoBehaviour 
{

	protected WeaponDataProcess Wdp;
    public float rapidValue = 80F;
    public Animator anim;

	private bool isputc4 = false;
    private const float relaodMotionSpeed = 1.2F;
	private GameObject player;
	private GameObject c4;

    private int granadeProcExeCount = 0;
    private Vector3 saveGranadePos;
    private string granadeName;
    public bool isStopPosture = false; // Do you assume a posture of offence?
    public bool canPlayerFire = true;

    public const float headShotDamage = 600F;
	
    protected void ShotGunFireProcess()
    {
        Entity_weapondata.Param wpData = NowWp().wpData;

        UseAmmoProcess();
        anim.SetTrigger("fire");
        NowWp().wpBhv.PlayGunShotSE();// play gun shot SE.

        GameObject clone = Instantiate(Resources.Load<GameObject>("ingame/model/Shotgun_collision"), NowWp().model.transform.FindChild("MuzzleFlashPoint").position, NowWp().model.transform.rotation) as GameObject;
        GameObject col = clone.transform.FindChild("obj1").gameObject;

        DamageAreaControl colDamageArea = col.GetComponent<DamageAreaControl>();
        colDamageArea.SetShotGunDatas( wpData.minattack, wpData.attack, wpData.range);

       

        NowWp().wpBhv.PutMuzzleFlash();
    }

    // Common Guns process.
	protected void GunFireProcess()
	{
		UseAmmoProcess();
        anim.SetTrigger("fire");
        NowWp().wpBhv.PlayGunShotSE();// play gun shot SE.

        Transform modelTrans = NowWp().model.transform;
		RaycastHit HitRaycastHit = GetHitObject();

		if( HitRaycastHit.collider != null)
		{
            GameObject HitObject = HitRaycastHit.collider.gameObject;

			if( !HitObject.CompareTag("Stage"))
			{
                // Check that attack is HeadShot.
                if( ChkRayHitHead())
                {
                    HitObject.SendMessage("DamageReciver", headShotDamage);
                    Debug.Log("headshot");
                }
                else
                {
                    Transform hitTargetTrans = HitObject.transform;
                    HitObject.SendMessage("DamageReciver", GetWeaponDamage( modelTrans.position, hitTargetTrans.position));
                }

                if( HitObject.GetComponent<Rigidbody>() != null)
                {
                    HitObject.GetComponent<Rigidbody>().AddForce( -HitRaycastHit.normal * 30F, ForceMode.Impulse);
                }
			}
		}
			
        NowWp().wpBhv.EmmisionCase( modelTrans.rotation);
        NowWp().wpBhv.PutMuzzleFlash();
		MakeBulletMark( HitRaycastHit);	

		return;
	}

	protected void GranadeWeaponFire( string Name)
	{
        anim.SetTrigger("fire");

        switch (Name)
        {
            case "RPG-7":
                PutRPG7();
                UseAmmoProcess();
                break;
            case "MGL":
                PutMGL();
                UseAmmoProcess();
                break;
        }

    }

 	protected void  SupportWeaponFire( string Name)
	{
        anim.SetTrigger("fire");


        switch (Name)
        {
            case "C4":
                if( isputc4 == false && IsLeftAmmo())
                {
                    Put_c4();
                    UseAmmoProcess();
                }   
                else if( isputc4 == true)
                {
                    Bang_c4();
                }
                break;
            case "M18Claymore":
                if( !IsLeftAmmo()) return;
                PutM18();
                UseAmmoProcess();
                break;
            case "M67":
            case "M26":
                if( !IsLeftAmmo()) return;
                PutGranade(Name);
                UseAmmoProcess();
                break;
        }

        HideWeaponProcess();
	}

    protected void SordWeaponFire( string Name)
    {
        anim.SetTrigger("fire");
        NowWp().model.SendMessage("CheckStart", NowWp().ch_time);
    }
	
	void  UseAmmoProcess ()
	{
		NowWp().Set_nowammo( NowWp().nowammo - 1);	
		SetForbidWeaponTime();
	}
	
	void  SetForbidWeaponTime ()
	{
        NowWp().ch_time = ( rapidValue / NowWp().wpData.rapid);
	}
	
	/* Make bullet mark*/
	void  MakeBulletMark (  RaycastHit HitObject  )
	{
        GameObject clone;
        Vector3 crosspos = HitObject.point;
		Vector3 normalpos = HitObject.normal;

		if( HitObject.collider == null ) return;
		if( !HitObject.collider.CompareTag("Stage") ) return;

        /* Making bullet mark process. */
        if ( PhotonNetwork.offlineMode)
        {
            clone = Instantiate(Resources.Load<GameObject>("ingame/hole/hole"), crosspos + normalpos * 0.001f, Quaternion.identity) as GameObject;    
        } 
        else
        {
            clone = PhotonNetwork.Instantiate("ingame/hole/hole", crosspos + normalpos * 0.001f, Quaternion.identity, 0);
        }

		clone.transform.LookAt( normalpos + clone.transform.position);	
		clone.transform.SetParent( HitObject.collider.gameObject.transform);
	}
	

	
	
	/* Relation of c4(Weapon) opreation. */
	void  Put_c4 ()
	{
		if (!PhotonNetwork.offlineMode)
        {
            c4 = PhotonNetwork.Instantiate("C4/C4", player.transform.position + new Vector3(0, 0.5f, 0), Quaternion.identity, 0);
        } 
        else
        {
            c4 = Instantiate(Resources.Load<GameObject>("C4/C4"), player.transform.position + Vector3.up * 3F, Quaternion.identity) as GameObject; 
        }

		c4.GetComponent<Collider>().enabled = true;
		c4.GetComponent<Rigidbody>().useGravity = true;
		c4.SendMessage("Set_isplayers");
		isputc4 = true;
	}
	
	void  Bang_c4 ()
	{
		c4.SendMessage("ForceBang");
		if (!PhotonNetwork.offlineMode)
        {
            photonView.RPC("Bang_c4OnNet", PhotonTargets.Others, c4.GetPhotonView().viewID);
        }
        ResetC4Flag();
	}

	[RPC]
	void Bang_c4OnNet( int viewID)
	{
		PhotonView.Find(viewID).SendMessage("ForceBang");
	}
	
	void  ResetC4Flag ()
	{
		isputc4 = false;
	}
	
	/* End of c4 operation */
	
	/* Relation of M18(Weapon) opreation. */
	void  PutM18 ()
	{
        GameObject m18Model;
        const string wpName = "M18Claymore";
        string path = wpName + "/" + wpName;
        Vector3 pos = player.transform.position + player.transform.forward * 2.5f;

        if (PhotonNetwork.offlineMode)
        {
            m18Model = Instantiate( Resources.Load<GameObject>( path), pos, Quaternion.identity) as GameObject;
        } 
        else
        {
            m18Model = PhotonNetwork.Instantiate( path, pos, Quaternion.identity, 0);
        }

		m18Model.transform.rotation =  player.transform.rotation;
	}
	
	/* End of M18 operation */
	
	/* Relation of Granade operation */
	void  PutGranade (  string name  )
	{
        const float waitTime = 0.37F;

        granadeName = name;
        Invoke( "GranadeProc", waitTime);
        NowWp().granadeCtr.InvisibleOrdit();

        return;
	}

    void GranadeProc()
    {
        if (granadeProcExeCount == 0)
        {
            saveGranadePos = NowWp().model.transform.position - player.transform.position;
            granadeProcExeCount++;
            Invoke( "GranadeProc", 0.05F);
        } 
        else
        {
            Vector3 distVec = (NowWp().model.transform.position - player.transform.position) - saveGranadePos;

            GameObject Clone;
            if (PhotonNetwork.offlineMode)
            {
                Clone = Instantiate( Resources.Load<GameObject>( granadeName + "/" + granadeName), NowWp().model.transform.position, Quaternion.identity) as GameObject;
            } 
            else
            {
                Clone = PhotonNetwork.Instantiate( granadeName + "/" + granadeName, NowWp().model.transform.position, Quaternion.identity, 0);
            }

            Vector3 revisionVec = Vector3.Lerp( distVec.normalized, player.transform.forward, 0.8F ) + Vector3.up * 0.4F;
            Clone.GetComponent<Rigidbody>().AddForce( revisionVec * 10F , ForceMode.Impulse);

            Clone.gameObject.GetComponent<DamageBang>().enabled = true;

            granadeProcExeCount = 0;
        }

        return;
    }

	/* End of Granade operation */
	
	void  PutRPG7 ()
	{
        Vector3 DistPos = GetRayPosition();
		
		GameObject rpg7bullet;
        Vector3 pos = NowWp().model.transform.FindChild("MuzzleRoot").position;

        if (PhotonNetwork.offlineMode)
        {
            GameObject bullet = Resources.Load<GameObject>("RPG-7/RPG7bullet");
            rpg7bullet = Instantiate(bullet, pos, Quaternion.identity) as GameObject;
        } else
        {
            rpg7bullet = PhotonNetwork.Instantiate("RPG-7/RPG7bullet", pos, Quaternion.identity, 0);
        }
        rpg7bullet.transform.LookAt( DistPos);
	}
	
	void  PutMGL ()
	{
        Vector3 DistPos = GetRayPosition();
		
		GameObject Clone;
        Vector3 pos = NowWp().model.transform.FindChild("MuzzleRoot").position;

		if ( PhotonNetwork.offlineMode)
        {
            Clone = Instantiate(Resources.Load<GameObject>("MGL/MGLbullet"), pos, Quaternion.identity) as GameObject;
        } 
        else
        {
            Clone = PhotonNetwork.Instantiate("MGL/MGLbullet", pos, Quaternion.identity, 0);
        }

		Clone.name = "MGL";
		Clone.transform.LookAt( DistPos);
		Clone.GetComponent<Rigidbody>().AddForce( Clone.transform.forward * 35, ForceMode.Impulse);
	}

    void HideWeaponProcess()
    {
        if ( NowWp().nowammo == 0 && NowWp().granadeCtr != null)
        {
            NowWp().granadeCtr.HideGrenade();
        }                          
    }
	
	/* Reload Operation. */
	protected void  ReloadProcess ()
	{

		if( 0 < NowWp().nowReload) return;
        if( PlayerGUIEndReloadProc() == 0) return;

		if (0 < NowWp().nowmagazine)
        {
            int chageAmmo = NowWp().wpData.ammo;
			
            if (NowWp().wpData.magazine == NowWp().nowmagazine
                && NowWp().wpData.ammo == NowWp().nowammo 
                && NowWp().type != WeaponKind.GRENADE
            )
            {
                chageAmmo++; // if no use guns, add 1 ammo in reload.
            } else if (NowWp().wpData.ammo == NowWp().nowammo)
            {
                return; // No need Reload.
            }
			
            NowWp().Set_nowammo(chageAmmo);
            NowWp().Set_nowmagazine(NowWp().nowmagazine - 1);
			
            // Set Reload Wait.
            float reloadSpeed = NowWp().wpData.charge_time * WeaponData.RELOADWEIGHT;
            anim.SetFloat("reloadSpeed", NowWp().wpData.charge_time);
            NowWp().Set_nowReload(reloadSpeed);
            PlayerGUIStartReloadProc();

            // Play reload motion.
            anim.SetTrigger("reload");

            // Emmision Magazine.
            if (NowWp().wpBhv != null)
            {
                NowWp().wpBhv.EmmisionMagazine(NowWp().model.transform.rotation);
            }
        }

		return;
	}

	
    public void RegisterPlayer( PlayerData data)
	{
		player = data.player;
        anim = data.anim;
	}

    /* 
     *  Reset Weapon Process. 
     */

	public void ResetAllWeapon()
	{
        foreach( WeaponData Wpd in Wdp.WeaponDataList)
        {
            Wdp.ResetWeapon(Wpd);
        }
	}

	public void ResetWeapon( int wpID)
	{
        Wdp.WeaponDataList[wpID].nowammo = Wdp.WeaponDataList[wpID].wpData.ammo;
        Wdp.WeaponDataList[wpID].nowmagazine = Wdp.WeaponDataList[wpID].wpData.magazine;
	}

    public void ResetAllWeaponMagazine()
    {
        for( int i = 0; i < Wdp.WeaponDataList.Count; i++)
        {
            Wdp.WeaponDataList[i].nowmagazine = Wdp.WeaponDataList[i].wpData.magazine;
        }
    }
	

    /* ***********
     * Condition *
     * ***********/
    public bool IsLeftAmmo ()
    {
        // if your's weapon is sord.
        if (NowWp().type == WeaponKind.SORD)
        {
            return true;
        }

        return 0 < NowWp().nowammo; 
    }
    
    public bool IsWeaponUse ()
    {
        return NowWp().ch_time < 0
            && NowWp().nowReload < 0
            && !isStopPosture
            && canPlayerFire;
    }

    /* END */

    // Follow methods is related Ray.

    protected virtual RaycastHit GetHitObject()
    {
        Debug.LogWarning("Dont Call!!!");
        return new RaycastHit();
    }

    protected virtual Vector3 GetRayPosition()
    {
        Debug.LogWarning("Dont Call!!!");
        return Vector3.zero;
    }

    /// <summary>
    /// Check ray hits head. It use for headshot.
    /// When Enemy, always return false.
    /// </summary>
    protected virtual bool ChkRayHitHead()
    {
        return false;
    }

    // END

    protected virtual void PlayerGUIStartReloadProc()
    {
        return;
    }

    protected virtual int PlayerGUIEndReloadProc()
    {
        return 0;
    }

    protected float GetWeaponDamage( Vector3 wpPos, Vector3 targetPos)
    {
        Entity_weapondata.Param wpData = NowWp().wpData;

        float nowRange = Vector3.Magnitude( targetPos - wpPos) ;
        float fluctDamage = wpData.attack * ( (wpData.range * LandmarkIndicatorBehavior.WORLD_SCALE_CORRECTION) / nowRange);

        return fluctDamage + wpData.minattack;
    }

    public virtual WeaponData NowWp()
    {
        Debug.LogWarning("Dont Call!!!");
        return null;
    }

}