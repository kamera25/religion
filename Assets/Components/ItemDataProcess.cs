﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum RECOVERYKIND
{
    HP_RECOVER,
    STMNRE_COVER,
    DEATH_RECOVER
}

public class ItemDataProcess : MonoBehaviour 
{

    private Entity_weapondata entityWp;
    private Entity_recovery entityRc;
    private Entity_ammo entityAmmo;
    private Entity_key entityKey;
    private Entity_Equipment entityEquip;

    protected WeaponDictionary wpDict;

    void  Awake ()
    {
        entityWp = Resources.Load<Entity_weapondata>("data/Weapon/weapon");
        entityRc = Resources.Load<Entity_recovery>("data/Recovery/Recovery");
        entityAmmo = Resources.Load<Entity_ammo>("data/Ammo/Ammo");
        entityKey = Resources.Load<Entity_key>("data/Key/Key");
        entityEquip = Resources.Load<Entity_Equipment>("data/Equipment/Equipment");

        wpDict = this.gameObject.AddComponent<WeaponDictionary>();
    }
	
    public string GetWeaponBrief( int ID)
    {
        return entityWp.sheets [0].list [ID].brief;
    }

    public string GetRecoveryBrief( int ID)
    {
        return entityRc.sheets [0].list [ID].brief;
    }

    public string GetWeaponName( int ID)
    {
        return entityWp.sheets [0].list [ID].name;
    }

    public string GetRecoveryName( int ID)
    {
        return entityRc.sheets [0].list [ID].name;
    }

    public string GetEquipmentName( int ID)
    {
        return entityEquip.sheets [0].list [ID].name;
    }

    public string GetKeyName( int ID)
    {
        return entityKey.sheets [0].list [ID].name;
    }

    public string GetAmmoName( int ID)
    {
        return entityAmmo.sheets [0].list [ID].name;
    }

    public int GetRecoveryNum1( int ID)
    {
        return entityRc.sheets [0].list [ID].num1;
    }
    
    public int GetRecoveryNum2( int ID)
    {
        return entityRc.sheets [0].list [ID].num2;
    }

    public string GetWeaponWeaponKind( int ID)
    {
        return entityWp.sheets [0].list [ID].type;
    }

    public RECOVERYKIND GetRecoveryKind( int ID)
    {
        return (RECOVERYKIND)Enum.Parse( typeof(RECOVERYKIND), entityRc.sheets [0].list [ID].kind);
    }

    public Entity_weapondata.Param GetWeaponPrama( int ID)
    {
        return entityWp.sheets [0].list [ID];
    }
}
