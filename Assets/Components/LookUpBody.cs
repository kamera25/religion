﻿using UnityEngine;
using System.Collections;

public class LookUpBody : Actor
{

	public Transform playerBack;
	private Animator animator;
	private RayControl rayCtr;
	private PoseModeControl poseCtr;

	private float dumping = 20f; 

	// Use this for initialization
	void Start () 
	{

		if( !PhotonView.Get(this).isMine && !PhotonNetwork.offlineMode)
		{
			this.enabled = false;
			return;
		}

        animator = GetAnimator();

        GameObject controller = GameObject.FindWithTag("GameController");
		rayCtr = controller.GetComponent<RayControl>();
		poseCtr = controller.GetComponent<PoseModeControl>();
	}
	
	// Update is called once per frame
	void Update () 
	{

        switch (poseCtr.PoseMode)
        {
            case POSE.SUBJECT:
            case POSE.THIED:
                animator.SetFloat("AimX", 0);
                return;
            case POSE.SHOULDER:

                SetUpBodyDirection();// Y coordinate             
                Vector3 dist = rayCtr.GetRayPositionForWeapon() - playerBack.position;  
                if( 10 < dist.sqrMagnitude)
                {   
                    SetDownBodyDirection();// X coordinate
                }   
                break;
        }

	}

	void SetUpBodyDirection()
	{
		Vector3 Hitpos = rayCtr.GetRayPositionForWeapon();
		float yVec ;

        Vector3 vec1 = Hitpos - playerBack.position;;
        Vector3 vec2 = new Vector3( Hitpos.x, playerBack.position.y, Hitpos.z) - playerBack.position;
		
		/* ********************* */
		/* UpBody Aiming Process.*/
		/* ********************* */	

		if( vec1.y < 0) // Down position.
		{
			yVec = Mathf.Clamp( Vector3.Angle( vec1, vec2), 0, 40) / 40 * -1;
		}
		else // Up position.
		{
			yVec = Mathf.Clamp( Vector3.Angle( vec1, vec2), 0, 35) / 35;	
		}
		animator.SetFloat( "AimY", Mathf.Lerp( animator.GetFloat("AimY"), yVec, Time.deltaTime * dumping));
	}
	
	void SetDownBodyDirection()
	{
		/* ************************ */
		/* DownBody Aiming Process. */
		/* ************************ */
		Vector3 Hitpos = rayCtr.GetRayPositionForWeapon();
		float xzVec ;

        Vector3 vec1 = this.transform.right;
		Vector3 vec2 = new Vector3( Hitpos.x, playerBack.position.y, Hitpos.z) - playerBack.position;
		
		// Is there mouse pointer on first or second quadrant(ja : syougen)?
		if( Vector3.Angle( this.transform.forward, vec2) < 60)
        {
			xzVec = Mathf.Clamp( Vector3.Angle( vec1, vec2) - 90, -50, 50) / 50;
			animator.SetFloat( "AimX", Mathf.Lerp( animator.GetFloat("AimX"), xzVec, Time.deltaTime * dumping));
		}
	}
}


