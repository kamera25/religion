﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundConfigBehavior : AudioSourceController
{
    public GameObject menu;

    private float bgmValue;
    private float seValue;

    [SerializeField] private Slider bgmSlider;
    [SerializeField] private Slider seSlider;

    void Start()
    {
        bgmSlider.value = bgmVolume;
        seSlider.value = seVolume;
    }

    public void ChangeBGMValue( float val)
    {
        bgmValue = bgmSlider.value;
        AudioSourceController.bgmVolume = (int)bgmSlider.value;
    }

    public void ChangeSEValue( float val)
    {
        seValue = seSlider.value;
        AudioSourceController.seVolume = (int)seSlider.value;
    }

    public void DestroyMenuProc()
    {

        Destroy(menu);
    }
	
}
