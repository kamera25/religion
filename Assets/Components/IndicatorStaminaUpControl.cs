﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IndicatorStaminaUpControl : Photon.MonoBehaviour 
{

    private StatusControl statusCtr;
    private Image indicatorImg;

	// Use this for initialization
	void Start () 
    {
        if ( !PhotonNetwork.offlineMode)
        {
            this.enabled = false;
            return;
        }

        indicatorImg = this.GetComponent<Image>();
        statusCtr = GameObject.FindWithTag("Player").GetComponent<StatusControl>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if( Time.frameCount % 2 == 0)
        {  
            return; 
        }

        if (statusCtr.IsStaminaSpeedUp())
        {
            indicatorImg.enabled = true;
        } 
        else
        {
            indicatorImg.enabled = false;
        }
	}
}
