using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemDetailUIBehavior : MonoBehaviour 
{
    [SerializeField] Image itemImage;
    [SerializeField] Text itemText;
    [SerializeField] Text detailText;

    [SerializeField] Button equipWeaponButton;
    [SerializeField] Button dropWeaponButton;

    private DETAILTYPE detailType;

    enum DETAILTYPE
    {
        MYWEAPON,
        WAREHOUSE
    }

    void Awake()
    {
        InvisibleAllObject();
    }

    private void InvisibleAllButtons()
    {
        equipWeaponButton.gameObject.SetActive(false);
        dropWeaponButton.gameObject.SetActive(false);
    }

    private void InvisibleAllObject()
    {
        InvisibleAllButtons();
        itemText.gameObject.SetActive(false);
        detailText.gameObject.SetActive(false);
        itemImage.gameObject.SetActive(false);
    }

    private void VisibleAllObject()
    {
        itemText.gameObject.SetActive(true);
        detailText.gameObject.SetActive(true);
        itemImage.gameObject.SetActive(true);
    }

    private void UpdateDetailUI( GameObject go, Button button)
    {
        VisibleAllObject();

        itemImage.sprite = go.GetComponent<Image>().sprite;

        ItemButtonBehavior itemButtonBhv = go.GetComponent<ItemButtonBehavior>();
        itemText.text = itemButtonBhv.pram.name;
        detailText.text = itemButtonBhv.pram.brief;

        // Change option of SendMessageAsButton.
        switch(detailType)
        {
            case DETAILTYPE.MYWEAPON:
                button.GetComponent<SendMessageAsButton>().option = itemButtonBhv.index.ToString();
                break;
            case DETAILTYPE.WAREHOUSE:
                button.GetComponent<SendMessageAsButton>().option = itemButtonBhv.pram.name;
                break;
        }

    }

    /*
     * Item Button Behavior.
     */
    public void PutEquipWeaponButton( GameObject go)
    {
        InvisibleAllButtons();
        detailType = DETAILTYPE.WAREHOUSE;
        
        dropWeaponButton.gameObject.SetActive(true);
        UpdateDetailUI( go, dropWeaponButton);
    }
    
    public void PutDropWeaponButton( GameObject go)
    {
        InvisibleAllButtons();
        detailType = DETAILTYPE.MYWEAPON;
        
        equipWeaponButton.gameObject.SetActive(true);
        UpdateDetailUI( go, equipWeaponButton);
    }

    public void DropWeapon()
    {
        InvisibleAllObject();
        equipWeaponButton.gameObject.SetActive(false);
    }

    public void EquipWeapon()
    {
        InvisibleAllObject();
        dropWeaponButton.gameObject.SetActive(false);
    }
}
