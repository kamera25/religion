﻿using UnityEngine;
using System.Collections;

public class LoadStageAddtiveButton : MonoBehaviour 
{
    public string scene;

    public void PushButton()
    {
        Application.LoadLevelAdditive(scene);
    }
}
