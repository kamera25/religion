﻿using UnityEngine;
using System.Collections;

public class DamageAreaControl : MonoBehaviour 
{

    public int existtime = 5;
    public bool useAttenuation = false;

    private float minDamage = 100F;
    private float fluctVal = 0F;
    private float wpRange = 0F;
    private Vector3 pivotPos;

    void Awake()
    {
        if( this.transform.parent != null)
        {
            pivotPos = this.transform.parent.position;
        }
    }

    void  Update()
    {

        if( existtime <= 0)
        {
            if( this.transform.parent != null)
            {
                Destroy( this.transform.parent.gameObject);
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
        existtime--;
    }
    
    void  OnTriggerEnter (  Collider Col  )
    {
        SendDamage(Col.gameObject);
    }
    
    void  OnColliderEnter (  Collision Col  )
    {
        SendDamage(Col.gameObject);
    }

    void SendDamage( GameObject obj)
    {
        switch (obj.tag)
        {
            case "Player":
            case "Enemy":
            case "EnemyNoMan":
            case "Explosive":
                float wpDamage;

                if( useAttenuation)
                {
                    float nowRange = Vector3.Magnitude( obj.transform.position - pivotPos) ;
                    float fluctDamage = fluctVal * ( (wpRange * LandmarkIndicatorBehavior.WORLD_SCALE_CORRECTION) / nowRange);

                    wpDamage = fluctDamage + minDamage;
                }
                else
                {
                    wpDamage = minDamage;
                }

                obj.SendMessage( "DamageReciver", wpDamage);
                break;
        }
    }

    /* *************
     * Accsessor.  *
     * *********** */

    public void SetShotGunDatas( float mindamage, float fluctdamage, float range)
    {
        minDamage = mindamage;
        fluctVal = fluctdamage;
        wpRange = range;

        useAttenuation = true;
    }

    public void SetDamage( float damage)
    {
        minDamage = damage;
    }

}
