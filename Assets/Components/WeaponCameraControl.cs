﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum SIGHT
{
	IRON = 0,
	SCOPE,
	NONE
}

public class WeaponCameraControl : Photon.MonoBehaviour
{

	public SIGHT sight;
	public Transform lensCam;
	public Transform lookAtObject;
    public Sprite scopeTexture;

    public float scopeFog = 1F;

    private float scopeFogDist;

	private GameObject scopeImg;
	private PoseModeControl poseCtr;
    private ScopeImgControl scopeImgCtr;
	private ScopeShotCamera scopeCamCtr;

    void Awake()
    {
        scopeFogDist = scopeFog * RenderSettings.fogDensity;
    }

	// Use this for initialization
	void Start () 
	{
		if ( !photonView.isMine && !PhotonNetwork.offlineMode) 
		{
			this.enabled = false;
			return;
		}

		poseCtr = GameObject.FindWithTag("GameController").GetComponent<PoseModeControl>();

		Transform player = this.transform.parent.parent;
		if (player == null) this.enabled = false;
		scopeCamCtr = player.GetComponent<ScopeShotCamera>();

		scopeImg = GameObject.Find("ScopeImg");
        scopeImgCtr = scopeImg.GetComponent<ScopeImgControl>();
	}

	void LateUpdate()
	{
		switch (sight) 
		{
			case SIGHT.SCOPE:
				if( poseCtr.PoseMode == POSE.SUBJECT)
				{
                    scopeImgCtr.SetScopeImage( scopeTexture);
				}		
				break;
			case SIGHT.NONE:
			case SIGHT.IRON:
                scopeImgCtr.SetTransparent();
				break;
		}


		if (sight == SIGHT.NONE) 
		{
			scopeCamCtr.EnableUsePlayerScope();
		}
		else
		{
			scopeCamCtr.DisableUsePlayerScope();

			Camera.main.transform.position = lensCam.position;
			Camera.main.transform.rotation = this.transform.rotation;
			Camera.main.fieldOfView = lensCam.GetComponent<Camera>().fieldOfView;
		}
	}

    void OnEnable()
    {
        // Fog Setting 
        if (sight == SIGHT.SCOPE)
        {
            RenderSettings.fogDensity = scopeFogDist;
        }
    }

    public void OnDisable()
    {
        if (sight == SIGHT.SCOPE)
        {
            RenderSettings.fogDensity = scopeCamCtr.defaultFogDist;
        }
    }
}
