﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeaponDrawControl : MonoBehaviour 
{
    [SerializeField] private Text ammoText;
    [SerializeField] private Text magText;
	
    private GameObject ammoUI;
    private GameObject magUI;

    private int beforeAmmo = 0;
    private int beforeMagazine = 0;
    private int beforeNowWeaponNo = 0;

    private PlayerWeaponControl playerWpCtr;

    void Start()
    {
        playerWpCtr = this.GetComponent<PlayerWeaponControl>();
        ammoUI = ammoText.transform.parent.gameObject;
        magUI = magText.transform.parent.gameObject;
    }

	// Update is called once per frame
	void Update () 
    {
        DrawGUI();
	}

    void  DrawGUI ()
    {
        
        if ( playerWpCtr.NowWeaponNo == 0)
        {
            ammoUI.SetActive(false);
            magUI.SetActive(false);
            
            return;
        } 
        else if( NowWp().wpData.ammo == -1) // Sord's ammo sets -1.
        {
            ammoUI.SetActive(false);
            magUI.SetActive(false);
            
            return;
        }
        else if ( playerWpCtr.NowWeaponNo != beforeNowWeaponNo 
                 || beforeAmmo != NowWp().nowammo 
                 || beforeMagazine == NowWp().nowmagazine)
        {
            
            ammoUI.SetActive(true);
            magUI.SetActive(true);
            
            /* Put Ammo Bar*/
            // Ammo of activity ratio.
            float AmmoExcessRate = (float)(NowWp().nowammo) / (float)( NowWp().wpData.ammo);
            
            if( AmmoExcessRate < 0.1F)
            {
                Color RED = new Color( 1F, 0.1F, 0.1F, 1F);
                ammoText.color = RED;
                ammoText.text = "× " + NowWp().nowammo;
            }
            else if( AmmoExcessRate < 0.4f)
            {
                Color YELLOW = new Color( 0.72F, 0.72F, 0F, 1F);
                ammoText.color = YELLOW;
                ammoText.text = "× " + NowWp().nowammo;
            }
            else
            {
                Color GRAY = new Color( 0.278F, 0.278F, 0.278F, 1F);
                ammoText.color = GRAY;
                ammoText.text = "× " + NowWp().nowammo;
            }
            
            /* if there is C4, Craymore, other Bomb, hide magazine expression.*/
            if( NowWp().wpData.magazine == 0)
            {
                magUI.SetActive(false);
            }
            else
            {
                magUI.SetActive(true);
                magText.text = "× " + NowWp().nowmagazine;
            }
        }
        
        
        beforeAmmo = NowWp().nowammo;
        beforeMagazine = NowWp().nowmagazine;
        beforeNowWeaponNo = playerWpCtr.NowWeaponNo;
        
    }

    private WeaponData NowWp()
    {
        return playerWpCtr.NowWp();
    }
}
