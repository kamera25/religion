﻿using UnityEngine;
using System.Collections;

public class WeaponStopPostureControl : MonoBehaviour 
{
    [SerializeField] private Animator anim;
    private bool hitWall;
    private PoseModeControl poseModeCtr;
    private PlayerWeaponControl playerWpCtr;

    void Start()
    {
        GameObject controller = GameObject.FindWithTag("GameController");
        poseModeCtr = controller.GetComponent<PoseModeControl>();
        playerWpCtr = controller.GetComponent<PlayerWeaponControl>();
    }

    void Update()
    {
        if (hitWall)
        {
            anim.SetBool("isPatrol", true);// Change Animation.
            poseModeCtr.DisablecanChange();// Set pose cannot change.
            poseModeCtr.ForceSetThiedPose();
            playerWpCtr.isStopPosture = true;
        } 
        else
        {
            anim.SetBool("isPatrol", false);
            poseModeCtr.EnablecanChange();
            playerWpCtr.isStopPosture = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Stage"))
        {
            hitWall = true;
        }
    }

    private void OnTriggerExit( Collider other)
    {
        if (other.CompareTag("Stage"))
        {
            hitWall = false;
        }
    }
}
