using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class SetBaseCamp : Photon.MonoBehaviour 
{

	GameObject[] basePoint;
	TEAM campNumber = TEAM.NONE;
	List<BaseCamp> baseCamp = new List<BaseCamp>();

    private int MAX_BASECAMP = 4;

	// Use this for initialization
	void Awake () 
    {
		basePoint = GameObject.FindGameObjectsWithTag("BasePoint");

		// Initialize baseCamp List.
		for( int i = 0; i < MAX_BASECAMP; i++)
		{
			BaseCamp bc = new BaseCamp();
			bc.team = TEAM.NONE;
			baseCamp.Add( bc); 
		}
	}
	

	public void SetPosFromAroundBasePoint( TEAM team, Transform ply)
	{
		ply.transform.position = GetPosVec3FromAroundBasePoint( team);
	}

	
	public Vector3 GetPosVec3FromAroundBasePoint( TEAM team )
	{
		// Search index of team
		int index;
		for( index = 0; index < MAX_BASECAMP; index++)
		{
			if( baseCamp[index] == null) continue;
			if( baseCamp[index].team == team) break;
		}
		
		if( index == 4 || team == TEAM.NONE) 
		{
			Debug.LogError( "Error!! No exist BaseCamp such a assign team.");
			return Vector3.zero;
		}
		
		// Get 2 point near my BaseCamp by random.
		GameObject[] Obj = new GameObject[2];
		int i = 0;
		while( true)
		{
			
			foreach( Transform child in baseCamp[index].campPoint.transform)
			{
				// 
				if( (int)Random.Range( 0, 2) == 0)
				{
					Obj[i++] = child.gameObject;
					if( i == 2) break;
				}
			}
			
			if( i == 2) break;
		}
		
		// Set Cararcter Position
		return Vector3.Lerp( Obj[0].transform.position, Obj[1].transform.position, Random.Range( 0f, 1f));
	}


	public void BatchSetBaseCamp( int index)
	{
		for( int i = 0; i < index; i++)
		{
			SetNewBaseCamp();
		}
	}

	public void BatchSetBaseCampFlagBattle( int index)
	{
		for( int i = 0; i < index; i++)
		{
			SetNewBaseCampAddMode( true);
		}
	}

	// Search Base Camp position and Set.
	public void SetNewBaseCampAddMode( bool isflagBattle)
	{
		/*if( isFinal)
		{
			Debug.LogError( "SetNewBaseCamp is arleady call!!!");
			return;
		}*/

		GameObject clone;
		while( true )
		{
			int index = Random.Range( 0, basePoint.Length);
			if( baseCamp[index].GetcampObject() == null)
			{
                campNumber++;

				if( PhotonNetwork.offlineMode)
				{
                    clone = Instantiate( Resources.Load<GameObject>("unit/bumpy"), basePoint[index].transform.position, Quaternion.identity) as GameObject; 
                    this.transform.parent.GetComponent<BaseCampBehavior>().team = campNumber;
				}
				else
                {
                    clone = PhotonNetwork.Instantiate( "unit/bumpy", basePoint[index].transform.position, Quaternion.identity, 0) as GameObject;  
                    photonView.RPC( "BaseCampSettingOnNet", PhotonTargets.AllBuffered, clone.GetPhotonView().viewID, (int)campNumber);
				}

				// Add BaseCamp class.
				baseCamp[index].SetcampObject( clone);
				baseCamp[index].campPoint = basePoint[index];
				baseCamp[index].team = campNumber;
				
				break;
			}
		}
	}

	[RPC]// Others
	public void BaseCampSettingOnNet( int viewID, int team)
	{
        PhotonView.Find( viewID).gameObject.GetComponent<BaseCampBehavior>().team = (TEAM)team;// Set
	}

	// Search Base Camp position and Set.
	public void SetNewBaseCamp( )
	{
		SetNewBaseCampAddMode( false);
	}
	

}
