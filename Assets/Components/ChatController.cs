﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class ChatController : Photon.MonoBehaviour 
{
    [SerializeField] GameObject chatUI;
    [SerializeField] InputField chatBox;
    private LogControl logCtr;
    private bool visibleChatUI = true;

    const float deleteWaitTime = 0.1F; 

	// Use this for initialization
	void Start () 
    {
        logCtr = GameObject.FindWithTag("GameController").GetComponent<LogControl>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if (Input.GetButtonDown("Chat"))
        {
            visibleChatUI = !visibleChatUI;
            if( visibleChatUI)
            {
                chatUI.SetActive(true);
                chatBox.gameObject.SetActive(true);
                EventSystem.current.SetSelectedGameObject( chatBox.gameObject);
        
                StartCoroutine( "DeleteChatBoxChara");
            }
        }

        if( !visibleChatUI){
            chatUI.SetActive(false);
            chatBox.gameObject.SetActive(false);
            EventSystem.current.SetSelectedGameObject( null);
        }
	}

    public void SendChatMessage()
    {
        if( chatBox.text != "")
        {
            logCtr.AddLogOnNet( PhotonNetwork.playerName + ":" + chatBox.text);
            chatBox.text = "";
            visibleChatUI = false;
        }
    }

    private IEnumerator DeleteChatBoxChara()
    {
        yield return null;
        chatBox.text = "";
    }
}


