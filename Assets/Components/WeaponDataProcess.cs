using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class WeaponDataProcess : Photon.MonoBehaviour 
{

	public List<WeaponData> WeaponDataList = new List<WeaponData>();
    public List<WeaponData> EnemyWeaponDataList = new List<WeaponData>();

    // 武器の最大所持数.
    public int maxMainWeapon = 1;
    public int maxSubWeapon = 1;
    public int maxSupportWeapon = 1;

    private Dictionary<int, WeaponData> weaponDataChacheList = new Dictionary<int, WeaponData>();

    private Entity_weapondata entityWp;

    private WeaponDictionary wpDict;
    private PlayerWeaponControl playerWpCtr;

	void  Awake ()
	{
        // Load weapon data from spreadsheet.
        entityWp = Resources.Load<Entity_weapondata>("data/Weapon/weapon");
        wpDict = this.gameObject.AddComponent<WeaponDictionary>();
        playerWpCtr = this.GetComponent<PlayerWeaponControl>();
	}

    /// <summary>
    /// Load Weapon Data into a WeaponDataList.
    /// </summary>
	public void SetWeaponDataToList ( string weaponName, Transform parent, bool isPlayer, int user)
	{
        RegisterNewUserWeapon(user);

        if (isPlayer)
        {
            WeaponDataList.Add( LoadWeaponData( weaponName, parent, true, user));
            UpdateAllWeaponType();
        } 
        else
        {
            EnemyWeaponDataList.Add( LoadWeaponData( weaponName, parent, false, user));
        }

	}

    /// <summary>
    /// Register user for using WeaponData chache.
    /// </summary>
    private void RegisterNewUserWeapon( int user)
    {
        if (!weaponDataChacheList.ContainsKey(user))
        {
            weaponDataChacheList.Add(user, null);
        }
    }

    /// <summary>
    /// Load Weapon Data from XML and prefabs into structure. Using weapon's name.
    /// </summary>
    private WeaponData LoadWeaponData( string weaponName, Transform parent, bool isPlayer, int user)
    {
        int ID = FindIdFromWeaponName( weaponName);
        return LoadWeaponDataFromID( ID, parent, isPlayer, user);
    }

    /// <summary>
    /// Load Weapon Data from XML and prefabs into WeaponData structure. Using ID.
    /// </summary>
    private WeaponData LoadWeaponDataFromID( int ID, Transform parent, bool isPlayer, int user)
    {
        WeaponData WpD  = new WeaponData();

        WpD.wpData = entityWp.sheets [0].list [ID];

        WpD.type = ConvertWptypeNameToID( WpD.wpData.type);
        WpD.recoil = ConvertRecoilMagToValue( WpD.wpData.recoil);
        WpD.bullet_type = ConvertAmmoNameToID( WpD.wpData.bullet_type);  

        if (isPlayer)
        {
            WpD.icon = Resources.Load<Sprite>(WpD.wpData.name + "/pict");
        }
        WpD.user = user;


        // Default Operation. 
        WpD.model = LoadModel( WpD.wpData.name, isPlayer);
        WpD.model.GetComponent<Collider>().enabled = false; // Weapon collider disable.
        if( parent != null)
        {
            WpD.model.transform.SetParent( parent);
        }

        if (WpD.wpData.magazine == 1)
        { // The item has one Magazine, should do odd Behavior.(Shiled, Support, etc...)
            WpD.wpData.magazine = 0;
        }
        ResetWeapon(WpD);
        //WpD.wpType = GetWeaponType( WpD.type);

        WpD.wpBhv = WpD.model.GetComponent<WeaponBehavior>();
        
        if (WpD.wpData.rapid == 0)
        {
            WpD.wpData.rapid = 30F;
        }


        WpD.ch_time = 0.0f;
        WpD.nowReload = 0.0f;

        switch (WpD.wpData.name)
        {
            case "M26":
            case "M67":
                WpD.granadeCtr = WpD.model.GetComponent<GranadeController>();
                break;
        }

        if (!PhotonNetwork.offlineMode)
        {
            photonView.RPC( "RegistWeaponOnOnline", PhotonTargets.OthersBuffered, parent.gameObject.GetPhotonView().viewID, WpD.model.GetPhotonView().viewID);
        }
        
        return WpD;
    }

    LANK ConvertRecoilMagToValue (  string name  )
    {
        name = name.Replace( "+", "_PLUS");
        name = name.Replace( "-", "_MINUS");

        LANK lank = (LANK)Enum.Parse( typeof(LANK), name);
        return lank;
    }
    
    WeaponKind ConvertWptypeNameToID (  string name  )
    {
        WeaponKind wpKind = (WeaponKind)Enum.Parse( typeof( WeaponKind), name);
        return wpKind;
    }

    // 全ての武器タイプを最新の状態に更新する.
    void UpdateAllWeaponType()
    {
        int i = 0;
        foreach (WeaponData Wpd in WeaponDataList)
        {
            WeaponKind type = Wpd.type;
            Wpd.wpType = GetWeaponType( type, i);
            i++;
        }
    }

    // 武器タイプの取得を行う.
    WEAPONTYPE GetWeaponType( WeaponKind wpKind, int no)
    {
        if (no == 0)
        {
            return WEAPONTYPE.MAIN;
        } 
        else if (wpKind == WeaponKind.SUPPORT)
        {
            return WEAPONTYPE.SUPPORT;
        }

        return WEAPONTYPE.SUB;
    }

    // 武器名からそのWeaponTypeを検出し,これ以上の追加が可能かチェックします.
    public bool CheckAddingWeaponTypeFromName( string wpName)
    {
        int ID = FindIdFromWeaponName( wpName);
        WeaponData WpD  = new WeaponData();
        
        WpD.wpData = entityWp.sheets [0].list [ID];
        WpD.type = ConvertWptypeNameToID( WpD.wpData.type);

        WEAPONTYPE thisWpType = GetWeaponType(WpD.type, WeaponDataList.Count);

        int count = WeaponDataList
                    .Where(data => data.wpType == thisWpType)
                    .Count();

        // 使う変数で分岐
        switch (thisWpType)
        {
            case WEAPONTYPE.MAIN:
                return count < maxMainWeapon;
            case WEAPONTYPE.SUB:
                return count < maxSubWeapon;
            case WEAPONTYPE.SUPPORT:
                return count < maxSupportWeapon;
        }

        return false;
    }
	

	BULLET ConvertAmmoNameToID (  string name  )
	{
        switch (name)
        {
            case ".45ACP":
                return BULLET._45ACP;
            case ".22L":
                return BULLET._22L;
            case "9mm":
                return BULLET._9MM;
            case ".44magnam":
                return BULLET._44MAGNAM;
            case ".357mag":
                return BULLET._357MAG;
            case "5.56mm":
                return BULLET._556MM;
            case "HEAT":
                return BULLET._HEAT;
            case "40mm":
                return BULLET._40MM;
            case ".38Spc":
                return BULLET._38SPC;
            case "12GAGE":
                return BULLET._12GAGE;
            case "7.62mmx39":
                return BULLET._762MMX39;
            case "none":
                return BULLET.NONE;
        }
		
		Debug.LogWarning( "Error!! Cannot covert AmmoName(" + name + ") to ID!!!");
        return BULLET.NONE;
	}

    public float GetMinAttack( int ID)
    {
        return entityWp.sheets [0].list [ID].minattack;
    }

    /// <summary>
    /// Find Weapon ID from weapon name.
    /// !! Warning !! It is a VERY HEAVY method. 
    /// </summary>
	public int FindIdFromWeaponName (  string Name  )
	{
		return wpDict.FindIdFromWeaponName(Name);
	}
	
    /// <summary>
    /// Load a model from some weapon prefabs.
    /// also, change a model's setting. 
    /// </summary>
	GameObject LoadModel (  string name, bool isPlayer)
	{
		GameObject clone;

        if (PhotonNetwork.offlineMode)
        {
            clone = Instantiate(Resources.Load<GameObject>(name + "/" + name), new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        }
        else
        {
            clone = PhotonNetwork.Instantiate(name + "/" + name, new Vector3(0, 0, 0), Quaternion.identity, 0);
        }

		clone.GetComponent<Rigidbody>().isKinematic = true;
		clone.GetComponent<Collider>().enabled = false;
		clone.tag = "PlayerWeapon";
		
		// if Time Bomb, damage_bang Component will be disable. 
        // It is The reason that the weapon model sets for using throw by default. 
        switch (name)
        {
            case "M26":
            case "M67":
                clone.GetComponent<DamageBang>().enabled = false;
                
                //Load Ordit Guide. 
                GameObject ordit = Instantiate(Resources.Load<GameObject>( "M26/OrditGuide"), clone.transform.position, Quaternion.identity) as GameObject;
                ordit.transform.SetParent( clone.transform);
                ordit.name = "OrditGuide";
                break;
            case "M18Claymore":
                Destroy( clone.transform.FindChild("DagerArea").gameObject);
                break;
        }

		// Default Setting : renderer is disable.
		if( isPlayer) clone.SetActive(false);

        if (!PhotonNetwork.offlineMode)
        {
            photonView.RPC("WeaponDisableOnNet", PhotonTargets.OthersBuffered, clone.GetPhotonView().viewID);
        }

		return clone;
	}

    /// <summary>
    /// Remove Weapon Data into a WeaponDataList.
    /// </summary>
    public void RemoveWeaponDataFromList( int index, int userID)
    {
        WeaponData wpD = WeaponDataList [index];

        // 消すためにアクティブ化する.
        wpD.model.SetActive(true);

        if (PhotonNetwork.offlineMode)
        {
            GameObject.Destroy(wpD.model);
        } 
        else
        {
            int id = wpD.model.GetPhotonView().viewID;

            PhotonNetwork.Destroy(wpD.model);
            photonView.RPC("RemoveWeaponDataFromListOnNet", PhotonTargets.Others, id);
        }

        playerWpCtr.ResetNowWeaponNo();
        weaponDataChacheList [userID] = null;
        WeaponDataList.Remove(wpD);
        UpdateAllWeaponType();
    }

    [RPC]// 他のユーザに武器の放棄を伝達.
    private void RemoveWeaponDataFromListOnNet( int modelViewID)
    {
        WeaponData wpD = EnemyWeaponDataList
                        .Where( wpd => wpd.model.GetPhotonView().viewID == modelViewID)
                        .FirstOrDefault();

        weaponDataChacheList [wpD.user] = null;
        EnemyWeaponDataList.Remove(wpD);
    }
	
	void  DisableCollider ()
	{
		foreach( WeaponData Weapon in WeaponDataList)
		{
			Weapon.model.GetComponent<Collider>().enabled = false;
		}
	}
	
	void  EnableCollider ()
	{
		foreach( WeaponData Weapon in WeaponDataList)
		{
			Weapon.model.GetComponent<Collider>().enabled = false;
		}
	}

    /* 
     *  Reset Weapon Process. 
     */
    public void ResetWeapon( WeaponData WpD)
    {
        WpD.nowammo = WpD.wpData.ammo;
        WpD.nowmagazine = WpD.wpData.magazine;
    }
	
	/* *********** */
	/* Accsessors. */
	/* *********** */
	public string Get_WeaponNameFromList (  int ID  )
	{
        return WeaponDataList[ID].wpData.name;
	}
	
	public int Get_WeaponListCount ()
	{
		return WeaponDataList.Count;
	}

    public int GetWeaponIDFromList (  int index  )
    {
        return WeaponDataList[index].wpData.id;
    }

    // Search using weapon from instanceID.
    public WeaponData GetNowUseWeaponFromID( int user, bool isPlayer)
    {

        if (isPlayer) // Player Process.
        {
            if( weaponDataChacheList[user] == null || weaponDataChacheList[user].model.activeSelf == false)
            {
                WeaponData wpD = WeaponDataList
                                 .Where( wpd => wpd.model.activeSelf == true)
                                 .Where( wpd => wpd.user == user)
                                 .FirstOrDefault();

                weaponDataChacheList[user] = wpD;
            }
            return weaponDataChacheList[user];
        } 
        else // Enemy Process.
        {
            RegisterNewUserWeapon(user);

            if( weaponDataChacheList[user] == null || weaponDataChacheList[user].model.activeSelf == false)
            {
                WeaponData wpD = EnemyWeaponDataList
                                .Where( wpd => wpd.model.activeSelf == true)
                                .Where( wpd => wpd.user == user)
                                .FirstOrDefault();
                
                weaponDataChacheList[user] = wpD;
            }
            return weaponDataChacheList[user];
        }
    }

    [RPC]
    public void RegistWeaponOnOnline( int parentViewID, int weaponViewID)
    {
        WeaponData WpD = new WeaponData();

        WpD.model = PhotonView.Find( weaponViewID).gameObject;

        // Set a parent ("[Chara]/Weapon") of this Gameobject.
        GameObject weaponHead = PhotonView.Find(parentViewID).gameObject;
        WpD.model.transform.SetParent(weaponHead.transform);
        WpD.user = weaponHead.GetInstanceID();

        EnemyWeaponDataList.Add(WpD);
        RegisterNewUserWeapon(WpD.user);

        return;
    }


    [RPC]// Send to Others.
    void WeaponDisableOnNet( int viewID)
    {
        PhotonView.Find( viewID).gameObject.SetActive(false);
    }
}