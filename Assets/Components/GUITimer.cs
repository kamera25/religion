﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections;

public class GUITimer : Photon.MonoBehaviour 
{

	public float limitTime = 0;
	public bool isStruct = false;

	private float time;
	public bool isStart = false;
	private bool isSet = false;
    private bool isInvisible = false;
	private DateTime dtEnd;

    [SerializeField] private Text textShadow;
    [SerializeField] private Text textMain;
    [SerializeField] private GameObject timerMain;

    public const int TIMER_MAX = 999999; 
    private bool isUse = true;

	// Use this for initialization
	void Awake() 
	{
        RefreshTimer(limitTime);

		this.name = "Timer";
        SetTimerStrings("");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if( !isStart) return;

		if( isStruct)
		{

            if( isInvisible)
            {
                SetTimerStrings( "");
            }
            else
            {
                TimeSpan dt = dtEnd - DateTime.Now;
                SetTimeStringAsStruct( dt);
            }

			if( dtEnd < DateTime.Now && isSet) TimeUp();
		}
		else
		{
			// Setting Timer strings.
            SetTimeString();
			time -= Time.deltaTime;
			if( time <= 0) TimeUp();
		}
	}

    private void SetTimeStringAsStruct( TimeSpan dt)
    {
        string nowTime = dt.Minutes.ToString("00") + ":" 
                       + dt.Seconds.ToString("00") + ":" 
                       + ((int)(dt.Milliseconds * 0.1F)).ToString("00");

        SetTimerStrings(nowTime);
    }

    private void SetTimeString()
    {
        string timerStr = TimerString();
        SetTimerStrings(timerStr); 
    }  

    private void SetTimerStrings( string str)
    {
        if (isUse)
        {    
            textShadow.text = str;
            textMain.text = str;
        } 
        else
        {
            textShadow.text = "Limitless";
            textMain.text = "Limitless"; 
        }


    }

	private string TimerString()
	{
		string minute 		= Mathf.Floor( time / 60).ToString("00");
		string second 		= Mathf.Floor( time % 60).ToString("00");
		string milliSecond 	= ( time % 1 * 99).ToString("00");

		return minute + ":" + second + ":" + milliSecond;
	}

	private void TimeUp()
	{
        GameObject.FindWithTag("GameController").SendMessage("TimeUp");
		
		// Disable my gameobject.
		this.gameObject.SetActive(false);
	}
	
    private void RefreshTimer( float time)
    {
        this.time = time;
        SetTimeString();
    }

    public void SetTime( float t)
    {
        time = t;

        // もし時間制限が無限大なら.
        if (t == TIMER_MAX)
        {
            isUse = false;
        }
    }

	public void StartTimer()
	{
	
        if (PhotonNetwork.offlineMode)
        {
            isStart = true;
        }
        else
        {
	    	//Time Setting
		    dtEnd = DateTime.Now;
	    	dtEnd = dtEnd.AddSeconds( time);
	    	photonView.RPC( "SetTimeAsStruct", PhotonTargets.AllBuffered, dtEnd.ToString(), time);
        }
	}

    [RPC]// Others
    public void SetTimeAsStruct( string t, float time)
    {
        dtEnd = DateTime.Parse( t);
        isSet = true;
        isStruct = true;
        isStart = true;

        if (time < 0)
        {
            isInvisible = true;
        } 

    }
}
