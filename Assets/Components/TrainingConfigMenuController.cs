﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TrainingConfigMenuController : MonoBehaviour 
{
    public Transform content;
    FlagManager flagManager;
    TrainingController trainingCtr;

    bool forbidFlag = true;

	// Use this for initialization
	void Start () 
    {
        GameObject controller;

        controller = GameObject.FindWithTag("GameController");
        if (controller == null)
        {
            Debug.LogError("No finding controller.");
            this.gameObject.SetActive(false);
            return;
        }

        flagManager = controller.GetComponent<FlagManager>();
        trainingCtr = controller.GetComponent<TrainingController>();

        InitializeFlagForToggle();
    }


    // Get some flags from FlagManager(GameController), put togglemark.
    void InitializeFlagForToggle()
    {
        // Loop. travase Content's childs.
        foreach (Transform trans in content)
        {
            // トグルボタンに付加されている、SendMessageAsButtonにそれぞれのoptionに参照するキーがある.
            string key = trans.GetComponent<SendMessageAsButton>().option;
            bool flag = (bool)flagManager.flagDictionary[key];

            if( !flag)
            {
                trans.GetComponent<Toggle>().isOn = false;
            }
        }

        forbidFlag = false;
    }

    // Set some flags to FlagManager(GameController) from reciving some toggle(SendMessageAsButton).
    void SetFlagForToggle( string key)
    {
        if (forbidFlag)
        {
            return;
        }

        flagManager.flagDictionary[key] = !(bool)flagManager.flagDictionary[key];

        // Send changing training setting.
        flagManager.flagDictionary ["Change_Training_Setting"]  = true;
        trainingCtr.ImmediateApply();
    }

}
