﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponDictionary : MonoBehaviour
{
    static Dictionary<string, int> wpDictionary = new Dictionary<string, int>();

    void Awake()
    {
        GetWeaponDictionary();
    }

    // Singleton.
    public void GetWeaponDictionary()
    {
        Entity_weapondata entityWp = Resources.Load<Entity_weapondata>("data/Weapon/weapon");

        if (wpDictionary.Count == 0)
        {
            Entity_weapondata.Sheet wpSheet = entityWp.sheets[0];
            for (int i = 0; i < wpSheet.list.Count; i++)
            {
                string name = wpSheet.list[i].name;
                wpDictionary [name] = i;
            }
        }

        return;
    }
	
    public int FindIdFromWeaponName( string wpName)
    {
        if( wpDictionary.ContainsKey(wpName))
        {
            return wpDictionary [wpName];
        }

        Debug.LogWarning("FindIdFromWeaponName : No such a weapon! - " + wpName);

        return -1;
    }
}
