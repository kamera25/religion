﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerNetworkControl : Actor
{
    public TEAM team ;
    public string userName = "anonymous";

	private Vector3 nowPlayerPos = Vector3.zero;
	private Quaternion nowPlayerRot = Quaternion.identity;
	private Animator animator;
    private GUIStyle style;
    private StatusControl statusCtr;

	private float AimX = 0F;
	private float AimY = 0F;
    private float Horizontal = 0F;
    private float Vertical = 0F;
    private float putNameTime = 10F;
    private TEAM playerTeam = TEAM.NONE;

    void OnGUI()
    {

        if( photonView.isMine) return;

        TEAM pTeam = GetPlayerTeam();

        if ( pTeam == team || 0F < putNameTime )
        {
            Vector3 textPos = Camera.main.WorldToScreenPoint( this.transform.position);

            // Bug Avoidance?
            textPos.y = Screen.height - textPos.y;

            Rect rect = new Rect(textPos.x - 30F, textPos.y, 100, 100);
            ChangeStyleTeamColor(pTeam);

            GUI.Label(rect, userName, style);
        } 

    }

    void ChangeStyleTeamColor( TEAM team)
    {
        GUIStyleState styleState = new GUIStyleState();


        switch (team)
        {
            case TEAM.BLUE:
                styleState.textColor = new Color( 0F, 0F, 1F, 1F) ;
                break;
            case TEAM.RED:
                styleState.textColor = new Color( 1F, 0F, 0F, 1F) ;
                break;
            case TEAM.GREEN:
                styleState.textColor = new Color( 0F, 1F, 0F, 1F) ;
                break;
            case TEAM.YELLOW:
                styleState.textColor = new Color( 0F, 0.6F, 0.6F, 1F) ;
                break;
        }

        style.normal = styleState;
    }


	// Use this for initialization
	void Start () 
	{
        style = new GUIStyle();
        style.fontStyle = FontStyle.Bold;

        animator = GetAnimator();
        statusCtr = this.GetComponent<StatusControl>();

		if ( PhotonNetwork.offlineMode)
		{
			this.enabled = false;
		}

        //自分自身のPlayerじゃなければ.
        if (!this.GetComponent<PhotonView>().isMine)
        {
            this.gameObject.layer = LayerMask.NameToLayer("Default");
            this.gameObject.tag = "Enemy";
        }
	}
	
	// Update is called once per frame
	void Update () 
    {

        if (statusCtr.isDead)
        {
            putNameTime = 0F;
        }
        putNameTime -= Time.deltaTime;

		if( photonView.isMine) return; 

		transform.position = Vector3.Lerp( transform.position, nowPlayerPos, Time.deltaTime * 5);
		transform.rotation = Quaternion.Lerp( transform.rotation, nowPlayerRot, Time.deltaTime * 5);

		animator.SetFloat( "AimX", Mathf.Lerp( animator.GetFloat("AimX"), AimX, Time.deltaTime * 5));
		animator.SetFloat( "AimY", Mathf.Lerp( animator.GetFloat("AimY"), AimY, Time.deltaTime * 5));
        animator.SetFloat( "Horizontal", Mathf.Lerp( animator.GetFloat("Horizontal"), Horizontal, Time.deltaTime * 5));
        animator.SetFloat( "Vertical", Mathf.Lerp( animator.GetFloat("Vertical"), Vertical, Time.deltaTime * 5));

	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)    
	{
        // Player(=animator)が登録されていなければ.
        if (animator == null)
        {
            return;
        }

		if( stream.isWriting)// Caster.
		{
			stream.SendNext( transform.position);
			stream.SendNext( transform.rotation);

			// Mecanim's sync.
			stream.SendNext( animator.GetFloat("AimX"));
			stream.SendNext( animator.GetFloat("AimY"));
            stream.SendNext( animator.GetFloat("Horizontal"));
            stream.SendNext( animator.GetFloat("Vertical"));
            stream.SendNext( animator.GetFloat("reloadSpeed"));

            stream.SendNext( animator.GetBool("isDead"));
            stream.SendNext( animator.GetBool("useLadder"));
            stream.SendNext( animator.GetBool("onAir"));
            stream.SendNext( animator.GetBool("isDash"));
            stream.SendNext( animator.GetBool("isSquat"));
            stream.SendNext( animator.GetInteger("weaponKind"));
            stream.SendNext( animator.GetBool("reload"));
            stream.SendNext( animator.GetBool("fire"));
            stream.SendNext( animator.GetBool("isAvoid"));
            stream.SendNext( animator.GetInteger("charaID"));
		}
		else// Reciver
		{
			nowPlayerPos = (Vector3)stream.ReceiveNext();
			nowPlayerRot = (Quaternion)stream.ReceiveNext();
			
            // Mecanim's Sync.
			AimX = (float)stream.ReceiveNext();
			AimY = (float)stream.ReceiveNext();
            Horizontal = (float)stream.ReceiveNext();
            Vertical = (float)stream.ReceiveNext();
            animator.SetFloat( "reloadSpeed", (float)stream.ReceiveNext());

            animator.SetBool( "isDead", (bool)stream.ReceiveNext());
            animator.SetBool( "useLadder",(bool)stream.ReceiveNext());
            animator.SetBool( "onAir", (bool)stream.ReceiveNext());
            animator.SetBool( "isDash", (bool)stream.ReceiveNext());
            animator.SetBool( "isSquat", (bool)stream.ReceiveNext());
            animator.SetInteger( "weaponKind", (int)stream.ReceiveNext());
            animator.SetBool( "reload", (bool)stream.ReceiveNext());
            animator.SetBool( "fire", (bool)stream.ReceiveNext());
            animator.SetBool( "isAvoid", (bool)stream.ReceiveNext());
            animator.SetInteger( "charaID", (int)stream.ReceiveNext());
		}
	}

    void DamageReciver( float damage)
    {
        putNameTime = 6F;
    }

    // チームを取得する.
    TEAM GetPlayerTeam()
    {
        if (playerTeam == TEAM.NONE)
        {
            PhotonPlayer player = PhotonNetwork.player;
            playerTeam = (TEAM)Enum.Parse( typeof(TEAM), player.GetTeam().ToString());
        }
        
        return playerTeam;
    }

}
