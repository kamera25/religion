﻿using UnityEngine;
using System.Collections;

public class ChangeWeaponControl : MonoBehaviour 
{

    private WeaponDataProcess WpDataProc;
    private LoadPlayersWeapon loadPlyWp;
    private int wpHeadID;

    [SerializeField] private GameObject cannotEquipDialog;

    void Start()
    {
        WpDataProc = GameObject.FindWithTag("GameController").GetComponent<WeaponDataProcess>();

        Transform wpHead = GameObject.FindWithTag("Player").transform.FindChild("Weapon");
        loadPlyWp = wpHead.GetComponent<LoadPlayersWeapon>();
        wpHeadID = wpHead.gameObject.GetInstanceID();
    }

    public void DropWeapon( string index)
    {
        WpDataProc.RemoveWeaponDataFromList( int.Parse(index), wpHeadID);
        this.GetComponent<MyWeaponListControl>().UpdateList();
    }

    public void EquipWeapon( string wpName)
    {
        bool canAdd = WpDataProc.CheckAddingWeaponTypeFromName(wpName);
        if (!canAdd)
        {
            cannotEquipDialog.SetActive(true);
            this.GetComponent<MyWeaponListControl>().UpdateList();
            return;
        }

        loadPlyWp.LoadWeaponDataToList( wpName);
        this.GetComponent<MyWeaponListControl>().UpdateList();
    }


}
