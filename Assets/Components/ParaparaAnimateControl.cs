﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParaparaAnimateControl : MonoBehaviour {

	public List<Sprite> warmImg = new List<Sprite>();
	public float changeTime = 0.2F;
	public float dumping = 0.005F;

	private SpriteRenderer sprite;
	private Vector3 distPos;
	private Vector3 originPos;
	private int nowId = 0;
	private float time;

	// Use this for initialization
	void Start () 
	{
		sprite = this.GetComponent<SpriteRenderer>();
		time = changeTime;
		originPos = this.transform.position;
        distPos = new Vector3( Random.Range( -3F, 3F), Random.Range( -3F, 3F), Random.Range( -3F, 3F));
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (time < 0F) 
		{
			if( warmImg.Count - 1 == nowId)
			{
				nowId = 0;
			}
			else
			{
				nowId++;
			}
			sprite.sprite = warmImg[nowId];
			distPos = new Vector3( Random.Range( -3F, 3F), Random.Range( -3F, 3F), Random.Range( -3F, 3F));
			time = changeTime;
		}
		
		this.transform.Translate( distPos * Time.deltaTime + ( originPos - this.transform.position) * dumping);
		//this.transform.eulerAngles = pos;

		time -= Time.deltaTime;
	}
	
}
