﻿using UnityEngine;
using System.Collections;

public class StatusControl : BrunchArmyStatus
{
    public int armyKind = 0;
    public float hp ;
    public float stamina ;
    public float recoveryStamina = 50;
    public bool isDead = false ;

    private float staminaSpeedRate = 1F;
    private float staminaSpeedTime = 0F;

    private const int ADRENALINE_ID = 4;
    private const float ADRENALINE_LIFE = 30F;

    private Animator anim;
    private string firstMyTag = null;

    private int enemyLayer = 0;

    private WeaponChangeControl wpChangeCtr;
    private WeaponControl wpCtr;

    private bool isPlayer;

    /*
	public enum SKILL
	{
		DEATHMATCH2 = 0,
		DEATHMATCH4,
		POLICEVSTHIEF,
		FLAGBATTLE,
		ONLYWEAPON,
		MONSTERTEAMKILL,
		BOMBDISMANTLING
	}*/


	// Use this for initialization
	void Start () 
	{
        // if not use army data...
        if (armyKind != 0)
        {
            LoadArmyDataFromXML(armyKind);
        } 
        else
        {
            myData.attack = 100F;
        }
        
        hp = myData.maxhp;
        stamina = myData.stamina;

        UpdateIsPlayer();

        // if this is enemy.
        if (isPlayer)
        {
            GameObject controller = GameObject.FindWithTag("GameController");
            wpChangeCtr = controller.GetComponent<WeaponChangeControl>();
            wpCtr = controller.GetComponent<WeaponControl>();
        } 
        else
        {
            enemyLayer = LayerMask.NameToLayer("DeadEnemyLayer");
        }

        anim = GetAnimator();
	}
	
	// Update is called once per frame
	void Update () 
    {
        SlowlyRecoverStamina();

		hp = Mathf.Clamp( hp, 0.0f, myData.maxhp);
		stamina = Mathf.Clamp( stamina, 0.0f, myData.stamina);

        if (hp <= 0 && !isDead )
        {
            if( !CheckHavingAdrenaline())
            {
                DeadProcess();
            }
        }
	}

    private void DeadProcess()
    {
        isDead = true;
        UpdateIsPlayer();
        firstMyTag = this.gameObject.tag;

        anim.SetTrigger("isDead");
        if( !this.CompareTag("EnemyNoMan"))
        {
            anim.SetLayerWeight( 1, 0F);
            anim.SetLayerWeight( 2, 0F);
        }

        if (isPlayer)
        {
            wpChangeCtr.EnableChangeLock();
            wpCtr.enabled = false;
        }
        else
        {
            this.gameObject.tag = "DeadEnemy";
            this.gameObject.layer = enemyLayer;
        }

        return;
    }

    private bool CheckHavingAdrenaline()
    {
        if( isPlayer)
        {
            ItemControl itemCtr = GameObject.FindWithTag("GameController").GetComponent<ItemControl>();
            if( itemCtr.UseRecovery(ADRENALINE_ID))
            {
                RecoveryHPUsingRate( ADRENALINE_LIFE);
                itemCtr.gameObject.SendMessage( "AddLogMesseage", "アドレナリンを利用して、蘇生しました。");
                return true;
            }
        }
    
        return false;
    }

    private void SlowlyRecoverStamina()
    {
        stamina += recoveryStamina * Time.deltaTime * staminaSpeedRate;
        if(staminaSpeedTime < 0F)
        {
            staminaSpeedRate = 1F;
        }

        staminaSpeedTime -= Time.deltaTime;
    }

	public void DamageReciver( float damage)
	{
        DamageApply( damage);

        if ( !PhotonNetwork.offlineMode && !photonView.isMine)
        {
            photonView.RPC("DamageApply", PhotonTargets.Others, damage);
        }
    }

    [RPC]
    public void DamageApply( float damage)
    {
        hp -= damage;
        hp = Mathf.Clamp(hp, 0.0f, myData.maxhp);
    }

	public void StunReciver( float damage)
	{
		stamina -= damage;
		stamina = Mathf.Clamp( stamina, 0.0f, myData.stamina);
	}

	public bool isLeaveStamina( float damage)
	{
		return stamina > damage;
	}

	public void RecoveryAllStatus()
	{
        if (PhotonNetwork.offlineMode)
        {
            RestartAnimator( myData.stamina, myData.maxhp);
        } 
        else
        {
            photonView.RPC("RestartAnimator", PhotonTargets.All, myData.stamina, myData.maxhp);
        }

        if (isPlayer)
        {
            wpCtr.enabled = true;
            wpChangeCtr.DisableChangeLock();
        }
    }

    public void RecoveryHPUsingRate( float rate)
    {
        hp += myData.maxhp * rate / 100;
    }

    public void RecoveryStaminaSpeedUp( float rate, float time)
    {
        staminaSpeedRate = staminaSpeedRate * ( 100F + rate) / 100F; 
        staminaSpeedTime = time;
    }

    public void StopRecoveryStaminaSpeed()
    {
        staminaSpeedRate = 1F;
        staminaSpeedTime = 0F;
    }

    [RPC]
    void RestartAnimator( float hp, float stamina)
    {
        if (anim != null)
        {
            anim.SetTrigger("isRestart");
            if (firstMyTag != "EnemyNoMan")
            {
                anim.SetLayerWeight(1, 1F);
                anim.SetLayerWeight(2, 1F);
            }
        }

        if( firstMyTag != null)
        {
            this.gameObject.tag = firstMyTag;
        }

        this.hp = hp;
        this.stamina = stamina;
        isDead = false;
    }

    public bool IsStaminaSpeedUp()
    {
        return 0F < staminaSpeedTime;
    }

    void UpdateIsPlayer()
    {
        isPlayer = this.CompareTag("Player");
    }
}
