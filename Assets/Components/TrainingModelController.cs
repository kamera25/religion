using UnityEngine;
using System.Collections;

public class TrainingModelController : Actor
{

    private TrainingController trainingCrl;
    private StatusControl statusCtr;
    private Animator anim;
    private AIBehavior aiBhav;

    private float deadTime = 0F;

    public bool isMove = true;

    void Awake()
    {
        statusCtr = this.GetComponent<StatusControl>();
        aiBhav = this.GetComponent<AIBehavior>();
    }

    void Start()
    {
        trainingCrl = GameObject.FindWithTag("GameController").GetComponent<TrainingController>();
        if( trainingCrl == null)
        {
            Debug.LogWarning("Not training mode now.");
        }

        anim = GetAnimator();
    }

    void Update()
    {
        if (aiBhav.PeekImmediate() == EXPLICIT.ARRIVE_POINT)
        {
            aiBhav.RandamSetNextAimPoint();
        }

        if (statusCtr.isDead)
        {
            deadTime += Time.deltaTime;
            if( 3F < deadTime)
            {
                ResetModel();
            }
        }

        if (isMove)
        {
            aiBhav.RunAI();
        } 
        else
        {
            aiBhav.StopAI();
        }
    }
   
    public void ResetModel()
    {
        statusCtr.RecoveryAllStatus();
        aiBhav.RespawnRandamPositon();
        aiBhav.RandamSetNextAimPoint();
        aiBhav.RebornAI();
        deadTime = 0F;
    }

    public void DamageReciver( float damage)
    {
        if (!statusCtr.isDead)
        {
            anim.SetTrigger("isHit");
            trainingCrl.DetectDamage(damage, this.transform.position);
        }
    }
}
