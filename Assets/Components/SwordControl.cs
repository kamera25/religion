﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SwordControl : MonoBehaviour 
{
    private List<int> HitEnemyList = new List<int>();
    private float time = 0F;
    private float damage = 600F;
    private bool isCheck = false;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {

        if( isCheck && time < 0F)
        {
            Reset();
        }

        time -= Time.deltaTime;
	}

    void Reset()
    {
        HitEnemyList.Clear();
        isCheck = false;
    }

    void CheckStart( float t)
    {
        isCheck = true;
        time = t;
    }

    // When hit gameobject
    public void HitTriggerProc( GameObject obj)
    {
        if ( !obj.CompareTag("Stage") && !obj.CompareTag("Player"))
        {
            int id = obj.GetInstanceID();

            if( CheckAlreadyHit( id))
            {
                HitEnemyList.Add( id);
                obj.SendMessage( "DamageReciver", damage);
            }
        }
    }

    // Search list, check already hit?
    bool CheckAlreadyHit( int objID)
    {
        return !HitEnemyList
            .Any(id => id == objID);
    }

}
