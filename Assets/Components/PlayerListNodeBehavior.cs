﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerListNodeBehavior : MonoBehaviour 
{
    Image teamSprite;
    Text nameText;
    Text pointText;


	// Use this for initialization
	void Awake()
    {
        teamSprite = this.transform.FindChild("TeamImage").GetComponent<Image>();
        nameText = this.transform.FindChild("PlayerNameText").GetComponent<Text>();
        pointText = this.transform.FindChild("KillNumText").GetComponent<Text>();
    }

    public void SetTeam( TEAM team)
    {
        teamSprite.sprite = Resources.Load<Sprite>("ingame/RaderIcon/Landmark/BASECAMP_" + team.ToString());
    }
	
    public void SetName( string name)
    {
        nameText.text = name.ToString();
    }

	public void SetPoint( int point)
    {
        pointText.text = point.ToString();
    }
}
