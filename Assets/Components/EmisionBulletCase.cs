﻿using UnityEngine;
using System.Collections;

public class EmisionBulletCase : Photon.MonoBehaviour 
{

    public void EmmsionCase( Quaternion Q)
    {
        GameObject bulletCase;

        if (PhotonNetwork.offlineMode)
        {
            bulletCase = Instantiate( Resources.Load<GameObject>("bullet/M4BulletCase"), this.transform.position, Q) as GameObject;
        } 
        else
        {
            bulletCase = PhotonNetwork.Instantiate( "bullet/M4BulletCase", this.transform.position, Q, 0);
        }

        Rigidbody rig = bulletCase.GetComponent<Rigidbody>();
        rig.AddForce( - this.transform.up * 20F - this.transform.right * 18F - this.transform.forward * 10F, ForceMode.Impulse);
        rig.AddTorque( Vector3.up * Random.Range( -6, 6), ForceMode.Impulse);
    }
}
