﻿using UnityEngine;
using System.Collections;

public class CollectFlagControl : Photon.MonoBehaviour 
{

	ItemControl itemCtl;
	GameObject controller;
	OnlineModeControl onlineCtr;
    //public TEAM team ;

    BaseCampBehavior baseCampBhv;

	// Use this for initialization
	void Start () 
	{
		controller = GameObject.FindWithTag("GameController");
		itemCtl = controller.GetComponent<ItemControl>();

        baseCampBhv = this.transform.parent.GetComponent<BaseCampBehavior>();

		GameObject rader = GameObject.Find("Rader");
		if( rader != null)
		{
			rader.GetComponent<RaderControl>().RegisterElementWithRader( this.transform, GetRaderElementByTeam());
		}

        // Beta!!! Put LandmarkIndicator.
        GameObject playModeUI = GameObject.Find("PlaymodeUI");
        GameObject indicator = Instantiate<GameObject>( Resources.Load<GameObject>( "unit/LandmarkIndicator"));
        indicator.transform.SetParent( playModeUI.transform);
        LandmarkIndicatorBehavior landmarkBhav = indicator.GetComponent<LandmarkIndicatorBehavior>();
        landmarkBhav.landmark = GetRaderElementByTeam();
        landmarkBhav.landmarkPos = this.transform.position;

	}

	RADERELEMENT GetRaderElementByTeam()
	{		                                                       
		switch( baseCampBhv.team)
		{
			case TEAM.RED:
					return RADERELEMENT.BASECAMP_RED;
			case TEAM.BLUE:
					return RADERELEMENT.BASECAMP_BLUE;
			case TEAM.GREEN:
					return RADERELEMENT.BASECAMP_GREEN;
			case TEAM.NONE:
			case TEAM.YELLOW:
					return RADERELEMENT.BASECAMP_YELLOW;
		}

		Debug.LogWarning( "Basecamp dosen`t set radericon by team.");
		return RADERELEMENT.NONE;
	}

	void OnTriggerEnter( Collider Col) 
	{

		if( PhotonNetwork.offlineMode) return;

		int flag = 0;

		/* if touch Player Base. */
		if (Col.gameObject.CompareTag("Player"))
        {
            TEAM colTeam = Col.GetComponent<PlayerNetworkControl>().team;
            if (colTeam != baseCampBhv.team)
                return;

            flag = itemCtl.RemoveIDFromKey(0);
            onlineCtr = GameObject.FindWithTag("NetworkController").GetComponent<OnlineModeControl>();
            onlineCtr.photonView.RPC("SetFlagPoint", PhotonTargets.All, (int)colTeam, flag);
            if (flag != 0)
            {
                controller.SendMessage("AddLogOnNet", colTeam.ToString() + " Team +" + flag + " GET!!!");

                // Beta!! フラグポイントの追加を通知する.
                PhotonPlayer photonPly = PhotonNetwork.player;
                photonPly.SetScore( photonPly.GetScore() + flag);
            }
        }
	}


}
