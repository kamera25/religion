﻿using UnityEngine;
using System.Collections;

public class EnemyRespawon : MonoBehaviour 
{
	public int enemyCount; 

	// Use this for initialization
	void Start () 
    {

		GameObject[] resPoint = GameObject.FindGameObjectsWithTag("Respawn"); // Get respawn points.


		for( int i = 0; i < enemyCount; i++)
		{
			GameObject clone = Instantiate( Resources.Load<GameObject>("unit/CapsuleBoy"), resPoint[ Random.Range( 0, resPoint.Length)].transform.position, Quaternion.identity) as GameObject;	
			clone.transform.SetParent( this.transform);
		}
	}
}
