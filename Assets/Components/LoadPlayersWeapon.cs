﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class LoadPlayersWeapon : MonoBehaviour
{
	public List<string> Weapons= new List<string>();
    WeaponDataProcess WpDataProc;

	// Use this for initialization
	void Start () 
	{
        // Get is this created or not by me?
        bool isMine = this.GetComponent<PhotonView>().isMine;
		if( !isMine) return;

        WpDataProc = GameObject.FindWithTag("GameController").GetComponent<WeaponDataProcess>();
		foreach( string weapon in Weapons)
		{
            LoadWeaponDataToList( weapon);
        }

        return;
	}

    public void LoadWeaponDataToList( string weapon)
    {
        WpDataProc.SetWeaponDataToList( weapon, this.transform, true, this.gameObject.GetInstanceID());
    }
}
