﻿using UnityEngine;
using System.Collections;

public class MuzzleFlashControl : Photon.MonoBehaviour 
{
	public GameObject explosion = null;

	public void PutMuzzleFlash()
	{
        if (PhotonNetwork.offlineMode)
        {
            Instantiate(explosion, this.transform.position, Quaternion.identity);
        } 
        else
        {
            PhotonNetwork.Instantiate( "Explosion/" + explosion.name, this.transform.position, Quaternion.identity, 0);
        }
    }
}
