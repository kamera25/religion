﻿using UnityEngine;
using System.Collections;

public class GoToScene : MonoBehaviour 
{
	private bool isTransision = false;
    public string scene;

    private void StartTransision()
    {
        if (isTransision)
            return;

        Camera.main.SendMessage( "fadeOut");
        Invoke( "GoToOnline", 3F);
        isTransision = true;
    }

	private void GoToOnline()
	{
		Application.LoadLevel( scene);
	}
}
