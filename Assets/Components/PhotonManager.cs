﻿using UnityEngine;
using System.Collections;


public class PhotonManager : Photon.MonoBehaviour 
{

    [SerializeField] bool isDebug = false;
    private GameObject cannotConnectUI;

    NetworkMenuControl networkMenuCtr;

	private GameObject controller;
	private OnlineModeControl onlineModeCtr;

    public const string version = "develop1"; // version is using for Networking. If change code, rewrite this string. 
    private string userName;
    private int ticket;

    void OnDisable()
    {
        PhotonNetwork.Disconnect();
    }

	// Use this for initialization
	void Start () 
	{
        controller = GameObject.FindWithTag("GameController");
        onlineModeCtr = this.GetComponent<OnlineModeControl>();
        networkMenuCtr = this.GetComponent<NetworkMenuControl>();

        Transform networkUI = GameObject.Find("NetworkUI").transform;
        cannotConnectUI = Resources.Load<GameObject>("Dialog/ConnectErrorDialog");



        if (isDebug)
        {   
            PhotonNetwork.ConnectUsingSettings(version);
            OnJoinedRoom();
        } 
    }


    public void CreateRoomFromButton( string name, BATTLE mode, STAGE stage, string userName, int ticket, int time)
    {
        RoomOptions roomOption = new RoomOptions();

        roomOption.isOpen = true;
        roomOption.isVisible = true;
        roomOption.maxPlayers = 4;
        roomOption.customRoomProperties = new ExitGames.Client.Photon.Hashtable ()
        { 
            {"onlineMode", mode},
            {"Stage", stage}
        };
        roomOption.customRoomPropertiesForLobby = new string[]
        {
            "onlineMode",
            "Stage"
        };

        PhotonNetwork.JoinOrCreateRoom(name, roomOption, TypedLobby.Default);

        PhotonNetwork.playerName = userName;
        onlineModeCtr.ticket = ticket;
        onlineModeCtr.time = time;

        Debug.Log("OK " + name + " is made.");
    }

    void OnDisconnectedFromPhoton()
    { 
        Instantiate<GameObject>( cannotConnectUI);
    }

	void OnJoinedLobby()
	{
        if (isDebug)
            return;

        controller.SendMessage("AddLogMesseage", "Join lobby.");
        //PhotonNetwork.JoinRandomRoom();
	}

	void OnPhotonRandomJoinFailed()
	{
        controller.SendMessage( "AddLogMesseage", "fail to join room.");
		PhotonNetwork.CreateRoom("testRoom");
	}

    void OnPhotonPlayerDisconnected(PhotonPlayer player)
    { 
        controller.SendMessage( "AddLogMesseage", player.name + "さんが退出しました。");
        if( networkMenuCtr.IsPutNetworkMenuUI())
        {
            networkMenuCtr.UpdateData();
        }
    }

    void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        if( newMasterClient == PhotonNetwork.player)
        {
            onlineModeCtr.SetProcessOnlineModeForMC();
        }
    }

    // 誰かが新しく入った時に呼び出される.
    void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        networkMenuCtr.UpdateData();
    }

    // 得点に変更があった時に呼び出される.
    void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps)
    {
        networkMenuCtr.UpdateDataOnly();
    }

	void OnJoinedRoom()
	{
        onlineModeCtr.battleMode = (BATTLE) int.Parse( PhotonNetwork.room.customProperties ["onlineMode"].ToString());

        GameObject clone = PhotonNetwork.Instantiate( "unit/Player", Vector3.zero, Quaternion.identity,0);
		onlineModeCtr.NetworkStart( clone);


        controller.SendMessage( "AddLogMesseage", "フィールドにログイン出来ました。");
        controller.SendMessage( "AddLogOnNet", PhotonNetwork.playerName + "さんが参加しました。");
        controller.SendMessage( "AddLogMesseage", "参戦者は全員で、" + PhotonNetwork.room.playerCount + "人です。");

		// Set GameStartTimer.
        NetworkTimerBehavior netTimer = this.GetComponent<NetworkTimerBehavior>();
        netTimer.enabled = true;
        if ( !PhotonNetwork.isMasterClient)
        {
            netTimer.SyncTimer();
        }

        networkMenuCtr.UpdateData();


	}



}
