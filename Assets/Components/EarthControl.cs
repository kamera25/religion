﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PlaceData
{
    public PLACE place;
    public Vector3 direction;
}

public enum PLACE
{
    NONE = 0,
    JAPAN,
    HAWAII,
    CALIFORNIA
};

public class EarthControl : MonoBehaviour 
{
    public List<PlaceData> PlaceList = new List<PlaceData>();
    public float dump = 5F;
    public float lookAtDump = 0.1F;
    public PLACE aimPlace = PLACE.NONE; 
    public Vector3 direction;
    public GameObject placePointer;

    private PLACE beforePlace = PLACE.NONE;

	// Use this for initialization
	void Start () 
    {
	   // Test Data!!!!
        PlaceData plD1 = new PlaceData();

        //
        plD1.place = PLACE.JAPAN;
        plD1.direction = new Vector3(-17.4F, -27.2F, 33.6F);
        PlaceList.Add(plD1);

        PlaceData plD2 = new PlaceData();
        plD2.place = PLACE.CALIFORNIA;
        plD2.direction = new Vector3(-23.1F, 88.1F, -27.6F);
        PlaceList.Add(plD2);
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (beforePlace != aimPlace)
        {
            direction = GetDirectionFromPlace( aimPlace);
            if( aimPlace != PLACE.NONE)
            {
                placePointer.SetActive(true);
            }
            else
            {
                placePointer.SetActive(false);
            }
        }

        if (aimPlace != PLACE.NONE)
        {
            this.transform.rotation = Quaternion.Lerp( this.transform.rotation, Quaternion.Euler(direction), Time.deltaTime * lookAtDump);
        } 
        else
        {
            this.transform.Rotate( 0F, Time.deltaTime * -dump, 0F);
        }

        beforePlace = aimPlace;
	}


    Vector3 GetDirectionFromPlace( PLACE place)
    {
        if (place == PLACE.NONE)
        {   
            return Vector3.zero;
        }

        Vector3 vec = PlaceList
                        .Where(plD => plD.place == place)
                        .Select(plD => plD.direction)
                        .FirstOrDefault();

        if (vec == null)
        {
            Debug.LogWarning("No such a Place!!!! --- EarthControl.cs");
            return Vector3.zero;
        }

        return vec;
    }

    public void SetAimPlace( PLACE place)
    {
        aimPlace = place;
    }
}
