﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NovelController : MonoBehaviour 
{
    enum NOVELMODE
    {
        NORMAL,
        TIMESTOP
    }

    public string NovelTitle = "";
    private Entity_Novel_Training1 entityNovel;
    private int novelPage = 0;

    private int talkNowCharaNum = 0;
    public float time = 0;
    private string nowTalkText = "";
    private float putOneCharaTime = 0.05F;

    [SerializeField] Text charaName;
    [SerializeField] Text talkText;
    [SerializeField] Image novelArrow;
    [SerializeField] NOVELMODE novelMode;

	// Use this for initialization
	void Start () 
    {
	    if (NovelTitle == "")
        {
            Debug.LogWarning("NovelController : No input novel title!!");
            this.enabled = false;
            return;
        }
        entityNovel = Resources.Load<Entity_Novel_Training1>( "data/Novel/" + NovelTitle);

        talkText.text = "";
        UpdateNovelIndication();

        if (novelMode == NOVELMODE.TIMESTOP)
        {
            Time.timeScale = 0F;
        }

	}
	
	// Update is called once per frame
	void Update () 
    {
        if (putOneCharaTime < time)
        {
            UpdateTalkText();
        }

        if (novelMode == NOVELMODE.NORMAL)
        {
            time += Time.deltaTime;
        } 
        else
        {
            time += 0.01F;
        }

    }



    void UpdateTalkText()
    {
        if (talkText.text != nowTalkText)
        {
            int charaNum = (int)(time / putOneCharaTime);
            talkText.text = nowTalkText.Substring(0, charaNum);
        } 
        else
        {
            novelArrow.gameObject.SetActive(true);
        }
    }


    void UpdateNovelIndication()
    {
        Entity_Novel_Training1.Param novel = entityNovel.sheets [0].list[novelPage];

        if (novel.name == "END")
        {
            DestroyNovel();
            return;
        }

        charaName.text = novel.name;

        // talk text.
        nowTalkText = novel.words.Replace( "\\n", "\n"); // NewLine charactor replacing process.

        // Arrow process.
        novelArrow.gameObject.SetActive(false);
    }

    private void DestroyNovel()
    {
        GameObject.Destroy(this.transform.parent.gameObject);
    }

    public void PushNextButton()
    {
        novelPage++;
        time = 0;
        talkNowCharaNum = 0;
        UpdateNovelIndication();
    }
}
