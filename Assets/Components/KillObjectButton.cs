﻿using UnityEngine;
using System.Collections;

public class KillObjectButton : MonoBehaviour 
{
    public GameObject obj;
	
    public void PushButton()
    {
        GameObject.Destroy(obj);
    }
}
