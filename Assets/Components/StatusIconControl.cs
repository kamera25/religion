﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatusIconControl : Photon.MonoBehaviour 
{

    private StatusControl statusCtr;
    private Image statusImg;
    
    // Use this for initialization
    void Start () 
    {
        if ( !PhotonNetwork.offlineMode)
        {
            this.enabled = false;
            return;
        }

        statusImg = this.GetComponent<Image>();
        statusCtr = GameObject.FindWithTag("Player").GetComponent<StatusControl>();
    }
	
	// Update is called once per frame
	void Update () 
    {
        if( Time.frameCount % 5 != 0)
        {  
            return; 
        }

        if ( IsWarningStamina())
        {
            statusImg.enabled = true;
        } 
        else
        {
            statusImg.enabled = false;
        }
	}

    bool IsWarningStamina()
    {
        return (statusCtr.stamina / statusCtr.myData.stamina) < 0.3F;
    }
}
