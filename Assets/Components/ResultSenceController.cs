﻿using UnityEngine;
using System.Collections;

public class ResultSenceController : MonoBehaviour
{
    [SerializeField] string nextSence = "topmenu";

    void GoToNext()
    {
        Camera.main.SendMessage("fadeOut");
        GetComponent<AudioSource>().Play();
        Invoke("Transition", 3F);
    }
	
    void Transition()
    {
        Application.LoadLevel(nextSence);
    }
}
