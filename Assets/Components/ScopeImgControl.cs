﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScopeImgControl : MonoBehaviour 
{
	public Sprite transparent;
	private PoseModeControl poseCtr;
    private Image scopeImg;

	// Use this for initialization
	void Start () 
	{
		poseCtr = GameObject.FindWithTag("GameController").GetComponent<PoseModeControl>();
        scopeImg = this.GetComponent<Image>();
	}

	void Update()
	{
		if( poseCtr.PoseMode != POSE.SUBJECT && scopeImg.sprite != transparent)
		{
			scopeImg.sprite = transparent;
		}
	}

    public void SetTransparent()
    {
        if( scopeImg.sprite != transparent)
        {
            scopeImg.sprite = transparent;
        }
    }

    public void SetScopeImage( Sprite spt)
    {
        if( scopeImg.sprite != spt)
        {
            scopeImg.sprite = spt;
        }
    }
}
