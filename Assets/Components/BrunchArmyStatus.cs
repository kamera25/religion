﻿using UnityEngine;
using System.Collections;

public class BrunchArmyStatus : Actor
{
    private Entity_army armyDatas;
    public Entity_army.Param myData;

    void  Awake ()
    {
        /* Load ArmyData XML file */
        armyDatas = Resources.Load<Entity_army>("data/Army/brunchofarmy");
    }

    protected void LoadArmyDataFromXML( int index)
    {
        myData = armyDatas.sheets [0].list [index];

        return;
    }

}
