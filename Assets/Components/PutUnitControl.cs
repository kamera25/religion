﻿using UnityEngine;
using System.Collections;

public enum STAGEMODE
{
    NONE = 0,
    TRAINING = 1,
    ONLINE
}

public class PutUnitControl : MonoBehaviour 
{

	public bool isNetwork;
	public bool isPutRader; 
	public bool isPutTimer;
    public bool isNovel;
    public bool isJoypad = false;

    public static STAGEMODE stageMode = STAGEMODE.NONE;

	// Use this for initialization
	void Awake () 
	{

		// if you want to put rader...
		if( isNetwork)
		{
            //Application.LoadLevelAdditive( "FlagBattle");
            /*
			GameObject clone = Instantiate( Resources.Load<GameObject>("unit/PhotonController"), Vector3.zero, Quaternion.identity) as GameObject;
			clone.transform.parent = this.transform;
			clone.name = "PhotonController";*/
		}
		else
		{
			PhotonNetwork.offlineMode = true;
			foreach( GameObject obj in GameObject.FindGameObjectsWithTag("Player"))
			{
				obj.layer = 9;// Set Ignore Raycast.
			}

		}
		if( isPutRader)
		{
			GameObject clone = Instantiate( Resources.Load<GameObject>("unit/Rader"), Vector3.zero, Quaternion.identity) as GameObject;
			clone.transform.SetParent( this.transform);
		}
		if( isPutTimer)
		{
			PutTimer();
		}

        if (stageMode == STAGEMODE.TRAINING)
        {
            TrainingModeSetting();

            if (isNovel)
            {
                Application.LoadLevelAdditive("NovelScene");
            }
        }

       
	}
	
	public void PutTimer()
	{
		GameObject clone;

		if (PhotonNetwork.offlineMode)
        {
            clone = Instantiate<GameObject>(Resources.Load<GameObject>("unit/Timer"));
        } 
        else
        {
            clone = PhotonNetwork.InstantiateSceneObject( "unit/Timer", Vector3.zero, Quaternion.identity, 0, null);
        } 

		clone.name = "Timer";
	}


   private void TrainingModeSetting()
   {
        this.gameObject.AddComponent<TrainingController>();
        FlagManager flagManager = this.GetComponent<FlagManager>();

        // Initialize traning mode flags.
        flagManager.flagDictionary ["PUT_TARGET"]               = true;
        flagManager.flagDictionary ["PUT_MANTARGET"]            = true;
        flagManager.flagDictionary ["MOVE_MANTARGET"]           = true;
        flagManager.flagDictionary ["PUT_NORMALTARGET"]         = true;
        flagManager.flagDictionary ["MOVE_NORMALTARGET"]        = true;
        flagManager.flagDictionary ["PUT_LOG"]                  = true;
        flagManager.flagDictionary ["PUT_LOG_DAMAGE"]           = true;
        flagManager.flagDictionary ["PUT_LOG_DISTANCE"]         = true;
        flagManager.flagDictionary ["AUTO_HP_RECOVERY"]         = true;
        flagManager.flagDictionary ["AUTO_STAMINA_RECOVERY"]    = true;
        flagManager.flagDictionary ["AUTO_BULLET_RECOVERY"]     = true;

        flagManager.flagDictionary ["Change_Training_Setting"]  = true;
   }
}
