﻿using UnityEngine;
using System.Collections;

public class RespawnBehavior : MonoBehaviour {

	GameObject[] resPoint;

	// Use this for initialization
	void Awake ()
	{
		resPoint = GameObject.FindGameObjectsWithTag("Respawn");
	}


	
	public void RespawnItemSetting( GameObject obj)
	{
		SetRespawnPosition( obj);
        Transform env_DynTrans = GameObject.Find("Envioroment(Dynamic)").transform;
		obj.transform.SetParent( env_DynTrans);
	}

	public void SetRespawnPosition( GameObject obj)
	{
		obj.transform.position = SetRespawnPositionVec3();
	}

	public Vector3 SetRespawnPositionVec3()
	{
		return resPoint[ Random.Range( 0, resPoint.Length)].transform.position;
	}

}
