﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(PhotonView))]
public class GameoverControl : Photon.MonoBehaviour 
{

	private GameObject player;
	public bool isOnceNoHP = true;
    private bool startRespwan = false;
	
	private float time;

	private StatusControl statusCtr;
    private SetBaseCamp setBaseCamp;
	private OnlineModeControl onlineModeCtr;

    [SerializeField] private GameObject gameoverUI;
    [SerializeField] private GameObject playmodeUI;

    private NetworkMenuControl netMenuCtr = null;

	void  Start ()
	{
		setBaseCamp = GameObject.Find("BasePoints").GetComponent<SetBaseCamp>();

		if( player == null)
		{
			this.enabled = false;
			return;
		}

		GetStatusControl();
	}

	void  Update ()
	{

        // If not kill, go away.
        if( 0 < statusCtr.hp) return;

		// this point down,  
		if( PhotonNetwork.offlineMode && isOnceNoHP)
		{
			Camera.main.SendMessage("fadeOut");
            Invoke( "DeadWait", 3F);
			isOnceNoHP = false;
		}
		if( !PhotonNetwork.offlineMode)
		{
			KnockDownHPOnNet();
		}

	}

    void DeadWait()
    {
        //putRetryMenu = true;
        playmodeUI.SetActive(false);
        gameoverUI.SetActive(true);
        Camera.main.SendMessage("Disable");
        Time.timeScale = 0F;
    }

    public void QuitMission()
    {
        Application.LoadLevel( "topmenu");
    }
    
    public void RestartMission()
    {
        Time.timeScale = 1F;
        Application.LoadLevel (Application.loadedLevel);
    }

	void KnockDownHPOnNet()
	{
		if( isOnceNoHP)
		{
			if( onlineModeCtr == null)
            {
                onlineModeCtr = GameObject.FindWithTag("NetworkController").GetComponent<OnlineModeControl>();
            }

            time = 5F;

            this.SendMessage( "AddLogOnNet",  PhotonNetwork.playerName + " さんが殺されました！！");
            switch( onlineModeCtr.battleMode)
            {
                case BATTLE.FLAGBATTLE:
                    this.SendMessage( "AddLogMesseage", "所持していたフラグを全て落としてしまいました！");
                    break;
                case BATTLE.DEATHMATCH2:
                case BATTLE.DEATHMATCH4:
                    onlineModeCtr.photonView.RPC( "SetFlagPoint", PhotonTargets.All, (int)player.GetComponent<PlayerNetworkControl>().team, -1);

                    // Beta!! キルポイントを1減らす.
                    PhotonPlayer photonPly = PhotonNetwork.player;
                    photonPly.SetScore( photonPly.GetScore() - 1);
                    break;
            }

			isOnceNoHP = false;
            startRespwan = true;
		}

        if (startRespwan)
        {
            if (time <= 0)
            {
                startRespwan = false;
                GetNetworkMenuControl().StartBattle();
                player.SendMessage("SetState", PLAYERSTATE.DISABLE);
            } else
            {
                GetNetworkMenuControl().WaitRespawn(time);
                time -= Time.deltaTime;
            }
        }
	}

	void GetStatusControl()
	{
		if( player == null) return;
		statusCtr = player.GetComponent<StatusControl>();
	}

    NetworkMenuControl GetNetworkMenuControl()
    {
        if(netMenuCtr == null)
        {
            netMenuCtr = GameObject.FindWithTag("NetworkController").GetComponent<NetworkMenuControl>();
        }

        return netMenuCtr;
    }

	// Reciver from regist Player. 
    public void RegisterPlayer( PlayerData data)
	{
        player = data.player;
		GetStatusControl();
		this.enabled = true;
	}

    public void StartRespawn()
    {
        isOnceNoHP = true;
    }
}