﻿using UnityEngine;
using System.Collections;

public class RecoveryCircleControl : MonoBehaviour 
{
    public float recoveryTime = 5F;
    private float time = 0F;

    private CommonGaugeController commonGaugeCtr;
    private bool isStart = false;
    private GameObject controller;
    private GameObject player = null;

    BaseCampBehavior baseCampBhv;

    void Awake()
    {
        controller = GameObject.FindWithTag("GameController");
        commonGaugeCtr = controller.GetComponent<CommonGaugeController>();

        baseCampBhv = this.transform.parent.GetComponent<BaseCampBehavior>();
    }

	// Update is called once per frame
	void Update () 
    {
	    if (isStart)
        {
            if( recoveryTime < time)
            {
                controller.SendMessage( "AddLogMesseage", "弾薬を補給しました。");

                WeaponControl wpCtr = controller.GetComponent<WeaponControl>();
                wpCtr.ResetAllWeapon();

                StopRecovery();
            }

            commonGaugeCtr.value = time / recoveryTime;
            time += Time.deltaTime;
        } 
        else
        {
            time = 0F;
        }

	}

    private void OnTriggerEnter(Collider other)
    {
        GameObject player = GetPlayer();
        if (player == other.gameObject)
        {
            TEAM colTeam = player.GetComponent<PlayerNetworkControl>().team;
            if ( !ChkMyCampOnNet( colTeam))
            {
                return;
            }

            isStart = true;
            commonGaugeCtr.UseGauge(CGUSETYPE.RECOVERYCIRCLE, 0F);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        GameObject player = GetPlayer();
        if (player == other.gameObject)
        {
            StopRecovery();
        }
    }   

    void StopRecovery()
    {
        commonGaugeCtr.DropGauge(CGUSETYPE.RECOVERYCIRCLE);
        isStart = false;
    }

    GameObject GetPlayer()
    {
        if(player == null)
        {
            player = GameObject.FindWithTag("Player");
        }

        return player;
    }

    bool ChkMyCampOnNet( TEAM team)
    {
        if (!PhotonNetwork.offlineMode)
        {
            if( baseCampBhv.team != team)
            {
                return false;
            }
        }

        return true;
    }
}
