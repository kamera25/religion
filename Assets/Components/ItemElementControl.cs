﻿using UnityEngine;
using System.Collections;

public class ItemElementControl : Photon.MonoBehaviour 
{
	
	public int ItemNo; 
	public float visibleRange = 2000;
	public bool isRidClone = false;
	public ItemControl.ITEMKIND ItemKind;

	private GameObject controller;
	private ItemControl itemCtl; 
	private TextMesh itemText;
	// 0:ammo 1:recovery 2:key 3:weapon 4:equipment
	
	void  Start ()
	{
		GameObject parent = GameObject.Find("Envioroment(Dynamic)");
		controller = GameObject.FindWithTag("GameController");
		itemCtl = controller.GetComponent<ItemControl>();
		itemText = GetComponentInChildren<TextMesh>();
		
        itemText.text = name;
		if( isRidClone) DeleteCloneString();
		if( parent != null) this.transform.SetParent( parent.transform);

	}
	
	void  Update ()
	{
		transform.Rotate( 0, 45 * Time.deltaTime, 0);

		if( ( Camera.main.transform.position - this.transform.position).sqrMagnitude < visibleRange)
		{
			itemText.text = name;
		}
		else
		{
			itemText.text = "";
		}
	}

	/* Delete '(Clone)' of "[Item](Clone)". */
	void  DeleteCloneString ()
	{
		int Index;
		
		Index = this.name.IndexOf("(");
		if( Index != -1)
		{
			this.name = this.name.Substring( 0, Index);
		}
	}

	
	void OnTriggerStay ( Collider collider  )
	{
		bool  ItemAddFlag = false;
		
		
		if( !collider.gameObject.CompareTag( "Player") || !Input.GetButtonDown("Search") ) return;

		switch(ItemKind)
		{
		case ItemControl.ITEMKIND.AMMO:
			if( !itemCtl.Is_NowAmmoMax())
			{
				itemCtl.AddAmmoList(ItemNo);
				ItemAddFlag = true;
			}
			break;
		case ItemControl.ITEMKIND.RECOVERY:
			if( !itemCtl.Is_NowRecoveryMax())
			{				
				itemCtl.AddRecoveryList(ItemNo);
				ItemAddFlag = true;
			}
			break;
		case ItemControl.ITEMKIND.KEY:
			itemCtl.AddKeyList(ItemNo);
			ItemAddFlag = true;
			break;
		case ItemControl.ITEMKIND.WEAPON:
			//itemCtl.AddWeaponList(ItemNo);
                Debug.LogWarning("Now dosent get item. The reason is remaking LoadPlayersWeapon.");
			ItemAddFlag = true;
			break;
		case ItemControl.ITEMKIND.EQUIPMENT:
			itemCtl.AddEquipmentList(ItemNo);
			ItemAddFlag = true;	
			break;				
		}
		
		if (ItemAddFlag)
        {
			
            GameObject clone = Instantiate<GameObject>(Resources.Load<GameObject>("unit/GetItemSE"));	
            Destroy(clone, 5.0f);

            controller.SendMessage("AddLogMesseage", "Get an item : " + this.name);
            if (PhotonNetwork.offlineMode)
            {
                Destroy(this.gameObject);
            } else
            {
                photonView.RPC("DestroyItem", PhotonTargets.MasterClient);
            }
        }
		else
		{
			controller.SendMessage( "AddLogMesseage", "Now Max item! ");
		}
	}

	[RPC]
	void DestroyItem()
	{
		PhotonNetwork.Destroy(gameObject);
	}
}