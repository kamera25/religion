﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrainingController : MonoBehaviour 
{
    private GameObject player;

    private StatusControl statusCtr;
    private FlagManager flagManager;
    private PlayerWeaponControl playerWpCtr;
    private LogControl logCtr;

    private float time;
    private const float CHECK_TIME_SPAN = 2F;

    List<TrainingModelController> modelList= new List<TrainingModelController>();
    List<TrainingModelController> manModelList= new List<TrainingModelController>();

	// Use this for initialization
	void Start () 
    {
        player = GameObject.FindWithTag("Player");
        statusCtr = player.GetComponent<StatusControl>();

        flagManager = this.GetComponent<FlagManager>();
        playerWpCtr = this.GetComponent<PlayerWeaponControl>();
        logCtr      = this.GetComponent<LogControl>();

        InitalizeTargets();
	}
	
    void InitalizeTargets()
    {
        for( int i = 0; i < 3; i++)
        {
            GameObject model = Instantiate<GameObject>( Resources.Load<GameObject>("Enemy/Target/TargetNormal"));
            TrainingModelController trainigCtr = model.GetComponent<TrainingModelController>();

            trainigCtr.ResetModel();
            modelList.Add(trainigCtr);
        }

        for( int i = 0; i < 3; i++)
        {
            GameObject model = Instantiate<GameObject>( Resources.Load<GameObject>("Enemy/Target_Man/Target_Man"));
            TrainingModelController trainigCtr = model.GetComponent<TrainingModelController>();
            
            trainigCtr.ResetModel();
            manModelList.Add(trainigCtr);
        }


    }

	// Update is called once per frame
	void Update () 
    {
        // wait about 2 minutes.
        if( CHECK_TIME_SPAN < time)
        {
            if( (bool)flagManager.flagDictionary ["Change_Training_Setting"])
            {
                ApplyTrainingSetting();
            }

            CyclicProcess();
            time = 0F;
        }

        time += Time.deltaTime;
	}

    // Apply status from setting menu.
    void ApplyTrainingSetting()
    {
        // Speed up stamina recovery.
        if ((bool)flagManager.flagDictionary ["AUTO_STAMINA_RECOVERY"])
        {
            statusCtr.RecoveryStaminaSpeedUp( 400F, 1000000F);
        } 
        else
        {
            statusCtr.StopRecoveryStaminaSpeed();
        }

        // If enemy put.
        if ((bool)flagManager.flagDictionary ["PUT_TARGET"])
        {
            // 通常のターゲット
            if ( (bool)flagManager.flagDictionary ["PUT_NORMALTARGET"])
            {
                ModelsSetActive(true);
                bool on = (bool)flagManager.flagDictionary ["MOVE_NORMALTARGET"];
                ModelsMoveActive(on);
            } 
            else
            {
                ModelsSetActive(false);
            }

            // 人形のターゲット
            if ( (bool)flagManager.flagDictionary ["PUT_MANTARGET"])
            {
                ManModelsSetActive(true);
                bool on = (bool)flagManager.flagDictionary ["MOVE_MANTARGET"];
                ManModelsMoveActive(on);
            } 
            else
            {
                ManModelsSetActive(false);
            }

        } else
        {
            AllModelsSetActive(false);
        }


        flagManager.flagDictionary ["Change_Training_Setting"]  = false;
    }

    void ModelsSetActive( bool value)
    {
        foreach( TrainingModelController modelCtr in modelList)
        {
            modelCtr.gameObject.SetActive(value);
        }
    }

    void ModelsMoveActive( bool value)
    {
        foreach( TrainingModelController modelCtr in modelList)
        {
            modelCtr.isMove = value;
        }
    }

    void ManModelsSetActive( bool value)
    {
        foreach( TrainingModelController modelCtr in manModelList)
        {
            modelCtr.gameObject.SetActive(value);
        }
    }

    void ManModelsMoveActive( bool value)
    {
        foreach( TrainingModelController modelCtr in manModelList)
        {
            modelCtr.isMove = value;
        }
    }

    void AllModelsSetActive( bool value)
    {
        ModelsSetActive(value);
        ManModelsSetActive(value);
    }


    void CyclicProcess( )
    {
        if ((bool)flagManager.flagDictionary ["AUTO_HP_RECOVERY"])
        {
            statusCtr.RecoveryHPUsingRate(10F);
        }

        if( (bool)flagManager.flagDictionary ["AUTO_BULLET_RECOVERY"])
        {
            playerWpCtr.ResetAllWeaponMagazine();
        }
    }

    public void DetectDamage( float damage, Vector3 pos)
    {

        // if log putting is enable. 
        if ((bool)flagManager.flagDictionary ["PUT_LOG"])
        {
            // ヘッドショットか?
            if( (int)damage == WeaponControl.headShotDamage)
            {
                logCtr.AddLogMesseage( "HeadShot!!!"); 
            }
            else
            {
                logCtr.AddLogMesseage( "Hit!!");
            }

            if((bool)flagManager.flagDictionary ["PUT_LOG_DAMAGE"])
            {
                logCtr.AddLogMesseage( "Damage : " + damage.ToString());
            }
            if((bool)flagManager.flagDictionary ["PUT_LOG_DISTANCE"])
            {
                Vector3 distVec = pos - player.transform.position;
                float distance = Vector3.Magnitude( distVec) / LandmarkIndicatorBehavior.WORLD_SCALE_CORRECTION;
                logCtr.AddLogMesseage( "Distance : " + distance); 
            }


        }
    }

    // TraningConfigMenu から呼び出す.すぐに変更を適応する
    public void ImmediateApply()
    {
        time = CHECK_TIME_SPAN;
    }
}
