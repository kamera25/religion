﻿using UnityEngine;
using System.Collections;

public enum WIN
{
    NONE,
    SPECIFICKILL = 0,
    ALLKILL,
    GETITEM,
    TIMEUP
}


public class GameClearControl : MonoBehaviour 
{

    public bool isClear = false;
    public WIN winCondtion;
    public GameObject target;
    public float time;


    StatusControl targetStatus;

	// Use this for initialization
	void Start () 
    {
        if (winCondtion == WIN.SPECIFICKILL && target == null)
        {
            Debug.LogWarning("No set kill targets.");
            isClear = true;
        } 
        else if (winCondtion == WIN.GETITEM && target == null)
        {
            Debug.LogWarning("No set Item targets.");
            isClear = true;
        } 
        else if (winCondtion == WIN.TIMEUP)
        {
            GameObject timer = Instantiate<GameObject>( Resources.Load<GameObject>("unit/Timer")); 
            timer.SendMessage( "SetTime", time);
            timer.SendMessage( "StartTimer");
        }

	}
	
	// Update is called once per frame
	void Update () 
    {
        if (isClear || !PhotonNetwork.offlineMode)
            return;

        switch (winCondtion)
        {
            case WIN.ALLKILL:
                if (GameObject.FindWithTag("Enemy") == null)
                {
                    WinGame();
                }
                break;
            case WIN.SPECIFICKILL:
                if (target.CompareTag("DeadEnemy"))
                {
                    WinGame();
                }
                break;
            case WIN.GETITEM:
                if (target == null)
                {
                    WinGame();
                }
                break;
        }


	}

    void WinGame()
    {
        isClear = true;

        Invoke("FirstClearProc", 2F);
    }

    void FirstClearProc()
    {
        Camera.main.SendMessage("fadeOut");
        Invoke("GoToResultSence", 3F);
    }

    void GoToResultSence()
    {
        Application.LoadLevel("Result");
    }

    void TimeUp()
    {
        if ( winCondtion == WIN.TIMEUP)
        {
            WinGame();
        }
    }
}
