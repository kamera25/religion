﻿using UnityEngine;
using System.Collections;

public class EndDialogControl : MonoBehaviour 
{

    public GameObject route;

	void Quit()
    {
        GameObject.FindWithTag("GameController").GetComponent<AudioSource>().Play();
        Application.Quit();
    }

    void BackMenu()
    {
        GameObject.FindWithTag("GameController").GetComponent<AudioSource>().Play();
    }

    void PutMenu()
    {
        GameObject.FindWithTag("GameController").GetComponent<AudioSource>().Play();
    }
}
