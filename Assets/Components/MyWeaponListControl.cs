﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MyWeaponListControl : ItemProcessForMenu 
{

    private ItemControl playerItemCtr;
    private WeaponDataProcess wpDataProc;
    [SerializeField] RectTransform myWpList;
    [SerializeField] GameObject nodePref;

    private List<GameObject> nodeList = new List<GameObject>();

	// Use this for initialization
	void Start () 
    {
        GameObject controller = GameObject.FindWithTag("GameController");

        playerItemCtr = controller.GetComponent<ItemControl>();
        wpDataProc = controller.GetComponent<WeaponDataProcess>();
        UpdateList();
	}
	
	public void UpdateList () 
    {
        DestroyNodes();

        int count = playerItemCtr.Get_WeaponCount();

        for( int i = 0; i< count; i++)
        {
            // Instatiate a node.
            GameObject node = Instantiate<GameObject>(nodePref);
            nodeList.Add(node);
            RectTransform nodetr = node.GetComponent<RectTransform>();
            nodetr.SetParent(myWpList);

            // Set Data.
            RectTransform button = nodetr.FindChild("Weapon").GetComponent<RectTransform>();
            ItemButtonBehavior itemButton = button.GetComponent<ItemButtonBehavior>();
            int wpIndex = playerItemCtr.GetListData(i, ItemControl.ITEMKIND.WEAPON);

            itemButton.pram = GetWeaponPrama(wpIndex);
            itemButton.index = i;

            // Update type image.
            RectTransform typeImage = nodetr.FindChild("WeaponTypeImage").GetComponent<RectTransform>();
            typeImage.GetComponent<WeaponTypeImageControl>().UpdateType( wpDataProc.WeaponDataList[i].wpType);

            // Load sprite.
            Sprite wpSprite = GetItemSprite( i, playerItemCtr, ItemControl.ITEMKIND.WEAPON);
            button.GetComponent<Image>().sprite = wpSprite;
        }
	}

    private void DestroyNodes()
    {
        foreach (GameObject node in nodeList)
        {
            GameObject.Destroy(node);
        }
    }
}
