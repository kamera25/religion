﻿using UnityEngine;
using System.Collections;

public class PostureBlurController : Actor
{
    private Animator anim;
    private PlayerWeaponControl playerWp;
    public float blur = 0.01F;
    public float aimX;
    public float aimY;

	// Use this for initialization
	void Start () 
    {
        anim = GetAnimator();

        playerWp = GameObject.FindWithTag("GameController").GetComponent<PlayerWeaponControl>();
        StartCoroutine("StartBlur");
    }

    void Update()
    {
        if (playerWp.NowWeaponNo == 0 ) return;
        anim.SetFloat( "AimX", Mathf.Lerp( anim.GetFloat( "AimX"), aimX, Time.deltaTime));
        anim.SetFloat( "AimY", Mathf.Lerp( anim.GetFloat( "AimY"), aimY, Time.deltaTime));
    }

    IEnumerator StartBlur()
    {
        // if No equip weapon.
        while (true)
        {
            while (true)
            {
                if ( playerWp.NowWeaponNo == 0 || anim == null) break;

                aimX = (float)playerWp.NowWp().recoil * blur * Random.Range(-1F, 1F) * GetSquatState() + anim.GetFloat("AimX");
                aimY = (float)playerWp.NowWp().recoil * blur * Random.Range(-1F, 1F) * GetSquatState() + anim.GetFloat("AimY");

                yield return new WaitForSeconds(0.15F);
            }
            yield return new WaitForSeconds(0.025F);
        }
    }


    int GetSquatState()
    {
        if (anim.GetBool("isSquat")) return 0;
        return 1;
    }
}
