﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartDialogControl : MonoBehaviour 
{
    public Text titleText;
    public Text detailText;
    private string sence;
    public GameObject route;

    public void PutDialog( string s, PLACE place, string missionName, string detail)
    {
        sence = s;

        titleText.text = missionName;
        detailText.text = "場所 : " + place.ToString() + "\n" + detail;
        this.gameObject.SetActive( true);

    }

    void PushNo()
    {
        GameObject.FindWithTag("GameController").GetComponent<AudioSource>().Play();

        GameObject.Find("Earth Pivot").SendMessage( "SetAimPlace", PLACE.NONE);
        this.gameObject.SetActive( false);
        route.SetActive(true);
    }

    void PushYes()
    {
        GameObject.FindWithTag("GameController").GetComponent<AudioSource>().Play();

        Application.LoadLevel( sence);
    }
}


