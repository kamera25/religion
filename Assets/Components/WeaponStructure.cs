using UnityEngine;
using System.Collections;



public enum WeaponKind
{
	HAND = 0,
	ASSAULT,
	SMG,
	SHOT,
	MACHINE,
	RIFLE,
	GRENADE,
	SHIELD,
	SUPPORT,
    SORD,
	NONE
}

public enum LANK
{
    S_PLUS,
    S = 0,
    S_MINUS,
    A_PLUS,
    A,
    A_MINUS,
    B_PLUS,
    B,
    B_MINUS,
    C_PLUS,
    C,
    C_MINUS,
    D_PLUS,
    D
}

public enum BULLET
{
    NONE = -1,
    _45ACP = 0,
    _22L,
    _9MM,
    _44MAGNAM,
    _357MAG,
    _556MM,
    _HEAT,
    _40MM,
    _38SPC,
    _12GAGE,
    _762MMX39
}

public enum WEAPONTYPE
{
    MAIN,
    SUB,
    SUPPORT
}

public class WeaponData
{

	public const float RELOADWEIGHT = 1.5F;
    public Entity_weapondata.Param wpData;

    public WeaponKind type;
    public LANK recoil;
    public BULLET bullet_type;

    public Sprite icon;

	public float ch_time;
	public float nowReload;

	public GameObject model;
    public WeaponBehavior wpBhv;

	public int nowmagazine;
	public int nowammo;
    public int user;

    public WEAPONTYPE wpType;
    public GranadeController granadeCtr;


	/* ********* */
	/* Accsessor */
	/* ********* */
	public void Set_nowmagazine( int magazine)
	{
		nowmagazine = magazine;
	}

	public void Set_nowammo( int ammo)
	{
		nowammo = ammo;
	}

	public void Set_nowReload( float time)
	{
		nowReload = time;
	}

}
