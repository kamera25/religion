﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class NetworkMenuControl : Photon.MonoBehaviour 
{
    private GameObject networkMenuUI;
    private Text joinNumText;
    private Text statusText;
    private Button respawnButton;
    private GameObject playerListNode;
    private Transform playerListScrollView;

    private List<GameObject> nodeList = new List<GameObject>();

    void Start()
    {
        Transform networkUI = GameObject.Find("NetworkUI").transform;
        networkMenuUI = networkUI.FindChild("NetworkMenu").gameObject;

        #if !UNITY_EDITOR
        networkMenuUI.SetActive(false);
        #endif

        playerListScrollView = networkMenuUI.transform.FindChild("PlayerList").FindChild("ScrollView");

        joinNumText = networkMenuUI.transform.FindChild("JoinNumText").GetComponent<Text>();

        playerListNode = Resources.Load<GameObject>("Menu/OnlineMode/PlayerListNode");

        statusText = networkMenuUI.transform.FindChild("StatusText").GetComponent<Text>();
        respawnButton = networkMenuUI.transform.FindChild("RespawnButton").GetComponent<Button>();
    }

    public void UpdateDataOnly()
    {
        //最大人数と参加人数を表示.
        joinNumText.text = PhotonNetwork.room.playerCount + "/" + PhotonNetwork.room.maxPlayers;
        UpdatePlayerList();
    }

	public void UpdateData()
    {
        PutNetworkMenu();

        UpdateDataOnly();
    }

    // 全プレイヤーのリスト情報を更新します.
    private void UpdatePlayerList()
    {
        AdjustNodeList();

        for (int i = 0; i < PhotonNetwork.room.playerCount; i++)
        {
            PhotonPlayer player = PhotonNetwork.playerList[i];

            // ノードを取得する
            GameObject node = nodeList[i];

            PlayerListNodeBehavior listBhav = node.GetComponent<PlayerListNodeBehavior>();
            TEAM team = (TEAM)Enum.Parse( typeof(TEAM), player.GetTeam().ToString());
            listBhav.SetName ( player.name);
            listBhav.SetPoint( player.GetScore());
            listBhav.SetTeam ( team);
        }
    }

    // Nodeの数を調節する.
    private void AdjustNodeList()
    {
        // ノードの削除
        for (int i = nodeList.Count - 1; 0 <= i; i--)
        {
            GameObject node = nodeList[i];
            Destroy( node);
            nodeList.RemoveAt(i);
        }

        // ノードの作成
        for (int i = 0; i < PhotonNetwork.room.playerCount; i++)
        {
            InstantiateNode();
        }
    }

    // ノードの生成を行う.
    private void InstantiateNode()
    {
        GameObject node = Instantiate<GameObject>(playerListNode);
        nodeList.Add(node);
        node.transform.SetParent( playerListScrollView);
    }

    public void WaitBattle( float t)
    {
        respawnButton.interactable = false;
        statusText.text = "プレイヤーを募集しています。残り" 
                        + t.ToString("0") 
                        + "秒で戦闘開始します。";
    }

    public void WaitRespawn( float t)
    {
        PutNetworkMenu();
        respawnButton.interactable = false;
        statusText.text = "再リスポンまで、残り"
                        + t.ToString("0")
                        + "秒です。";
    }

    // カウントダウンタイマーが終了したら呼び出される.
    public void StartBattle()
    {
        respawnButton.interactable = true;
        statusText.text = "戦闘開始しました。";
        if ( PhotonNetwork.isMasterClient)
        {
            GameObject.FindWithTag("NetworkController").GetComponent<OnlineModeControl>().StartGameTimer();
        }
    }

    public void PutNetworkMenu()
    {
        networkMenuUI.SetActive(true);

        // PoseMenuUI(ESCキーで出るメニュー)が出ていれば.
        GameObject poseMenuUI = GameObject.Find("PoseMenuUI");
        if (poseMenuUI != null)
        {
            Destroy( poseMenuUI); // 消す.
        }
    }

    public bool IsPutNetworkMenuUI()
    {
        return networkMenuUI.activeSelf;
    }
}
