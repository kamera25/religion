using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public enum BATTLE
{
    NONE = 0,
	DEATHMATCH2,
	DEATHMATCH4,
	POLICEVSTHIEF,
	FLAGBATTLE,
	ONLYWEAPON,
	MONSTERTEAMKILL,
	BOMBDISMANTLING
}

public enum TEAM : byte 
{
	NONE = 0,
	RED = 1,
	BLUE = 2,
	YELLOW = 3,
	GREEN = 4
}

public class OnlineModeControl : Photon.MonoBehaviour 
{

    public BATTLE battleMode = BATTLE.NONE;
    public int ticket = 10;
    public int time;

    private int teamNum = 2;

    private GameObject dm4BattleUI;
    private GameObject dm2BattleUI;
    private GameObject flagBattleUI;

	private float itemRespawnTime = 30.0f;
	private GameObject player;
	private PlayerNetworkControl playerNetworkCtr;
	private RespawnBehavior respawnBehav;
	private SetBaseCamp setBaseCamp;
	private TEAM assignedTeam = TEAM.NONE;

    private Text redPoint;
    private Text bluePoint;
    private Text greenPoint;
    private Text yellowPoint;

    const int TIME_SLIDER_MAX = 26;

    private WeaponControl wpCtr;
    private GameObject controller;

    private int respanwCount = 0;

	void Start()
	{
		respawnBehav	= GameObject.Find("RespawnPoints").GetComponent<RespawnBehavior>();
		setBaseCamp 	= GameObject.Find("BasePoints").GetComponent<SetBaseCamp>();

        Transform networkUI = GameObject.Find("NetworkUI").transform;
        dm4BattleUI = networkUI.FindChild("DM4Battle").gameObject;
        dm2BattleUI = networkUI.FindChild("DM2Battle").gameObject;
        flagBattleUI = networkUI.FindChild("FlagBattle").gameObject;

        wpCtr = GameObject.FindWithTag("GameController").GetComponent<WeaponControl>();
        controller = GameObject.FindWithTag("GameController");
	}

	public void NetworkStart( GameObject ply)
	{
        // Setting Player Model...
		player = ply;
        player.tag = "Player";
		player.layer = 9 ;// Set Ignore Raycast.

        playerNetworkCtr = player.GetComponent<PlayerNetworkControl>();


		switch( battleMode)
		{
			case BATTLE.FLAGBATTLE:
                SetFlagBattle();
				break;
			case BATTLE.DEATHMATCH2:
                SetDeathMatch2();
				break;
			case BATTLE.DEATHMATCH4:
                SetDeathMatch4();
				break;
		}

        int playerViewID = player.GetPhotonView().viewID;
        photonView.RPC("RequestNearBaseCamp", PhotonTargets.MasterClient, playerViewID);
        photonView.RPC("SetTeamPlayerNetworkControl", PhotonTargets.AllBuffered, playerViewID, PhotonNetwork.playerName);

		// Invisible Player Charactor.

		//player.SetActive(false);
        PlayerStandBySpawn();

	}


    [RPC]
    private void SetTeamPlayerNetworkControl( int viewID, string userName)
    {
        GameObject ply = PhotonView.Find( viewID).gameObject;
        ply.GetComponent<PlayerNetworkControl>().userName = userName;
    }

    private void SetFlagBattle()
    {
        GameObject flagBattle = flagBattleUI;
        flagBattle.gameObject.SetActive(true);

        Transform battleCounter = flagBattle.transform.FindChild("BattleCounter");
        redPoint        = battleCounter.FindChild("RedText").GetComponent<Text>();
        bluePoint       = battleCounter.FindChild("BlueText").GetComponent<Text>();
        
        photonView.RPC( "AskMyTeamToMC", PhotonTargets.MasterClient, player.GetPhotonView().viewID);
        
        // if you are MasterClient.
        
        if( PhotonNetwork.isMasterClient)
        {
            setBaseCamp.BatchSetBaseCampFlagBattle(2);// Put Bumpy at 2 places.
            setBaseCamp.SetPosFromAroundBasePoint( playerNetworkCtr.team, player.transform);// Set Character near Bumpy.
            
            RespawnFlags(4);
            GameObject.FindWithTag("GameController").SendMessage("PutTimer");
            SetProcessOnlineModeForMC();
        }

        return;
    }

    private void SetDeathMatch2()
    {
        dm2BattleUI.SetActive(true);

        redPoint        = dm2BattleUI.transform.FindChild("RedText").GetComponent<Text>();
        bluePoint       = dm2BattleUI.transform.FindChild("BlueText").GetComponent<Text>();

        photonView.RPC( "AskMyTeamToMC", PhotonTargets.MasterClient, player.GetPhotonView().viewID);

        // if you are MasterClient.
        if( PhotonNetwork.isMasterClient)
        {
            // Initialize Points
            photonView.RPC( "SetFlagPoint", PhotonTargets.AllBuffered, (int)TEAM.RED, ticket);
            photonView.RPC( "SetFlagPoint", PhotonTargets.AllBuffered, (int)TEAM.BLUE, ticket);

            setBaseCamp.BatchSetBaseCampFlagBattle(2);// Put Bumpy at 2 places.
            setBaseCamp.SetPosFromAroundBasePoint( playerNetworkCtr.team, player.transform);// Set Character near Bumpy.
            GameObject.FindWithTag("GameController").SendMessage("PutTimer");
            SetProcessOnlineModeForMC();
        }

        return;
    }

    private void SetDeathMatch4()
    {
        dm4BattleUI.SetActive(true);

        teamNum = 4;
        
        redPoint        = dm4BattleUI.transform.FindChild("RedText").GetComponent<Text>();
        bluePoint       = dm4BattleUI.transform.FindChild("BlueText").GetComponent<Text>();
        greenPoint       = dm4BattleUI.transform.FindChild("GreenText").GetComponent<Text>();
        yellowPoint       = dm4BattleUI.transform.FindChild("YellowText").GetComponent<Text>();

        photonView.RPC( "AskMyTeamToMC", PhotonTargets.MasterClient, player.GetPhotonView().viewID);
        
        // if you are MasterClient.
        if( PhotonNetwork.isMasterClient)
        {
            // Initialize Points
            photonView.RPC( "SetFlagPoint", PhotonTargets.AllBuffered, (int)TEAM.RED, ticket);
            photonView.RPC( "SetFlagPoint", PhotonTargets.AllBuffered, (int)TEAM.BLUE, ticket);
            photonView.RPC( "SetFlagPoint", PhotonTargets.AllBuffered, (int)TEAM.GREEN, ticket);
            photonView.RPC( "SetFlagPoint", PhotonTargets.AllBuffered, (int)TEAM.YELLOW, ticket);
            
            setBaseCamp.BatchSetBaseCampFlagBattle(4);// Put Bumpy at 2 places.
            setBaseCamp.SetPosFromAroundBasePoint( playerNetworkCtr.team, player.transform);// Set Character near Bumpy.
            GameObject.FindWithTag("GameController").SendMessage("PutTimer");
            SetProcessOnlineModeForMC();
        }
        
        return;
    }

    public void SetProcessOnlineModeForMC()
    {
        switch( battleMode)
        {
            case BATTLE.FLAGBATTLE:
                StartCoroutine( "RespawnFlagOnFixedTime");
                break;
            case BATTLE.DEATHMATCH2:
                StartCoroutine( "DecisionDeathMatch2");
                break;
            case BATTLE.DEATHMATCH4:
                StartCoroutine( "DecisionDeathMatch4");
                break;
        }
    }

    IEnumerator DecisionDeathMatch2()
    {
        while (true)
        {
            if (GetFlagPoint(TEAM.RED) <= 0 || GetFlagPoint(TEAM.BLUE) <= 0)
            {
                photonView.RPC( "CallGameSet", PhotonTargets.All);
                break;
            }
            
            yield return new WaitForSeconds(0.5F);
        }
    }

    IEnumerator DecisionDeathMatch4()
    {
        while (true)
        {
            if (    GetFlagPoint(TEAM.RED) <= 0 
                ||  GetFlagPoint(TEAM.BLUE) <= 0 
                ||  GetFlagPoint(TEAM.GREEN) <= 0 
                ||  GetFlagPoint(TEAM.YELLOW) <= 0
            )
            {
                photonView.RPC( "CallGameSet", PhotonTargets.All);
                break;
            }
            
            yield return new WaitForSeconds(0.5F);
        }
    }

    [RPC]
    public void CallGameSet()
    {
        GameObject.FindWithTag("GameController").SendMessage("TimeUp");
    }


	// Assign Team Player.
	public TEAM AssignNewTeam( int teamNum)
	{
        TEAM team = (TEAM)((int)assignedTeam % teamNum) + 1; // Setting not select Team::NONE.
        assignedTeam = (TEAM)( (int)assignedTeam + 1);
        return team;
       
	}
	
	[RPC]// Other
	public void SetTeamOnNet( int viewID, int team)
	{
        PhotonView playerPhotonView = PhotonView.Find(viewID);
		playerPhotonView.GetComponent<PlayerNetworkControl>().team = (TEAM)team;// Set 

        // もし自分のチームがMCから帰ってきたら.
        if (playerPhotonView.isMine)
        {
            // TEAMをPhoton.teamに変換して、ローカルPhotonPlayerにセット
            TEAM t = (TEAM) team;
            PunTeams.Team teamPun = (PunTeams.Team)Enum.Parse( typeof(PunTeams.Team), t.ToString());
            PhotonNetwork.player.SetTeam( teamPun);
        }
	}

	[RPC]// master
    public void AskMyTeamToMC( int viewID)
	{
        TEAM team = AssignNewTeam(teamNum);
        photonView.RPC( "SetTeamOnNet", PhotonTargets.AllBuffered, viewID, (int)team);
	}

	[RPC]// To Master
	public void RequestNearBaseCamp( int viewID)
	{
		GameObject ply = PhotonView.Find( viewID).gameObject;
		TEAM team = ply.GetComponent<PlayerNetworkControl>().team;

		photonView.RPC( "SetPlayerPos", PhotonTargets.AllBuffered, viewID, setBaseCamp.GetPosVec3FromAroundBasePoint( team));

	}

    [RPC]// To Master
    public void RequestSetPosRespawnPoint( int viewID)
    {
        photonView.RPC( "SetPlayerPos", PhotonTargets.AllBuffered, viewID, respawnBehav.SetRespawnPositionVec3());
    }

	[RPC]// To client
	public void SetPlayerPos( int viewID, Vector3 pos)
	{
		PhotonView.Find( viewID).transform.position = pos;
	}


	//　Put Flags on fixed time.(Corotine)
	IEnumerator RespawnFlagOnFixedTime()
	{
		while( true )
		{
			yield return new WaitForSeconds( itemRespawnTime);
			RespawnFlags( 2);
		}
	}

	void RespawnFlags( int unit)
	{
		for( int i = 0; i < unit; i++)
		{
			PhotonNetwork.Instantiate( "Key/Flag/Flag", respawnBehav.SetRespawnPositionVec3(), Quaternion.identity, 0);
		}
	}

	[RPC]
	public void SetFlagPoint( int t, int point)
	{
		TEAM team = (TEAM)t;
        int nowPoint;

		switch( team)
		{
			case TEAM.RED:
                nowPoint = GetFlagPoint(TEAM.RED) + point;
                redPoint.text 	= nowPoint.ToString();
				break;
			case TEAM.BLUE:
                nowPoint = GetFlagPoint(TEAM.BLUE) + point;
				bluePoint.text 	= nowPoint.ToString();
				break;		
            case TEAM.GREEN:
                nowPoint = GetFlagPoint(TEAM.GREEN) + point;
                greenPoint.text  = nowPoint.ToString();
                break;  
            case TEAM.YELLOW:
                nowPoint = GetFlagPoint(TEAM.YELLOW) + point;
                yellowPoint.text  = nowPoint.ToString();
                break;  
		}
	}

	public int GetFlagPoint( TEAM team)
	{

		switch( team)
		{
			case TEAM.RED:
				return int.Parse( redPoint.text);
			case TEAM.BLUE:
				return int.Parse( bluePoint.text);
            case TEAM.GREEN:
                return int.Parse( greenPoint.text);
            case TEAM.YELLOW:
                return int.Parse( yellowPoint.text);
		}

		return 0;
	}

    // リスポンまでの待機状態にする処理.
    private void PlayerStandBySpawn()
    {
        player.SendMessage("SetState", PLAYERSTATE.DISABLE);
        player.transform.position = new Vector3( 0F, -999F, 0F);
    }
    
    private void StartPlayerSpawn()
    {
        player.SendMessage("SetState", PLAYERSTATE.ENABLE);

        int playerViewID = player.GetPhotonView().viewID;
        photonView.RPC("RequestNearBaseCamp", PhotonTargets.MasterClient, playerViewID);

        GameObject audio = GameObject.Find("Audio");
        if (audio != null)
        { 
            audio.GetComponent<AudioSource>().Play();
        }
        
        GameObject.Find("FlagBattle").SetActive(true);
    }

    public void StartGameTimer()
    {
        GUITimer timer = GameObject.Find("Timer").GetComponent<GUITimer>();

        // If time is infinity.
        if( time == TIME_SLIDER_MAX)
        {
            timer.SetTime( GUITimer.TIMER_MAX); 
        }
        else
        {
            timer.SetTime( time * 60);
        }
        
        timer.StartTimer();
    }

	// オンラインゲームを開始します!!.
	public void StartOnlineGame()
	{
        if (respanwCount == 0)
        {
            StartPlayerSpawn();
        } 
        else
        {
            StartRespawnPlayer();
        }

        controller.GetComponent<PoseModeControl>().ForceSetThiedPose();
        respanwCount++;
	}


    public void StartRespawnPlayer()
    {
        player.SendMessage("SetState", PLAYERSTATE.ENABLE);

        // Reset Player's Status.
        if ( battleMode == BATTLE.FLAGBATTLE)
        {
            controller.GetComponent<ItemControl>().RemoveIDFromKey(0); // MUST ORDER!! BEFORE ENABLE PLAYER!!!
        }

        player.GetComponent<StatusControl>().RecoveryAllStatus();
        controller.GetComponent<GameoverControl>().StartRespawn();
        wpCtr.ResetAllWeapon();
        
        // Set Character Position Respawn Point..
        photonView.RPC("RequestSetPosRespawnPoint", PhotonTargets.MasterClient, player.GetPhotonView().viewID);
    }

}
