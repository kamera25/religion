using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameSetControl : Photon.MonoBehaviour 
{

	public bool isPutResult = false;
	private Sprite winLogo;
	private Sprite loseLogo;

	private GameObject OnlineCtr;
	private bool isWin ;

	private GameObject player;

	private int redPoint = 0;
	private int bluePoint = 0;

    [SerializeField] private GameObject gameSetUI;
    [SerializeField] private GameObject playModeUI;
    [SerializeField] private GameObject resultUI;
    [SerializeField] private Image resultImg;
    [SerializeField] private Text redPointText;
    [SerializeField] private Text bluePointText;

    void Start()
    {

        winLogo = Resources.Load<Sprite>("ingame/youwin");
        loseLogo = Resources.Load<Sprite>("ingame/youlose");
    }

    void Update()
    {
        if( !isPutResult) return;
        
        resultUI.SetActive(true);
        
        if (isWin)
        {
            resultImg.sprite = winLogo;
        } 
        else
        {
            resultImg.sprite = loseLogo;
        }
        
        redPointText.text = redPoint.ToString();
        bluePointText.text = bluePoint.ToString();
    }

	// Recive Message from Timer.
	public void TimeUp()
	{
        if ( PhotonNetwork.offlineMode) return;

		Time.timeScale = 0.1f;
        Invoke( "OrderPutResult", 0.5F);
		gameSetUI.SetActive(true);
		
	}


	private void OrderPutResult()
	{
        this.enabled = true;

		gameSetUI.SetActive(false); 
        playModeUI.SetActive(false);

		if (OnlineCtr == null)
        {
            OnlineCtr = GameObject.FindWithTag("NetworkController");
        }
		redPoint = OnlineCtr.GetComponent<OnlineModeControl>().GetFlagPoint( TEAM.RED);
		bluePoint = OnlineCtr.GetComponent<OnlineModeControl>().GetFlagPoint( TEAM.BLUE);

		isWin = IsWinBattle( redPoint, bluePoint);
        isPutResult = true;
        Time.timeScale = 0.0f; // The World!!!
	}

	bool IsWinBattle( int redPoint, int bluePoint)
	{
		switch( player.GetComponent<PlayerNetworkControl>().team)
		{
			case TEAM.RED:
				return redPoint > bluePoint;
			case TEAM.BLUE:
				return bluePoint > redPoint;
		}

		Debug.LogError( "Undifined Rule on IsWinBattle()!!!");
		return false;
	}

    public void RegisterPlayer( PlayerData data)
    {
        player = data.player;
    }

}
