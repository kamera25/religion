﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class ChangeTopImageCntroller : MonoBehaviour {

    public Sprite xmasTop;

	// Use this for initialization
	void Start () 
    {
        DateTime dt = DateTime.Now;
        if ( dt.Month == 12 && dt.Day < 26)
        {
            this.GetComponent<Image>().sprite = xmasTop;
        }
	}

}
