﻿using UnityEngine;
using System.Collections;

public class RPG7Control : Photon.MonoBehaviour 
{

	public float Itemdamage;
	private bool  isRunBang = false;// Does Run RunBang Method?
	public GameObject bullet;
	private float time = 5;
	public GameObject explosion;

	void Start ()
	{
		
		int ID;
		
        WeaponDataProcess wpDataProc = GameObject.FindWithTag("GameController").GetComponent<WeaponDataProcess>();

		ID = wpDataProc.FindIdFromWeaponName( "RPG-7");
        Itemdamage = wpDataProc.GetMinAttack(ID);
	}
	
	void  Update ()
	{
		
		time -= Time.deltaTime;
        float power = (time + 30) * 10;
		GetComponent<Rigidbody>().AddForce( this.transform.forward * power, ForceMode.Force);

		if ( time < 0F && !isRunBang)
        {
            RunBang();
        }
	}
	
	void  DamageReciver (  float damage  )
	{
		time = 0;
	}
	
	void  RunBang ()
	{
		GameObject clone;
		
		this.GetComponent<AudioSource>().Play();

        clone = Instantiate( Resources.Load<GameObject>("ingame/model/Damage_area"), this.transform.position, Quaternion.identity) as GameObject;
        this.GetComponent<Collider>().enabled = false;
        clone.SendMessage("SetDamage",Itemdamage);

        if ( PhotonNetwork.offlineMode)
        {
            Instantiate(explosion, this.transform.position, Quaternion.identity);
            Destroy( bullet);
            Destroy( this.gameObject, 2f);
        } 
        else if( photonView.isMine)
        {
            PhotonNetwork.Instantiate( explosion.name, this.transform.position, Quaternion.identity, 0);
            PhotonNetwork.Destroy(bullet);
            Invoke( "DestroyBulletOnNet", 2F);
        }

		isRunBang = true;
	}

    void DestroyBulletOnNet()
    {
        PhotonNetwork.Destroy( this.gameObject);
    }
	
	void  OnCollisionEnter ( Collision collision  )
	{
		RunBang();
	}
}