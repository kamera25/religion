﻿using UnityEngine;
using System.Collections;

public class MissionInfomation : MonoBehaviour 
{
    public string sence;
    public PLACE place;
    public string missionName;
    [Multiline(3)] public string detail;

    public GameObject dialogObj;
    public GameObject earth;

    void SendDataToDialog()
    {
        GameObject.FindWithTag("GameController").GetComponent<AudioSource>().Play();

        earth.SendMessage( "SetAimPlace", place);
        dialogObj.GetComponent<StartDialogControl>().PutDialog( sence, place, missionName, detail);
        this.transform.parent.gameObject.SetActive(false);
    }

}
