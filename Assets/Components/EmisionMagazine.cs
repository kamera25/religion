﻿using UnityEngine;
using System.Collections;

public class EmisionMagazine : Photon.MonoBehaviour 
{
    [SerializeField] GameObject magazine; 

    public void EmmsionMagazine( Quaternion Q)
    {
        GameObject clone;
        if (PhotonNetwork.offlineMode)
        {
            clone = Instantiate( magazine, this.transform.position, Q) as GameObject;
        } 
        else
        {
            clone = PhotonNetwork.Instantiate( "Magazine/" + magazine.name, this.transform.position, Q, 0);
        }

        /* Originally Setting Vector3( -1, 1, -0.2) */
        Rigidbody rig = clone.GetComponent<Rigidbody>();

        rig.AddForce( -this.transform.up*4F , ForceMode.Impulse);
        rig.AddTorque(Vector3.up * Random.Range( -6, 6), ForceMode.Impulse);
    }
}
