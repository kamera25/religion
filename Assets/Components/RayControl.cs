using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/* !!!!!!!!!!!!!!!! Warning !!!!!!!!!!!!!!!!!!!!! */
/* when using it, up order priority from Unity!!! */
/* ********************************************** */

public class RayControl : Photon.MonoBehaviour {
	
	private RaycastHit hit;
	private bool istrue ;
	private PoseModeControl poseModeCtr ;
	private RectTransform reticule ;
	private	Ray ray ;
	private bool isDebug = false;
	private GameObject player;

    private int layerMaskNormal;
    private int layerMaskHeadShot;

    private const float rayDist = 10000F;

   // private Vector3 rayDirectionVec;

	// Use this for initialization
	void Start () 
	{
        /* Detect DebugMode. */
        #if UNITY_EDITOR
         isDebug = true;
        #endif

        layerMaskNormal = ~ ( (1 << LayerMask.NameToLayer("Wall")) + (1 << LayerMask.NameToLayer("Player Layer")) + (1 << LayerMask.NameToLayer("Head")));
        layerMaskHeadShot = (1 << LayerMask.NameToLayer("Head"));

		poseModeCtr = this.GetComponent<PoseModeControl>();
		reticule = GameObject.Find("Reticule").GetComponent<RectTransform>();

		if( player == null) this.enabled = false;
		SetplayerCharacterController();
	}

	// Update is called once per frame
	void Update () 
	{
		ChkRayObject();
	}

	void ChkRayObject()
	{
		ray = Camera.main.ScreenPointToRay( new Vector3( Screen.width / 2, Screen.height / 2, 1)); // Center of Screen. 
		istrue = Physics.Raycast(ray, out hit, rayDist, layerMaskNormal);


		SetReticulePosition();
		
	}
	
	void SetReticulePosition()
	{

		if( !isDebug && poseModeCtr.PoseMode == POSE.SUBJECT)
		{
			reticule.transform.position = new Vector3( -1000, -1000, 0);
			return;
		}


		/*Vector3 pos = Camera.main.WorldToScreenPoint( GetRayPosition());
		if( !istrue) pos = Camera.main.WorldToScreenPoint( GetRayPoint( 100));*/
		

        Vector3 pos = Vector3.zero;
		reticule.localPosition = pos;
	}
	
	public Vector3 GetRayPosition()
	{
		if( istrue)
		{
			return hit.point;
		}	
		else
		{
			return Vector3.zero;
		}
	}

	public Vector3 GetRayPositionForWeapon()
	{

		if (hit.collider == null)
        {
            return GetRayPoint(50F);
        }

		if( hit.collider.gameObject.CompareTag( "Stage") || hit.collider.gameObject.CompareTag( "Player"))
		{
			return GetRayPosition();
		}


		return GetRayPoint( 50F);
	}
	
	public Vector3 GetRayPoint( float i)
	{
		
		return ray.GetPoint( i);	
	}
	
	public RaycastHit GetRay()
	{
		if( istrue)
		{
			return hit;
		}	
		else
		{
			RaycastHit nullHit = new RaycastHit();
			return nullHit;
		}
	}

    /// <summary>
    /// Check ray hits head. It use for headshot.
    /// </summary>
    public bool ChkRayHitHead()
    {
        const float dist = 1F;
        Vector3 startPos = hit.point - ray.direction;
        
        bool ishit = Physics.Raycast( startPos, ray.direction, dist, layerMaskHeadShot);
        
        
        return ishit;
    }

	void SetplayerCharacterController()
	{
		if( player == null) return;
	}

    public void RegisterPlayer( PlayerData data)
	{
		player = data.player;
		SetplayerCharacterController();
		this.enabled = true;
	}
}
