﻿using UnityEngine;
using System.Collections;

public class BulletCaseProcess : Photon.MonoBehaviour 
{
    [SerializeField] float destroyTime = 5F;
    private float time = 0;

    void Awake()
    {
        if ( !this.photonView.isMine)
        {
            this.enabled = false;
        }
    }

    void Update()
    {
        if( destroyTime < time)
        {
            DestroyBulletCase();
        }

        time += Time.deltaTime;
    }

    void DestroyBulletCase()
    {
        if ( PhotonNetwork.offlineMode)
        {
            Destroy(this.gameObject);
        } 
        else
        {
            PhotonNetwork.Destroy(this.gameObject);
        }
    }
}
