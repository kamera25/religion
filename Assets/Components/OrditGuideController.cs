﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OrditGuideController : Photon.MonoBehaviour 
{

	private List<GameObject> SpriteList= new List<GameObject>();
	private RayControl rayCtr;
	public float gravity;
	public float xyPeriod;
	public float timeDumping;
	public float posDumping;
    public float posYDumping = 0F;

    private const int ORDITNUM = 20;

    private Transform thisTrans;

	// Use this for initialization
	void Start () 
	{   
		rayCtr = GameObject.FindWithTag("GameController").GetComponent<RayControl>();

		// Load Ordit Guide Sprites.
        thisTrans = this.transform;
        GameObject guideObj = Resources.Load<GameObject>("unit/OrditGuide");
		for( int i =0; i < ORDITNUM; i++)
		{
            GameObject orditSprite = Instantiate( guideObj, thisTrans.position, Quaternion.identity) as GameObject;
            orditSprite.transform.SetParent( thisTrans);
			orditSprite.SetActive( false);
			SpriteList.Add( orditSprite);
		}
		EnableSprites(); //test!!
	}

	public void EnableSprites()
	{
		foreach( GameObject sprite in SpriteList)
		{
			sprite.SetActive(true);
		}
	}

	public void DisableSprites()
	{
		foreach( GameObject sprite in SpriteList)
		{
			sprite.SetActive(false);
		}
	}

	// Update is called once per frame
	void Update () 
	{
		int i = 0;

        Vector3 distVec = ( rayCtr.GetRayPositionForWeapon() - thisTrans.position).normalized;

		foreach( GameObject sprite in SpriteList)
		{
            Vector3 newPos = thisTrans.position + distVec * i * xyPeriod;
			newPos.y += UniformLinearMotionY( i * timeDumping) + posYDumping;
			sprite.transform.position = Vector3.Lerp( sprite.transform.position, newPos, Time.deltaTime * posDumping);
			i++;
		}
	}

	float UniformLinearMotionY( float time)
	{
		return ( -gravity * time * time) / 2 ;
	}
}
