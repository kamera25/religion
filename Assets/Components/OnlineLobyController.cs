﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public enum STAGE
{
    NONE = 0,
    TOWN = 1,
    FOREST = 2
};

public class OnlineLobyController : Photon.MonoBehaviour
{
    [SerializeField] GameObject onlineRoomUI;
    [SerializeField] GameObject onlineDialog;

    [SerializeField] GameObject EntrantNumUIOnRoom1;
    [SerializeField] GameObject EntrantNumUIOnRoom2;
    [SerializeField] GameObject EntrantNumUIOnRoom3;

    [SerializeField] GameObject EntrantTextUIOnRoom1;
    [SerializeField] GameObject EntrantTextUIOnRoom2;
    [SerializeField] GameObject EntrantTextUIOnRoom3;

    [SerializeField] GameObject SelectOnlineModeUI;
    [SerializeField] GameObject connectFailUI;

    [SerializeField] GameObject InputUserNameUI;
    [SerializeField] InputField userName;

    [SerializeField] Slider ticketsSlider;
    [SerializeField] Slider timeSlider;

    private string roomName;
    private BATTLE battleMode = BATTLE.NONE;
    private STAGE stage = STAGE.NONE;

    private int ticket = 0;
    private int time = 15;


    void OnDisable ()
    {
        DisconnectServer();
    }

    void OnFailedToConnectToPhoton()
    {
        connectFailUI.SetActive(true);
        onlineDialog.SetActive(false);
    }

    void DisconnectServer()
    {
        PhotonNetwork.Disconnect ();
    }

    // Setup connecting to Server on PhotonCloud
    public void SetupConnectToServer()
    {
        // exist user name?
        if (PlayerPrefs.GetString("userName") == "")
        {
            InputUserNameUI.SetActive(true);
        }

        PhotonNetwork.ConnectUsingSettings ( PhotonManager.version);
    }

    void SaveUserName()
    {
        PlayerPrefs.SetString( "userName", userName.text);
    }

    void OnJoinedLobby()
    {
        onlineRoomUI.SetActive(true);
        onlineDialog.SetActive(false);
    }

    void OnReceivedRoomListUpdate()
    {
        RoomInfo[] roomInfo = PhotonNetwork.GetRoomList();
        foreach (RoomInfo info in roomInfo)
        {
            switch( info.name)
            {
                case "Room1":
                    PutStringRoomStatus( info, EntrantNumUIOnRoom1, EntrantTextUIOnRoom1);
                    break;
                case "Room2":
                    PutStringRoomStatus( info, EntrantNumUIOnRoom2, EntrantTextUIOnRoom2);
                    break;
                case "Room3":
                    PutStringRoomStatus( info, EntrantNumUIOnRoom3, EntrantTextUIOnRoom3);
                    break;                   
            }
            
        }
    }

    void PutStringRoomStatus ( RoomInfo info, GameObject EntrantNumUI, GameObject EntrantTextUI)
    {
        EntrantNumUI.GetComponent<Text>().text = info.playerCount + "/" + info.maxPlayers;

        int mode = int.Parse(info.customProperties ["onlineMode"].ToString());
        BATTLE battle = (BATTLE) mode;
        EntrantTextUI.GetComponent<Text>().text = battle.ToString();
    }


    void LoadStage()
    {

        if ( stage != STAGE.NONE)
        {
            LoadStageFromScene( stage);
            return;
        }

        // Load Stage Scene from roominfo(PhotonNetwork).
        RoomInfo[] roomInfo = PhotonNetwork.GetRoomList()
                                .Where(info => info.name == roomName)
                                .ToArray();
                                
        foreach (RoomInfo info in roomInfo)
        {
            stage = (STAGE) int.Parse( info.customProperties ["Stage"].ToString());
            LoadStageFromScene( stage);
        }

        return;
    }

    void LoadStageFromScene( STAGE stage)
    {

        PhotonNetwork.isMessageQueueRunning = false; 
        PutUnitControl.stageMode = STAGEMODE.ONLINE;
        DontDestroyOnLoad(this.gameObject);

        switch (stage)
        {
            case STAGE.TOWN:
                Application.LoadLevel("net_Town");
                return;
            case STAGE.FOREST:
                Application.LoadLevel("net_Forest");
                return;
        }

        return;
    }


    void EnterRoom1()
    {
        roomName = "Room1";
        BeforeEnterRoomProc();
    }

    void EnterRoom2()
    {
        roomName = "Room2";
        BeforeEnterRoomProc();
    }

    void EnterRoom3()
    {
        roomName = "Room3";
        BeforeEnterRoomProc();
    }

    void BeforeEnterRoomProc()
    {
        if (checkRoomExist(roomName))
        {
            EnterRoom();
        } 
        else
        {
            SelectOnlineModeUI.SetActive(true); // Put Select Mode on Online.
        }
    }

    /**/

    public void CreateRoomForFB()
    {
        battleMode = BATTLE.FLAGBATTLE;
    }

    public void CreateRoomForDM2()
    {
        battleMode = BATTLE.DEATHMATCH2;
    }

    public void CreateRoomForDM4()
    {
        battleMode = BATTLE.DEATHMATCH4;
    }

    public void CreateRoomForPaT()
    {
        battleMode = BATTLE.POLICEVSTHIEF;
    }


    void ChoseTown()
    {
        stage = STAGE.TOWN;
        EnterRoom();
    }

    void ChoseForest()
    {
        stage = STAGE.FOREST;
        EnterRoom();
    }

    public void SetTicket()
    {
        ticket = (int)ticketsSlider.value;
    }

    public void SetTime()
    {
        time = (int)timeSlider.value;
    }

    /**/

    void EnterRoom()
    {
        if (TopMenuBehavior.Mode != TOPMENUMODE.ONLINE)
        {
            return;
        }

        this.tag = "Untagged";
        this.GetComponent<AudioListener>().enabled = false;
        
        LoadStage();
        Application.LoadLevelAdditive( "OnlineMode");
        StartCoroutine("SetDataToPhotonManager");
    }

    // Wait Loading of Scene(OnlineMode).
    IEnumerator SetDataToPhotonManager()
    {
        while (true)
        {
            GameObject networkController = GameObject.FindWithTag("NetworkController");
            if (networkController != null)
            {

                PhotonNetwork.isMessageQueueRunning = true; 
                networkController.GetComponent<PhotonManager>().CreateRoomFromButton( roomName, battleMode, stage, PlayerPrefs.GetString("userName"), ticket, time);
                networkController.GetComponent<OnlineModeControl>().battleMode = battleMode;

                switch( stage)
                {
                    case STAGE.FOREST:
                        RenderSettings.fogColor = new Color( 0.25F, 0.25F, 0.25F, 1F);
                        RenderSettings.fog = true;
                        break;
                }

                break;
            }
            
            yield return new WaitForSeconds(0.5F);
        }
    }


    bool checkRoomExist( string name)
    {
        bool roomExist = PhotonNetwork.GetRoomList()
                         .Any(info => info.name == name);
                                
        return roomExist;
    }


}
