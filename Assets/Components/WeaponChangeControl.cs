﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class WeaponChangeControl : Photon.MonoBehaviour 
{
	private Sprite transDot;
	private int beforeWeaponNo = 0;
    private int beforeWeaponCount = 0;

	public bool  isChangeLock;

	private WeaponDataProcess Wdp;
	private PlayerWeaponControl WpCtr;
	private PoseModeControl poseCtr;
	private GameObject player;
    private Image Weaponicon;
    private int nextPutModelNo;


	void  Start ()
	{
		Wdp = this.GetComponent<WeaponDataProcess>();
		WpCtr = this.GetComponent<PlayerWeaponControl>();
		poseCtr = this.GetComponent<PoseModeControl>();

        WpCtr.NowWeaponNo = 0;

        Weaponicon = GameObject.Find("WeaponIcon").GetComponent<Image>();
        transDot = Resources.Load<Sprite>( "ingame/transp_dot_Sprite");

        isChangeLock = false;
		this.enabled = false;
	}
	
	void  Update ()
	{
        CheckWeaponChanging();

		if( isChangeLock || Time.timeScale == 0) return;
		
		// Get MouseWhile
		float scroll = Input.GetAxis("Mouse ScrollWheel");
		
		// Loop Processing MouseWhile
		if (scroll > 0 || Input.GetButtonDown("Change weapon"))
        {
            WpCtr.NowWeaponNo++;
        } else if (scroll < 0)
        {
            WpCtr.NowWeaponNo--;
        }

		if( beforeWeaponNo == WpCtr.NowWeaponNo) return;// if no use mousewheel, !!!!BREAK!!!!!

        ChangeWeaponProcess();
	}

    void CheckWeaponChanging()
    {
        if (Wdp.Get_WeaponListCount() != beforeWeaponCount)
        {
            WpCtr.NowWeaponNo = 0;
        }

        beforeWeaponCount = Wdp.Get_WeaponListCount();
    }

    // Weapon Change Process.
    void ChangeWeaponProcess()
    {
        WpCtr.NowWeaponNo =  WpCtr.NowWeaponNo % (Wdp.Get_WeaponListCount () + 1);
        if( WpCtr.NowWeaponNo == -1) WpCtr.NowWeaponNo = Wdp.Get_WeaponListCount ();
        
        if (WpCtr.NowWeaponNo == 0)
        {
            Weaponicon.sprite = transDot;
        } 
        else
        {
            Weaponicon.sprite = NowWp().icon;
        }

        DefineRenderWeapon();

        beforeWeaponNo = WpCtr.NowWeaponNo;
    }

	void DefineRenderWeapon()
	{
		if (WpCtr.NowWeaponNo != 0)
        {
            isChangeLock = true;
            nextPutModelNo = WpCtr.NowWeaponNo - 1;
            Invoke("StartPutWeapon", 0.32F);
            if (!PhotonNetwork.offlineMode)
            {
                photonView.RPC("EnableRenderWeaponOnNet", PhotonTargets.Others, NowWp().model.GetPhotonView().viewID);
            }
        }

		if (beforeWeaponNo != 0)
        {
            Wdp.WeaponDataList [beforeWeaponNo - 1].model.SetActive(false);
            if (!PhotonNetwork.offlineMode)
            {
                photonView.RPC("DisableRenderWeaponOnNet", PhotonTargets.Others, Wdp.WeaponDataList [beforeWeaponNo - 1].model.GetPhotonView().viewID);
            }
        }
	}

    // if this dont use , Camera strenge behavior. 
    void StartPutWeapon()
    {
        isChangeLock = false;
        Wdp.WeaponDataList[nextPutModelNo].model.SetActive( true);
        return;
    }

	[RPC]// Send to Others.
	void DisableRenderWeaponOnNet( int disableWpViewID)
	{
		PhotonView.Find( disableWpViewID).gameObject.SetActive(false);
	}

	[RPC]// Send to Others.
	void EnableRenderWeaponOnNet( int enableWpViewID)
	{
		PhotonView.Find( enableWpViewID).gameObject.SetActive(true);
	}

	public void EnableChangeLock()
	{
		isChangeLock = true;
	}
	
	public void DisableChangeLock()
	{
		isChangeLock = false;
	}

    public void RegisterPlayer( PlayerData data)
    {
        player = data.player;
        this.enabled = true;
    }


    private WeaponData NowWp()
    {
        return Wdp.WeaponDataList[WpCtr.NowWeaponNo - 1];
    }

    public void ResetBeforeWeaponNo()
    {
        ChangeWeaponProcess();
        beforeWeaponNo = 0;
    }
}