﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;



public class LandmarkIndicatorBehavior : MonoBehaviour 
{
    [SerializeField] Text distanceText;
    [SerializeField] Image landMarkIcon;
    public RADERELEMENT landmark;
    public Vector3 landmarkPos = Vector3.zero;
    [SerializeField] Transform landmarkObject = null;
    [SerializeField] float textUpdateTime = 1F;

    private Transform playerTrans;
    private Transform cameraTrans;
    private Image indicator;
    private float time = 0F;

    public const float WORLD_SCALE_CORRECTION = 3.5F;

	// Use this for initialization
	void Start () 
    {
        landMarkIcon.sprite = Resources.Load<Sprite>("ingame/RaderIcon/Landmark/" + landmark.ToString());
        indicator = this.GetComponent<Image>();
        cameraTrans = Camera.main.transform;

        // If landmarkObject is setting, Get this position.
        if (landmarkObject != null)
        {
            landmarkPos += landmarkObject.position;
        }

        // If player dosent exist.
        if (!GetPlayer())
        {
            return;
        }

        UpdateLandmarkIndicator();
        UpdateDistanceText();
	}
	
	// Update is called once per frame
	void Update () 
    {
        UpdateLandmarkIndicator();

        if (textUpdateTime < time)
        {
            // If player dosent exist.
            if (!GetPlayer())
            {
                return;
            }

            UpdateDistanceText();
            if( CheckLandmarkInView())
            {
                VisibleIndicator();
            }
            else
            {
                InvisibleIndicator();
            }

            time = 0F;
        }

        time += Time.deltaTime;
	}

    // Check landmark see player?
    bool CheckLandmarkInView()
    {
        Vector3 distVec = playerTrans.position - landmarkPos;
        return 90F < Vector3.Angle(distVec, playerTrans.forward);
    }

    void UpdateLandmarkIndicator()
    {

        Vector3 pos = Camera.main.WorldToScreenPoint(landmarkPos);
        this.transform.position = pos;
    }

    void UpdateDistanceText()
    {

        // Update distance.
        Vector3 distVec = playerTrans.position - landmarkPos;
        float distance = distVec.magnitude / WORLD_SCALE_CORRECTION;
        distanceText.text = distance.ToString("0.0");
    }

    void VisibleIndicator()
    {
        distanceText.enabled = true;
        landMarkIcon.enabled = true;
        indicator.enabled = true;
    }
    
    void InvisibleIndicator()
    {
        distanceText.enabled = false;
        landMarkIcon.enabled = false;
        indicator.enabled = false;
    }

    bool GetPlayer()
    {
        if (playerTrans == null)
        {
            GameObject player = GameObject.FindWithTag("Player");
            if (player != null)
            {
                playerTrans = player.transform;
            }
            else
            {
                return false;
            }
        }

        return true;
    }
}
