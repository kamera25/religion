﻿using UnityEngine;
using System.Collections;

public class ThirdShotCamControl : Actor
{
	[SerializeField] private float lookAtY = 10F; 
    [SerializeField] Transform playerCenter ;

	private float sensitivityX = 7F;
	private float sensitivityY = 0.4F;
	private Animator anim;
    private PlayerMoveControl playerMoveCtr;
    private StatusControl statusCtr;

	// Use this for initialization
	void Start () 
	{
        anim = GetAnimator();

        playerMoveCtr = this.GetComponent<PlayerMoveControl>();
        statusCtr = this.GetComponent<StatusControl>();
	}
	
	// Update is called once per frame
	void Update () 
	{

        if( Time.deltaTime != 0F && 0.2 < Time.timeScale && !playerMoveCtr.GetUseLadder() && !statusCtr.isDead)
		{
			float moveCurX = Input.GetAxis("Mouse X") * sensitivityX;
			float rotationX = this.transform.localEulerAngles.y + moveCurX;
			
			this.transform.localEulerAngles = new Vector3(0, rotationX, 0);
			//lookUp = Mathf.Clamp( lookUp - Input.GetAxis("Mouse Y") * sensitivityY, -4, 4);
			anim.SetFloat( "AimY", Mathf.Clamp( anim.GetFloat( "AimY") + Input.GetAxis("Mouse Y") * sensitivityY, -1, 1));
		}
	}



	void LateUpdate()
	{
        Camera.main.transform.position = new Vector3( this.transform.position.x, playerCenter.transform.position.y + lookAtY, this.transform.position.z) + this.transform.forward * -6 + Vector3.up * (  - anim.GetFloat("AimY") * 4);
        Camera.main.transform.LookAt( new Vector3( this.transform.position.x, playerCenter.transform.position.y, this.transform.position.z) + Vector3.up * lookAtY);
	}
}
