﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* !!! Upper the Order of this Component than RayControl (Because this do camera Control.) !!! */
public class ShoulderShotCamControl : Actor 
{

	public Transform cameraPosition ;
	private Transform scopeShotCameraPos ;
    private Animator anim;
	
    public float rotationY = 0F;
	private float sensitivityX = 7F;
	private float sensitivityY  = 7F;
	private float minimumY = -30F;
	private float maximumY = 35F;
	
    private StatusControl statusCtr;

    void Awake()
    {
        anim = GetAnimator();
    }

	// Use this for initialization
	void Start () 
	{
		if( !photonView.isMine )
		{
			this.enabled = false;
		}

        statusCtr = this.gameObject.GetComponent<StatusControl>();
	}

	public void ChangeCamFromThird()
	{
		Camera.main.transform.position = cameraPosition.position;
        rotationY = anim.GetFloat("AimY") * 40;

        return;	
    }

	void LateUpdate()
	{
        Camera.main.transform.position = Vector3.Lerp( Camera.main.transform.position, cameraPosition.position, 0.99F);

        /* Camera LookAt */
        if( Time.deltaTime != 0F && 0.2 < Time.timeScale && !statusCtr.isDead)
        {
            float rotationX = Camera.main.transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;
            rotationY += Input.GetAxis("Mouse Y") * sensitivityY ;
            rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
            Camera.main.transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        }

        return;
	}
}
