﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class NetworkTimerBehavior : Photon.MonoBehaviour 
{

	private DateTime dtEnd;

	public float time = 10F;
    public float releaseTime = 60F;
    private NetworkMenuControl netMenuCtr;

	void Awake()
	{
        #if !UNITY_EDITOR
        time = releaseTime;
        #endif

		this.enabled = false;
	}

	// Use this for initialization
	void Start () 
	{
		// Timer Setting
		dtEnd = DateTime.Now;
		dtEnd = dtEnd.AddSeconds(time);

        netMenuCtr = this.GetComponent<NetworkMenuControl>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if( DateTime.Now < dtEnd)
		{
			TimeSpan dt = dtEnd - DateTime.Now;// Timer
            netMenuCtr.WaitBattle(dt.Seconds);
		}
		else
		{
            netMenuCtr.StartBattle();
            this.enabled = false;
        }
        
    }
    
    
	public void SyncTimer()
	{
		photonView.RPC( "GetTime", PhotonTargets.MasterClient);
	}
	
	[RPC]// only use for MasterClient.
	public void GetTime()
	{
		photonView.RPC( "SetTime", PhotonTargets.Others, dtEnd.ToString());
	}
	
	[RPC]
	public void SetTime( string t)
	{
		dtEnd = DateTime.Parse( t);
	}
}
