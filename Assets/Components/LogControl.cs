﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Log 
{
	string messeage ;
	float createTime;

	public string Getmesseage()
	{
		return messeage;
	}
	public float GetcreateTime()
	{
		return createTime;
	}
	public void Setmesseage( string i)
	{
		messeage = i;
	}
	public void SetcreateTime( float i)
	{
		createTime = i;
	}
}

public class LogControl : Photon.MonoBehaviour 
{
	
	public List<Log> LogList = new List<Log>();
    private const int MAX_LOG = 10;
	
	void  OnGUI ()
    {
		if( Time.timeScale < 0.1 || 0 == LogList.Count) return;

		GUI.Box ( new Rect ( 10, 150, 250, 200), "");

		int i = 0;
		foreach ( Log log in LogList)
		{
			GUI.Label( new Rect( 20, 150 + 20 * i, 240, 40), log.Getmesseage());
			log.SetcreateTime( log.GetcreateTime() - Time.deltaTime);
			i++;
		}

		// Delete Log.
        Log logD = LogList
                    .Where(log => log.GetcreateTime() < 0.0f)
                    .FirstOrDefault();
        if (logD != null)
        {
            LogList.Remove( logD);
        }

        return;
	}

	public void AddLogOnNet( string Messeage)
	{
		photonView.RPC( "AddLogMesseage", PhotonTargets.All, Messeage);
	}

	[RPC]
	public void AddLogMesseage (  string Messeage)
	{
		Log newlog= new Log();
		newlog.Setmesseage( Messeage);
		newlog.SetcreateTime( 10.0f);
		
        LogList.Add(newlog);
        if (MAX_LOG < LogList.Count)
        {
            LogList.RemoveAt(0);
        }
	}

}