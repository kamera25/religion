﻿using UnityEngine;
using System.Collections;

public class SenceTransitionControl : MonoBehaviour 
{

    public string scene;
	
	public void SenceTransition()
    {
        Camera.main.SendMessage("fadeOut");
        GameObject.FindWithTag("GameController").GetComponent<AudioSource>().Play();
        Invoke("Transition", 2F);
    }

    void Transition()
    {
        Destroy( GameObject.Find("Canvas"));
        Destroy( GameObject.Find("EventSystem"));
        
        Application.LoadLevelAdditive(scene);
        Camera.main.SendMessage("fadeIn");
        this.enabled = false;
    }
}
