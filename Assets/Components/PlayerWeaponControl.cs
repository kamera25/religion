﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerWeaponControl : WeaponControl 
{
    public int NowWeaponNo;

    private GameObject reloadImg;

    private RayControl rayCtr;
    private WeaponChangeControl wpChangeCtr;

    void  Start ()
    {
        NowWeaponNo = 0;

        /* Find components */
        rayCtr = this.GetComponent<RayControl>();
        wpChangeCtr = this.GetComponent<WeaponChangeControl>();
        
        Wdp = this.GetComponent<WeaponDataProcess>();
        for( int i = 0; i < Wdp.Get_WeaponListCount(); i++)
        {
            Wdp.WeaponDataList[i].ch_time = 0.0f;
        }
        
        //disable Reload Image.
        reloadImg = GameObject.Find("ReloadImg");
        HideReloadImg();       
    }
    
    
    void  Update ()
    {
        
        /**/


        if (anim == null || Time.timeScale == 0)
        {
            return;
        }

        //  When player no has weapon, Don't Move!!
        if (NowWeaponNo == 0)
        {
            anim.SetInteger("weaponKind", -1);
            return;
        } 

        // Decrease Timer.
        NowWp().ch_time -= Time.deltaTime;
        NowWp().nowReload -= Time.deltaTime;

        anim.SetInteger("weaponKind", (int)NowWp().wpData.motion);

        /* Weapon Operation. */
        ReloadProcess();


        if (IsWeaponUse())
        {
            wpChangeCtr.DisableChangeLock();
        }
        if (!IsLeftAmmo() && NowWp().type != WeaponKind.SUPPORT)
        {
            return;
        }

        /* Debug!! NowWeaponNo is NOT accpted by this point!!  */
        switch( NowWp().type )
        {
            case WeaponKind.HAND:
                OnceClickFire();
                break;
            case WeaponKind.ASSAULT:
                SeveralClickFire();
                break;
            case WeaponKind.SMG:
                SeveralClickFire();
                break;
            case WeaponKind.SHOT:
                ShotGunClickFire();
                break;
            case WeaponKind.MACHINE:
                SeveralClickFire();
                break;
            case WeaponKind.RIFLE:
                OnceClickFire();
                break;
            case WeaponKind.GRENADE:
                GranadeClickFire( NowWp().wpData.name);
                break;
            case WeaponKind.SHIELD:
                break;
            case WeaponKind.SUPPORT:
                SupportClickFire( NowWp().wpData.name);
                break;
            case WeaponKind.SORD:
                SordClickFire( NowWp().wpData.name);
                break;
        }


        
        return;
    }



    /* ***************
     * Click Process *
     * **************/
    void OnceClickFire ()
    {
        if( !Input.GetButtonDown("Fire") || !IsWeaponUse()) return;
        GunFireProcess();
        wpChangeCtr.EnableChangeLock();

        return;
    }
    
    void SeveralClickFire ()
    {
        if( !Input.GetButton("Fire") || !IsWeaponUse()) return;
        GunFireProcess();
        wpChangeCtr.EnableChangeLock();
        
        return;
    }
    
    void ShotGunClickFire()
    {
        if( !Input.GetButtonDown("Fire") || !IsWeaponUse()) return;
        ShotGunFireProcess();
        wpChangeCtr.EnableChangeLock();
        
        return;
    }

    void GranadeClickFire( string Name)
    {

        if ( !Input.GetButtonDown("Fire") || !IsWeaponUse()) return;  
        GranadeWeaponFire(Name);
        wpChangeCtr.EnableChangeLock();
        
        return;
    }

    void SupportClickFire(string Name)
    {
        GranadeUpdateProc();// Ordit Process.

        if( !Input.GetButtonDown("Fire") || !IsWeaponUse()) return;
        SupportWeaponFire( Name);
        wpChangeCtr.EnableChangeLock();
        
        return;
    }

    void SordClickFire( string Name)
    {
        if( !Input.GetButtonDown("Fire") || !IsWeaponUse()) return;
        SordWeaponFire(Name);
        wpChangeCtr.EnableChangeLock();

        return;
    }

    void GranadeUpdateProc()
    {
        // Ordit Process.
        if ( NowWp().granadeCtr != null)
        {
            bool upThrow = anim.GetFloat("AimY") > 0;
            NowWp().granadeCtr.SetThrowPosture( upThrow);
        }
    }

    /* *** */
    /* End */
    /* *** */

    // Follow methods is related Ray.

    protected override RaycastHit GetHitObject()
    {
        return rayCtr.GetRay();
    }

    protected override Vector3 GetRayPosition()
    {
        Vector3 DistPos = rayCtr.GetRayPositionForWeapon();
        if (DistPos == Vector3.zero)// if ray DON`T collide, use Distination of ray.
        {
            DistPos = rayCtr.GetRayPoint(10000F); 
        }

        return DistPos;
    }

    protected override bool ChkRayHitHead()
    {
        return rayCtr.ChkRayHitHead();
    }

    // END

    public override WeaponData NowWp()
    {
        return Wdp.WeaponDataList[NowWeaponNo - 1];
    }

    protected override int PlayerGUIEndReloadProc()
    {
        HideReloadImg();
        //wpChangeCtr.disableChangeLock();
        if (Input.GetButtonDown("Reload") || (NowWp().nowammo == 0 && Input.GetButtonDown("Fire")))
        {
            return 1;
        }

        return 0;
    }

    protected override void PlayerGUIStartReloadProc()
    {
        wpChangeCtr.EnableChangeLock();
        PutReloadImg();
        return;
    }

    void  PutReloadImg()
    {
        reloadImg.SetActive(true);
    }
    
    void  HideReloadImg()
    {
        reloadImg.SetActive(false);
    }

    public void ResetNowWeaponNo()
    {
        NowWeaponNo = 0;
        wpChangeCtr.ResetBeforeWeaponNo();
    }
}
