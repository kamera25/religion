﻿using UnityEngine;
using System.Collections;

public class BaseCamp 
{
    public GameObject campObj = null;
    public GameObject campPoint { set; get;}
	public TEAM team;
	
	public void SetcampObject( GameObject Obj)
	{
		campObj = Obj;
	}
	
	public GameObject GetcampObject()
	{
		return campObj;
	}
}