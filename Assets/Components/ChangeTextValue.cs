﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeTextValue : MonoBehaviour 
{

    private Text text;
    [SerializeField] bool isTimer = false;
    [SerializeField] Slider slider;
    private int MAXSLIDERVAL;

	// Use this for initialization
	void Start () 
    {
        text = this.GetComponent<Text>();
        MAXSLIDERVAL = (int)slider.maxValue;
	}

    public void ChangeValue()
    {
        int ticket = (int)(slider.value);


        if (isTimer && ticket == MAXSLIDERVAL)
        {
            text.text = "∞";
        } else
        {
            text.text = ticket.ToString();
        }
    }
}
