using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;


public class BackPackControl : ItemProcessForMenu
{
    private ItemControl.ITEMKIND menuSelectKind = ItemControl.ITEMKIND.WEAPON;
    private ItemControl playerItemCtr;
    private Animator backAnim;

    private Image[] itemButtonImage;

    private Scrollbar scrollBar;

    public int nowPage = 0;
    private int maxPage = 10;

    private int itemNo = -1;
    private ItemControl.ITEMKIND itemKind;

    [SerializeField] GameObject itemDetailUI;
    [SerializeField] GameObject notFoundItemText;

    [SerializeField] private Text itemNameText;
    [SerializeField] private Text itemDetailText;
    [SerializeField] private Image itemImage;

    [SerializeField] private Button eatButton;
    [SerializeField] private Button dropButton;
    [SerializeField] private Button takeButton;

    private const int screenItemNum = 6;
    private int backPage;

	// Use this for initialization
	void Start () 
    {
        backAnim = GameObject.Find("WeaponButton").GetComponent<Animator>();
        backAnim.SetBool("Select", true);

        scrollBar = GameObject.Find("PageScrollbar").GetComponent<Scrollbar>();

        itemButtonImage = new Image[screenItemNum];
        for (int i = 0; i < screenItemNum; i++)
        {
            GameObject item = GameObject.Find("Item" + i);
            itemButtonImage[i] = item.GetComponent<Image>();
        }


        playerItemCtr = GameObject.FindWithTag("GameController").GetComponent<ItemControl>();

        ChangeItemKind();
        SetKindToWeapon();


        backPage = nowPage;
	} 

	// Update is called once per frame
	void Update () 
    {
	    if (itemNo != -1)
        {
            itemDetailUI.SetActive(true);
        } 
        else
        {
            itemDetailUI.SetActive(false);
        }

        if( nowPage != (int)(scrollBar.value * (maxPage-1)))
        {
            nowPage = (int)(scrollBar.value * (maxPage-1));
            LoadPage();
        }

        if (Input.GetButtonDown("ESC"))
        {
            GameObject.Destroy( this.transform.parent.gameObject);
        }
	}

    public void SetKindToWeapon()
    {
        menuSelectKind = ItemControl.ITEMKIND.WEAPON;
        PopupItemIndicator("WeaponButton");
        LoadPage();
    }

    public void SetKindToAmmo()
    {
        menuSelectKind = ItemControl.ITEMKIND.AMMO;
        PopupItemIndicator("AmmoButton");
        LoadPage();
    }

    public void SetKindToKey()
    {
        menuSelectKind = ItemControl.ITEMKIND.KEY;
        PopupItemIndicator("KeyButton");
        LoadPage();
    }

    public void SetKindToEquipment()
    {
        menuSelectKind = ItemControl.ITEMKIND.EQUIPMENT;
        PopupItemIndicator("EquipmentButton");
        LoadPage();
    }

    public void SetKindToRecovery()
    {
        menuSelectKind = ItemControl.ITEMKIND.RECOVERY;
        PopupItemIndicator("RecoveryButton");
        LoadPage();
    }

    private void PopupItemIndicator( string buttonName)
    {
        backAnim.SetBool("Select", false);
        backAnim = GameObject.Find(buttonName).GetComponent<Animator>();
        backAnim.SetBool("Select", true);

        ChangeItemKind();
    }

    private void RefleshItemList()
    {
        maxPage = playerItemCtr.MaxElemetNum( menuSelectKind) / (screenItemNum + 1) + 1;
        
        // Scroll Bar Behavior.
        scrollBar.value = 0;
        scrollBar.size = 1F / (float)maxPage;
        scrollBar.numberOfSteps = maxPage;
    }

    private void ChangeItemKind()
    {
        nowPage = 0;
        RefleshItemList();
    }

    public void IncrementPage()
    {
        nowPage++;
        nowPage = Mathf.Clamp( nowPage, 0, maxPage-1);
        scrollBar.value = (float)nowPage / ((float)maxPage-1F);
        LoadPage();
    }

    public void DecrementPage()
    {
        nowPage--;
        nowPage = Mathf.Clamp( nowPage, 0, maxPage-1);
        scrollBar.value = (float)nowPage / ((float)maxPage-1F);
        LoadPage();
    }

    private void LoadPage()
    {

        notFoundItemText.SetActive(false);

        // Set Transparent dots.
        for (int i = 0; i < screenItemNum; i++)
        {
            itemButtonImage[i].gameObject.SetActive(false);
        }
     
        // Set a item sprite to backpack menu.
        int j = screenItemNum * nowPage;
        for (int i = 0; i < screenItemNum; i++)
        {
            if( playerItemCtr.MaxElemetNum( menuSelectKind) <= j) 
            {
                break;
            }

            itemButtonImage[i].gameObject.SetActive(true);
            itemButtonImage[i].sprite = GetItemSprite( j, playerItemCtr, menuSelectKind);

            j++;
        }

        if (j == screenItemNum * nowPage)
        {
            notFoundItemText.SetActive(true);
        }


    }


    private void SetItemNo( string elementStr)
    {
        int elementNo = int.Parse(elementStr);

        itemNo = nowPage * screenItemNum + elementNo;
        itemKind = menuSelectKind;

        // Update itemDetailUI
        int id = playerItemCtr.GetListData(itemNo, menuSelectKind);;
        itemNameText.text = GetItemName(id, menuSelectKind);
        itemDetailText.text = GetItemDetail(id, menuSelectKind);
        itemImage.sprite = itemButtonImage[elementNo].sprite;

        SettingDetailButton();
    }

    public void UseItem()
    {

        switch (itemKind)
        {
            case ItemControl.ITEMKIND.RECOVERY:
     
                // Get ItemID from a select Item Number. 
                int itemID = playerItemCtr.GetListData( itemNo, ItemControl.ITEMKIND.RECOVERY);

                // Get ItemData from ID.
                RECOVERYKIND kind = GetRecoveryKind( itemID);
                int num1 = GetRecoveryNum1( itemID);


                // Apply a item. 
                switch(kind)
                {
                    case RECOVERYKIND.HP_RECOVER:
                        GameObject.FindWithTag("Player").SendMessage("RecoveryHPUsingRate", (float)num1);
                        break;
                    case RECOVERYKIND.STMNRE_COVER:
                        float time = (float) GetRecoveryNum2( itemID);
                        GameObject.FindWithTag("Player").GetComponent<StatusControl>().RecoveryStaminaSpeedUp( (float)num1, time);
                        break;
                }
                break;
        }

        DropItem();
    }

    public void DropItem()
    {
        switch (itemKind)
        {
            case ItemControl.ITEMKIND.RECOVERY:
                playerItemCtr.RemoveRecovery(itemNo);
                break;
        }

        RefleshItemList();
        LoadPage();

        itemNo = -1;
    }

    private void SettingDetailButton()
    {
        switch (itemKind)
        {
            case ItemControl.ITEMKIND.RECOVERY:
                dropButton.gameObject.SetActive(true);
                eatButton.gameObject.SetActive(true);
                takeButton.gameObject.SetActive(true);
                break;
            default:
                dropButton.gameObject.SetActive(true);
                eatButton.gameObject.SetActive(false);
                takeButton.gameObject.SetActive(true);
                break;
        }
    }
    

}
