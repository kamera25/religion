using UnityEngine;
using System.Collections;

public class PlayerMoveControl : CharactorYoke 
{
    private RayControl rayCtr;
	private AudioSource footSE;

	private float runSpeed = 38F;

	// Relation Emergency Avoidance Operation.
	public float runUseStamina = 200F;
	private float backHorizontal = 0.0f;
	private float backVertical = 0.0f;
    private float acceptTime = 0F;
    private float airTime = 0F;
    private bool isJoypad = false;

    private STATE backState ;

    private PlayerWeaponControl wpCtr;
    private WeaponChangeControl wpChangeCtr;

    private TWICEKEY twiceMode = TWICEKEY.NONE;
    enum TWICEKEY
    {
        NONE = 0,
        RUN,
        RIGHT,
        LEFT,
        BACK
    }

	protected override void Start()
	{
        base.Start();

        backState = state;

		if( !photonView.isMine && !PhotonNetwork.offlineMode)
		{
			this.enabled = false;
			return;
		}

		stateCtl = this.GetComponent<StatusControl>();
		charaCtl = this.GetComponent<CharacterController>();

        aniCtl = GetAnimator();

		footSE = this.transform.FindChild("Foot").GetComponent<AudioSource>();

        GameObject controller = GameObject.FindWithTag("GameController");
		rayCtr = controller.GetComponent<RayControl>();
        isJoypad = controller.GetComponent<PutUnitControl>().isJoypad;

        poseModeCtl = controller.GetComponent<PoseModeControl>();
        wpCtr = controller.GetComponent<PlayerWeaponControl>();
        wpChangeCtr = controller.GetComponent<WeaponChangeControl>();
	}



	void  Update ()
	{

        if (Time.timeScale == 0)
        {
            return;
        }

        StateChangeBehavior();
        isAirUpdate();
        IsDeadProcess();

        OnChangeStateProcess();
        backState = state;

        switch (state)
        {
            case STATE.NORMAL:
                OnGroundProcess();
                break;
            case STATE.RUN:
                RunningProcess();
                break;
            case STATE.FINISHLADDER:
                FinishClimbLadder();
                break;
            case STATE.ONLADDER:
                OnLadderProcess();
                break;
            case STATE.AIR:
                OnAirProcess();
                break;
            case STATE.BACKAVOID:
                RunAvoidance(0.0f, -1.0f);
                break;
            case STATE.LEFTAVOID:
                RunAvoidance(-1.0f, 0.0f);
                break;
            case STATE.RIGHTAVOID:
                RunAvoidance(1.0f, 0.0f);
                break;
            case STATE.SQUAT:
                SquatProcess();
                break;
        }       

        SetBodyDirection();
	}


    /// <summary>
    /// 梯子に登っている時に実行される.
    /// </summary>
    void OnLadderProcess()
    {
        float vertical = Input.GetAxis("Vertical");
        ClimbLadder( vertical);

        if ( Input.GetButtonUp("Search"))
        {
            poseModeCtl.EnablecanChange();
            DisableUseLadder();
        }
    }

    /// <summary>
    /// しゃがみ時に実行される.
    /// </summary>
    void SquatProcess()
    {
        if (Input.GetButtonUp("Squat Button"))
        {
            aniCtl.SetBool( "isSquat", false);
            state = STATE.NORMAL;
        }
    }

    /// <summary>
    /// 走っているときに実行される.
    /// </summary>
    void RunningProcess()
    {      
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 moveDirection = new Vector3( horizontal, 0, vertical);
        float useStamina = runUseStamina * Time.deltaTime;

        if (vertical <= 0F || !stateCtl.isLeaveStamina( useStamina))
        {
            DisableRun();

            return;
        }

        aniCtl.SetBool( "isDash", true);
        DisableLayerWeight();

        moveDirection = DirectionWithNomalize( moveDirection, runSpeed, 0F, false);
        footSE.pitch = 1.7F;

        if ( isAir)
        {
            aniCtl.SetBool( "onAir", true);
            moveDirection.y += 30F;
            state = STATE.AIR;
            airTime = 0F;
            footSE.pitch = 1.25F;
            EnableLayerWeight();
        }
        MoveWithGravity( moveDirection);

        stateCtl.StunReciver( useStamina);// decrease Stamina.
        DisableTransitionOfController();

        if (!footSE.isPlaying)
        {
            footSE.Play();
        }
    }

    /// <summary>
    /// 走るのやめる時の処理.
    /// </summary>
    void DisableRun()
    {
        state = STATE.NORMAL;
        footSE.pitch = 1.25F;
        aniCtl.SetBool( "isDash", false);
        EnableLayerWeight();

        EnableTransitionOfController();
    }

    /// <summary>
    /// コントローラにあるいくつかの操作を無効化する.
    /// </summary>
    void DisableTransitionOfController()
    {
        poseModeCtl.DisablecanChange();
        wpCtr.canPlayerFire = false;
        wpChangeCtr.EnableChangeLock();
    }
    
    /// <summary>
    /// コントローラにあるいくつかの操作を有効化する.
    /// </summary>
    void EnableTransitionOfController()
    {
        poseModeCtl.EnablecanChange();
        wpCtr.canPlayerFire = true;
        wpChangeCtr.DisableChangeLock();
    }

    /// <summary>
    /// 空中にいる時の処理.
    /// </summary>
    void OnAirProcess()
    {
        ApplyGravityAndResistAir();

        footSE.Stop();

        if ( !isRoll() && 2F < airTime)
        {
            aniCtl.SetBool( "onAir", false);
        }

        if ( charaCtl.isGrounded)
        {
            aniCtl.SetBool( "isDash", false);
            aniCtl.SetBool( "onAir", false);
            EnableTransitionOfController();
            airTime = 0F;
            state = STATE.NORMAL;
        }

        airTime += Time.deltaTime;
    }

    /// <summary>
    /// 歩いていたり、止まっている時の処理.
    /// </summary>
    void OnGroundProcess()
    {
        float horizontal = 0F;
        float vertical = 0F;

        if ( !stateCtl.isDead)
        {
            horizontal = Input.GetAxis("Horizontal");
            vertical = Input.GetAxis("Vertical");
        }

        Vector3 move = DirectionWithNomalize( new Vector3( horizontal, 0F, vertical), speed, 0F, true);
        MoveWithGravity( move);

        footSE.pitch = 1.25F;

        if ( isAir)
        {
            aniCtl.SetBool( "onAir", true);
            state = STATE.AIR;
            return;
        }

        if (move.sqrMagnitude > 0)
        {
            if (!footSE.isPlaying)
            {
                footSE.Play();
            }
        } else
        {
            StopFootSE();
        }

        if (Input.GetButtonUp("Squat Button"))
        {
            StopFootSE();
            aniCtl.SetBool( "isSquat", true);
            state = STATE.SQUAT;
        }
    }

    /// <summary>
    /// 二回押しの前処理.
    /// </summary>
    void StateChangeBehavior()
    {
        if (!(state == STATE.NORMAL || state == STATE.RUN))
            return;

        if (isJoypad)
        {
            JoypadProcess();
        } 
        else
        {
            TwiceKeyProcess();
        }
    }

    void JoypadProcess()
    {
        float vertical = Input.GetAxisRaw("Vertical");
        float horizontal = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Run Button"))
        {
            state = STATE.RUN;
        } 
        else if (Input.GetButton("Avoid Button"))
        {
            if (0.95f < horizontal)
            {
                state = STATE.RIGHTAVOID;
            } else if (horizontal < -0.95f)
            {
                state = STATE.LEFTAVOID;
            }
        }

        // Disable if button up.
        if( ( !Input.GetButton("Run Button") || vertical < 0.8f) && state == STATE.RUN)
        {
            DisableRun();
        }

        backVertical = vertical;
        backHorizontal = horizontal;
    }

    /// <summary>
    /// 2回押しアクションの実行などの処理.
    /// </summary>
	void TwiceKeyProcess()
	{
        if (state == STATE.LEFTAVOID || state == STATE.RIGHTAVOID)
        {
            return;
        }

        float vertical = Input.GetAxisRaw("Vertical");
		float horizontal = Input.GetAxisRaw("Horizontal");



		if( vertical == 0 && backVertical == -1)// Back Avoidance.
		{
       
            if( 0.0f < acceptTime && twiceMode == TWICEKEY.BACK)
            {
                state = STATE.BACKAVOID; 
            }
            else
            {
                acceptTime = 0.3f;
            }

            twiceMode = TWICEKEY.BACK;
		}
        else if( horizontal == 1 && backHorizontal == 0)// Right Avoidance.
		{
            if( 0.0f < acceptTime && twiceMode == TWICEKEY.RIGHT)
            {   
                aniCtl.SetFloat("Horizontal", 1F);
                state = STATE.RIGHTAVOID;
            }
            else
            {
                acceptTime = 0.3f;
            }

            twiceMode = TWICEKEY.RIGHT;
		}
        else if( horizontal == 0 && backHorizontal == -1) // Left Avoidance.
		{
			if( 0.0f < acceptTime && twiceMode == TWICEKEY.LEFT)
            {
                aniCtl.SetFloat("Horizontal", -1F);
                state = STATE.LEFTAVOID;
            }
			else
            {
                acceptTime = 0.3f;
            }

            twiceMode = TWICEKEY.LEFT;
		}	
        else if( vertical == 1 && backVertical == 0) // Foward Avoidance.
		{
			if( 0.0f < acceptTime && twiceMode == TWICEKEY.RUN)
			{
                state = STATE.RUN;
                poseModeCtl.ForceSetThiedPose();
                poseModeCtl.DisablecanChange();
			}
			else
            {
                acceptTime = 0.5f;
            }

            twiceMode = TWICEKEY.RUN;
		}	

        acceptTime -= Time.deltaTime;

        backVertical = vertical;
        backHorizontal = horizontal;

	}


	void  SetBodyDirection ()
	{
        switch( poseModeCtl.PoseMode)
        {
            case POSE.SHOULDER:
                LookAtBody( rayCtr.GetRayPositionForWeapon(), 0.5F);
                break;
            case POSE.SUBJECT:
                LookAtBody( rayCtr.GetRayPositionForWeapon(), 30F);
                break;
        }
	}

    private void OnTriggerStay(Collider col)
    {

        if ( col.CompareTag("LadderBottom"))
        {
            if (Input.GetButtonDown("Search") && ClimbStartLadder( col.transform))
            {
                StopFootSE();
                poseModeCtl.ForceSetThiedPose();
                poseModeCtl.DisablecanChange();
            } 
        }
        else if ( col.CompareTag("LadderTop") && ( state == STATE.ONLADDER || state == STATE.FINISHLADDER))
        {
            state = STATE.FINISHLADDER;
            aniCtl.SetBool( "finishLadder", true);
            countGoUpLadder = 10;
        }
    }

    public bool GetUseLadder()
    {
        if (state == STATE.ONLADDER || state == STATE.FINISHLADDER)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// ステートに変更があった時の処理を行う.
    /// </summary>
    void OnChangeStateProcess()
    {
        if (state == backState)
        {
            return;
        }

        if (backState == STATE.RUN && state == STATE.NORMAL)
        {
            DisableRun();
            poseModeCtl.EnablecanChange();
        }


    }

    private void StopFootSE()
    {
        if (footSE.isPlaying)
        {
            footSE.Stop();
        }
    }

}
