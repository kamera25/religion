﻿using UnityEngine;
using System.Collections;

public class BulletMarkControl : Photon.MonoBehaviour 
{

	private float time = 6;
	private float maxTime;

	void Start()
	{
        if( !photonView.isMine && !PhotonNetwork.offlineMode)
        {
            enabled = false;
        }

		maxTime = time;
	}

	
	void Update () 
	{
		
		time -= Time.deltaTime;
		/* Mark change immidiately transparent.. */
		Color color = this.GetComponent<Renderer>().material.color;
		color.a = time / maxTime;
		this.GetComponent<Renderer>().material.color = color;

		if( time <= 0 )
		{
		    PhotonNetwork.Destroy( this.gameObject);
		}
	}
	
	void DamageReciver( float damage )
	{
		time = 0F;
	}
}
