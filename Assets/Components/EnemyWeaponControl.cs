using UnityEngine;
using System.Collections;

public class EnemyWeaponControl : WeaponControl
{
    private WeaponDataProcess Wpd;
    RaycastHit hit;

    void Start()
    {
        Wpd = GameObject.FindWithTag("GameController").GetComponent<WeaponDataProcess>();
    }

    void Update()
    {
        anim.SetInteger("weaponKind", (int)NowWp().wpData.motion);
    }

	public void WeaponFire()
    {
        if( Time.timeScale == 0) return;/* Don't Move!! */
        
        NowWp().ch_time -= Time.deltaTime;
        NowWp().nowReload -= Time.deltaTime;
        anim.SetInteger("weaponKind", (int)NowWp().wpData.motion);

        /* Weapon Operation. */
        ReloadProcess();
        if( !IsLeftAmmo() && NowWp().type != WeaponKind.SUPPORT) return;
        anim.speed = 1F;

        /* Debug!! NowWeaponNo is NOT accpted by this point!!  */
        switch( NowWp().type )
        {
            case WeaponKind.HAND:
                EnemyGunFire();
                break;
            case WeaponKind.ASSAULT:
                EnemyGunFire();
                break;
            case WeaponKind.SMG:
                EnemyGunFire();
                break;
            case WeaponKind.SHOT:
                //ShotGunFireProcess( GetWeaponDamage());
                break;
            case WeaponKind.MACHINE:
                EnemyGunFire();
                break;
            case WeaponKind.RIFLE:
                EnemyGunFire();
                break;
            case WeaponKind.GRENADE:
                //GranadeClickFire( GetWeaponDamage(), NowWp().name);
                break;
            case WeaponKind.SHIELD:
                break;
            case WeaponKind.SUPPORT:
                //SupportClickFire( GetWeaponDamage(), NowWp().name);
                break;
        }
        
        return;
    }

    void EnemyGunFire()
    {
        if( !IsWeaponUse()) return;
        GunFireProcess();
    }

    protected override RaycastHit GetHitObject()
    {

        Physics.Raycast( NowWp().model.transform.position, NowWp().model.transform.forward, out hit, 1000.0f);

        return hit;
    }

    protected override Vector3 GetRayPosition()
    {
        Vector3 DistPos = hit.point;
        
        return DistPos;
    }

    protected override int PlayerGUIEndReloadProc()
    {
        if( NowWp().nowammo == 0 ) return 1;
        
        return 0;
    }

    public override WeaponData NowWp()
    {
        return Wpd.GetNowUseWeaponFromID( this.gameObject.GetInstanceID(), false);
    }
}
