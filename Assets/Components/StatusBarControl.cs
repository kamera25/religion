﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatusBarControl : MonoBehaviour {

	private Sprite normalSprite;
	private Sprite emergencySprite;
	private GameObject player;

	private RectTransform lifebarObj;
	private RectTransform staminabarObj;
    private Image lifebarImg ;

	private float widthSizeHP ;
	private float widthSizeStamina ;
    private float beforeHP;

    private float nowPutHP;
    private float nowPutStamina;
    const float timeDump = 0.08F;

	private StatusControl statusCtl;

	// Use this for initialization
	void Start () 
	{
	
        normalSprite = Resources.Load<Sprite>("ingame/Lifebar/normal");
        emergencySprite = Resources.Load<Sprite>("ingame/Lifebar/emgr");

		lifebarObj = GameObject.Find("LifeBar").GetComponent<RectTransform>();
		staminabarObj = GameObject.Find("StaminaBar").GetComponent<RectTransform>();
        lifebarImg = lifebarObj.FindChild("Image").GetComponent<Image>();
		
        widthSizeHP = lifebarObj.localScale.x;
		widthSizeStamina = staminabarObj.localScale.x;

		if( player == null) this.enabled = false;
		SetStatusControl();
       
	}
	
	// Update is called once per frame
	void Update () 
	{
        PutLifeBar();
        PutStaminaBar();
	}

    private void PutLifeBar()
    {
        Vector3 lifeScale = lifebarObj.localScale;

        nowPutHP = Mathf.Lerp( nowPutHP, statusCtl.hp, timeDump);

        lifeScale.x = nowPutHP / statusCtl.myData.maxhp * widthSizeHP;
        lifebarObj.localScale = lifeScale;

        if ( beforeHP != statusCtl.hp)
        {
            //if Charactor's HP is less than 20%
            if (statusCtl.hp / statusCtl.myData.maxhp < 0.2)
            {
                lifebarImg.sprite = emergencySprite;
            } 
            else
            {
                lifebarImg.sprite = normalSprite;
            }
            
            beforeHP = statusCtl.hp;
        }
    }

    private void PutStaminaBar()
    {
        Vector3 staminaScale = staminabarObj.localScale;

        nowPutStamina = Mathf.Lerp( nowPutStamina, statusCtl.stamina, timeDump);

        staminaScale.x = nowPutStamina / statusCtl.myData.stamina * widthSizeStamina;
        staminabarObj.localScale = staminaScale;
    }

    public void RegisterPlayer( PlayerData data)
	{
		player = data.player;
		SetStatusControl();
		this.enabled = true;
	}

    private void SetStatusControl()
    {
        if( player == null) return;

        statusCtl = player.GetComponent<StatusControl>();
        nowPutHP = statusCtl.hp;
        nowPutStamina = statusCtl.stamina;
    }
}
