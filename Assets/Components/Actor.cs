﻿using UnityEngine;
using System.Collections;

public enum PLAYERSTATE
{
    DISABLE,
    ENABLE
}

public class Actor : Photon.MonoBehaviour 
{
    private static PLAYERSTATE nowPlayerState = PLAYERSTATE.ENABLE ;

    private bool backEnable = true;

    /// <summary>
    /// Ger Animator from a charactor model.
    /// </summary>
    protected Animator GetAnimator()
    {
        GameObject actor = null;

        foreach( Transform child in this.transform)
        {
            if( child.CompareTag("Actor"))
            {
                actor = child.gameObject;
                break;
            }
        }

        if (actor == null)
        {
            return null;
        }

        return actor.GetComponent<Animator>();
    }

    public void SetState( PLAYERSTATE state)
    {
        nowPlayerState = state;
        if (nowPlayerState == PLAYERSTATE.DISABLE)
        {
            this.gameObject.SendMessage("DisableComponent");
        } else
        {
            this.gameObject.SendMessage("EnableComponent");
        }
    }

    public void DisableComponent()
    {
        this.enabled = false;
    }

    public void EnableComponent()
    {
        this.enabled = true;
    }

}
