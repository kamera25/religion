﻿using UnityEngine;
using System.Collections;

public class ReturnToTopMenu : Photon.MonoBehaviour 
{

	void ReturnToTop()
    {

        Destroy( GameObject.Find("MenuController"));
        if (PhotonNetwork.isMasterClient)
        {
            PhotonNetwork.DestroyAll();
        }

        PhotonNetwork.Disconnect();
        Application.LoadLevel("topmenu");
    }
}
