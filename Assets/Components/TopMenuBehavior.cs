﻿using UnityEngine;
using System.Collections;

public enum TOPMENUMODE
{
    TOP,
    STORY,
    ONLINE,
    TRAINING,
    END
}

public class TopMenuBehavior : MonoBehaviour 
{
      
    public static TOPMENUMODE Mode = TOPMENUMODE.TOP; 
        
    void  Start ()
    {
        if( GameObject.Find("Audio") == null)
        {
            GameObject.Find("Audio_menu").GetComponent<AudioSource>().Play();
            this.GetComponent<AudioListener>().enabled = true;
        }
        
        Time.timeScale = 1F;
    }
    
    public void ChangeOnlineMode()
    {
        Mode = TOPMENUMODE.ONLINE;
    }

    public void ChangeTrainingMode()
    {
        Mode = TOPMENUMODE.TRAINING;
    }
    
    void  PutEndMenu ()
    {
        Application.Quit();
    }
    
    void  PlaySE (){
        this.GetComponent<AudioSource>().Play();
    }
    
    /* Check MouseCursor position on Rect. */
    bool isFocueRect (  int X ,   int Y ,   int W ,   int H  )
    {
        Vector3 MouseCur = Input.mousePosition;
        MouseCur.z = 10.0F;
        MouseCur = Camera.main.ScreenToWorldPoint(MouseCur);
        
        //Debug.Log(MouseCur.x + " " + MouseCur.y);
        if( X < MouseCur.x && MouseCur.x < X + W
           &&  Y < MouseCur.y && MouseCur.y < Y + H)
        {
            return true;
        }
        
        return false;
    }
}

