﻿using UnityEngine;
using System.Collections;

public class RuleDialogBehavior : MonoBehaviour 
{
    [SerializeField] RectTransform detailScrollView;
    private OnlineModeControl onlineModeCtr;

	// Use this for initialization
	void Start () 
    {
        onlineModeCtr = GameObject.FindWithTag("NetworkController").GetComponent<OnlineModeControl>();
        string mode = onlineModeCtr.battleMode.ToString();

        Transform modeDetail = detailScrollView.FindChild(mode);
        modeDetail.gameObject.SetActive(true);
	}
	
	
}
