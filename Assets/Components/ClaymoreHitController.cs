﻿using UnityEngine;
using System.Collections;

public class ClaymoreHitController : MonoBehaviour 
{
    DamageBang damageCtr;

    void Start()
    {
        damageCtr = this.transform.parent.GetComponent<DamageBang>();
        this.transform.position = damageCtr.transform.position + damageCtr.transform.forward * 3.0f;
    }
    
    void OnTriggerEnter (  Collider Col)
    {
        if( Col.CompareTag( "Player") || Col.CompareTag( "Enemy"))
        {
            damageCtr.ForceBang();
        }
        
        return;
    }
    
    void DamageReciver ( float damage)
    {
        damageCtr.DamageReciver(damage);
        return;
    }
}
