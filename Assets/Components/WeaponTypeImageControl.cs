﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeaponTypeImageControl : MonoBehaviour 
{
    public WEAPONTYPE wpType;

    [SerializeField] Sprite mainSprite;
    [SerializeField] Sprite subSprite;
    [SerializeField] Sprite supportSprite;

	// Use this for initialization
	void Start () 
    {
        UpdateType(wpType);
	}
	
    public void UpdateType( WEAPONTYPE type)
    {
        wpType = type;

        switch (wpType)
        {
            case WEAPONTYPE.MAIN:
                this.GetComponent<Image>().sprite = mainSprite;
                break;
            case WEAPONTYPE.SUB:
                this.GetComponent<Image>().sprite = subSprite;
                break;
            case WEAPONTYPE.SUPPORT:
                this.GetComponent<Image>().sprite = supportSprite;
                break;
        }
    }

}
