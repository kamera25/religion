﻿using UnityEngine;
using System.Collections;

public class TrainingLobyController : MonoBehaviour 
{
    STAGE stage;

    void ChoseTown()
    {
        stage = STAGE.TOWN;
        EnterRoom();
    }
    
    void ChoseForest()
    {
        stage = STAGE.FOREST;
        EnterRoom();
    }

    void EnterRoom()
    {
        if (TopMenuBehavior.Mode != TOPMENUMODE.TRAINING)
        {
            return;
        }

        PutUnitControl.stageMode = STAGEMODE.TRAINING;

        switch (stage)
        {
            case STAGE.TOWN:
                Application.LoadLevel("stage");
                return;
            case STAGE.FOREST:
                Application.LoadLevel("Forest");
                return;
        }

    }
}
