﻿using UnityEngine;
using System.Collections;

public class PoseMenu : Photon.MonoBehaviour 
{
    private bool putMenu = false;

	void  Update ()
	{
		
		if( Input.GetButtonDown("ESC") )
		{
            if( PhotonNetwork.offlineMode)
            {
                switch(PutUnitControl.stageMode)
                {
                    case STAGEMODE.NONE:
                        MissionModeMenuProc();
                        break;
                    case STAGEMODE.TRAINING:
                        TrainingModeMenuProc();
                        break;
                }

            }
            else // When online mode.
            {
                OnlineModeMenuProc();
            }


            putMenu = !putMenu;
		}
	}

    void TrainingModeMenuProc()
    {
        if( !putMenu)
        {
            Time.timeScale = 0F;
            Application.LoadLevelAdditive("TrainingPauseMenu");
        }
        else
        {
            Time.timeScale = 1F;
        }
    }

    void MissionModeMenuProc()
    {
        if( !putMenu)
        {
            Time.timeScale = 0F;
            Application.LoadLevelAdditive("BackpackMenu");
        }
        else
        {
            Time.timeScale = 1F;
        }
    }

    void OnlineModeMenuProc()
    {
        if( !putMenu)
        {
            // NetwokMenuが出ていないかチェック
            if( GameObject.Find("NetworkMenu") != null)
            {
                return;// NetworkMenuが表示されている間は,何もしない.
            }

            Application.LoadLevelAdditive("OnlinePauseMenu");
        }
        else
        {
            Destroy( GameObject.Find("PoseMenuUI"));
        }
    }

}