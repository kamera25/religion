﻿using UnityEngine;
using System.Collections;

public class TriggerSend : MonoBehaviour 
{
    public SwordControl sordCtr;

    // When hit gameobject
    void OnTriggerEnter( Collider other)
    {
        sordCtr.HitTriggerProc(other.gameObject);
    }
}
