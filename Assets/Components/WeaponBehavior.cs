﻿using UnityEngine;
using System.Collections;

public class WeaponBehavior : MonoBehaviour 
{
    private EmisionBulletCase bulletCtr;
    private EmisionMagazine magazineCtr;
    private AudioSource gunShotSE;
    private MuzzleFlashControl muzzleCtr;

	// Use this for initialization
	void Awake () 
    {
        Transform obj;

        obj = this.transform.FindChild("EmissionPoint");
        if( obj != null) bulletCtr = obj.GetComponent<EmisionBulletCase>();

        obj = this.transform.FindChild("MagazinePoint");
        if( obj != null) magazineCtr = obj.GetComponent<EmisionMagazine>();

        Transform muzzleObj = this.transform.FindChild("MuzzleFlashPoint");
        if (muzzleObj != null)
        {
            muzzleCtr = muzzleObj.GetComponent<MuzzleFlashControl>();
            gunShotSE = muzzleObj.GetComponent<AudioSource>();
        }
	}
	
    public void EmmisionCase( Quaternion Q)
    {
        if (bulletCtr == null)
        {
            Debug.LogWarning("No regist EmmisionCase.");
            return;
        }

        bulletCtr.EmmsionCase(Q);
    }

    public void EmmisionMagazine( Quaternion Q)
    {
        if (magazineCtr == null)
        {
            Debug.LogWarning("No regist EmmisionMagazine.");
            return;
        }
        magazineCtr.EmmsionMagazine(Q);
    }

    public void PlayGunShotSE()
    {
        gunShotSE.Play();
    }

    public void PutMuzzleFlash()
    {
        muzzleCtr.PutMuzzleFlash();
    }
	
}
