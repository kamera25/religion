﻿using UnityEngine;
using System.Collections;

public abstract class CharactorYoke : Actor
{
    protected CharacterController charaCtl;
    protected Animator aniCtl;
    protected StatusControl stateCtl;
    protected PoseModeControl poseModeCtl;

    public  STATE state = STATE.AIR;
    public int countGoUpLadder = 0;
    private Vector3 moveVec = Vector3.zero;

    public float AvoidStunVal = 300F;
    protected float speed = 20.0f;
    protected float ladderSpeed = 7.0f;
    public float avoidanceSpeed = 45F;
    private float moveMotionDump = 4F;
    private float rotatespeed = 20.5f;
    private float gravity = 40.0f;
    private float avoidTime = 0F;
    protected bool isAir;
    protected bool isAirBefore = false;

    private int playerLayer;

    protected virtual void Start()
    {
        playerLayer = (1 << LayerMask.NameToLayer("Player Layer"));
    }

    public enum STATE
    {
        NORMAL = 0,
        RUN,
        AIR,
        BACKAVOID,
        RIGHTAVOID,
        LEFTAVOID,
        ONLADDER,
        FINISHLADDER,
        SQUAT
    }

    protected void IsDeadProcess()
    {
        if (stateCtl.isDead)
        {
            state = STATE.NORMAL;
            aniCtl.speed = 1.0F;
        }
    }

    protected void ClimbLadder( float upSpeed)
    {
        Vector3 moveDirection = new Vector3( 0, upSpeed, 0);
        moveDirection *= ladderSpeed;
        charaCtl.Move( moveDirection * Time.deltaTime);

        // Animation

        aniCtl.SetBool("useLadder", true);
        if ( upSpeed < 0.1F)
        {
            aniCtl.speed = 0.0F;
        } 
        else
        {
            aniCtl.speed = 1.0F;
        }
        DisableLayerWeight();
       
        return;
    }

    protected void FinishClimbLadder()
    {
        MoveWithNormalize( new Vector3( 0, 0.7F, 1F), ladderSpeed, 0);

        countGoUpLadder--;
        if (countGoUpLadder <= 0)
        {
            state = STATE.NORMAL;
            DisableUseLadder();
            aniCtl.SetBool( "finishLadder", false);
        }

        return;
    }

    protected void DisableUseLadder()
    {
        state = STATE.NORMAL;
        poseModeCtl.EnablecanChange();

        aniCtl.SetBool("useLadder", false);
        EnableLayerWeight();
        aniCtl.speed = 1.0F;

        return;
    }

    protected void RunAvoidance ( float right, float forward)
    {

        // when first transision.
        if (avoidTime == 0F)
        {
            if ( !stateCtl.isLeaveStamina(AvoidStunVal))
            {
                avoidTime = 0F;
                state = STATE.AIR;
                return;
            }

            stateCtl.StunReciver(AvoidStunVal);// decrease Stamina.
            aniCtl.SetTrigger("isAvoid");
        }

        float damp = 1F - avoidTime;
        if (damp < 0) damp = 0F;

        Vector3 move = new Vector3(right * avoidanceSpeed * damp, 6f * damp, forward * avoidanceSpeed * damp);
        MoveWithGravity( transform.TransformDirection( move));

        if (0.4F < avoidTime)
        {
            avoidTime = 0F;
            state = STATE.AIR;
            return;
        }

        avoidTime += Time.deltaTime;

        return;
    }

    protected bool isRoll()
    {
        int layermask = ~playerLayer;
        return !Physics.CheckSphere( this.transform.position, 3F, layermask);
    }

    protected void isAirUpdate()
    {
        //isAir = !Physics.Linecast( this.transform.position + Vector3.up * 3F, this.transform.position + Vector3.down * 1.8F);
        int layermask = ~playerLayer;
        isAirBefore = isAir;
        isAir = !Physics.CheckSphere( this.transform.position, 2F, layermask);
    }

    void MoveWithNormalize( Vector3 moveDirection, float speed, float bias)
    {
        moveDirection = DirectionWithNomalize( moveDirection, speed, bias, false);
        charaCtl.Move( moveDirection * Time.deltaTime);
        ApplyMoveAnimation(moveDirection);
    }

    protected void MoveWithGravity( Vector3 moveDirection)
    {
           
        moveDirection.y -= gravity * Time.deltaTime;
       
        if (!isAir)
        {
            moveDirection.y = -20;
        } 
        else if( isAir && !isAirBefore && state != STATE.AIR)
        {
            moveDirection.y = -gravity * Time.deltaTime;
        }

        charaCtl.Move( moveDirection * Time.deltaTime);

        moveVec = moveDirection;

        return;
    }

    protected void ApplyGravityAndResistAir()
    {
        float tempY = moveVec.y;
        moveVec = Vector3.Lerp( moveVec, Vector3.zero, Time.deltaTime * 0.7f);
        moveVec.y = tempY;

        MoveWithGravity( moveVec);

        return;
    }

    void ApplyMoveAnimation( Vector3 vec)
    {
        float moveH = Mathf.Lerp(aniCtl.GetFloat("Horizontal"), vec.x, Time.deltaTime * moveMotionDump);
        aniCtl.SetFloat( "Horizontal", moveH);

        float moveV = Mathf.Lerp(aniCtl.GetFloat("Vertical"), vec.z, Time.deltaTime * moveMotionDump);
        aniCtl.SetFloat( "Vertical", moveV);

        return;
    }

    protected Vector3 DirectionWithNomalize( Vector3 moveDirection, float speed, float bias, bool isWalk)
    {
        Vector3 move = moveDirection.normalized;

        ApplyMoveAnimation( move);

        if( isWalk) move.x = move.x * 0.7F;// side walk is low speed.
        moveDirection = transform.TransformDirection( move);
        moveDirection *= (speed + bias);

        return moveDirection;
    }



    protected bool ClimbStartLadder( Transform point)
    {
        float ladderAngle = Vector3.Angle(this.transform.forward, point.forward);
        if ( ladderAngle > 40F || state != STATE.NORMAL)
        {
            return false;
        }

        this.transform.position = point.position;
        this.transform.rotation = point.rotation;
        state = STATE.ONLADDER;

        return true;
    }

    protected void LookAtBody( Vector3 lookPos, float ignoreLimit)
    {
        lookPos.y = this.transform.position.y;
        
        Vector3 target = lookPos - this.transform.position;
        
        Quaternion rotation = Quaternion.LookRotation(target);
        if (ignoreLimit < Quaternion.Angle(rotation, this.transform.rotation))
        {
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, rotation, Time.deltaTime * rotatespeed);
        }

        return;
    }

    protected void EnableLayerWeight()
    {
        aniCtl.SetLayerWeight( 1, 1F);
        aniCtl.SetLayerWeight( 2, 1F);
    }
    
    protected void DisableLayerWeight()
    {
        aniCtl.SetLayerWeight( 1, 0F);
        aniCtl.SetLayerWeight( 2, 0F);
    }
}
