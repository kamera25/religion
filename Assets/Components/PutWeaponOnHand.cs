﻿using UnityEngine;
using System.Collections;

public class PutWeaponOnHand : Photon.MonoBehaviour 
{
    public Transform wpLeftHand;
    public Transform wpRightWrist;
    public bool isPlayer = true;

    private int wpInstanceID;
    private WeaponDataProcess Wpd;
    private PoseModeControl poseCtr;

    private WeaponData wpData = null;

    void Start()
    {
        GameObject controller = GameObject.FindWithTag("GameController");
        Wpd = controller.GetComponent<WeaponDataProcess>();
        poseCtr = controller.GetComponent<PoseModeControl>();

        // Get is this created or not by me?
        bool isMine = this.transform.parent.GetComponent<PhotonView>().photonView.isMine;

        if ( !PhotonNetwork.offlineMode && !isMine)
        {
            isPlayer = false;
        }
    }

    void  LateUpdate ()
    {
        PutWeaponPosition();
    }

    void PutWeaponPosition()
    {
        wpData = Wpd.GetNowUseWeaponFromID( this.gameObject.GetInstanceID(), isPlayer);
        if (wpData == null)
        {
            return;
        }

        Transform model = wpData.model.transform;

        model.position = wpRightWrist.position;
        RotateWeaponProc(model);
    }

    void RotateWeaponProc( Transform model)
    {
        if (poseCtr.PoseMode == POSE.SUBJECT)
        { // be rotated to Vector3.foward.
            model.rotation = wpRightWrist.rotation;
            
            // Erase a strage rotation.
            Vector3 rot = model.localEulerAngles;
            rot.z = 0F;
            model.localEulerAngles = rot;
        } 
        else
        { // be rotated nice to lock at.
            model.rotation = wpRightWrist.rotation;
        }
    }
}
