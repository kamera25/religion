using UnityEngine;
using System.Collections;

public class EnemyHumanControl : CharactorYoke 
{
    private AIBehavior ai;
    public GameObject Marionettea;
    private NavMeshAgent navMesh;
    private EnemyWeaponControl wpCtrl;

	// Use this for initialization
	protected override void Start () 
    {
        base.Start();

        ai = this.GetComponent<AIBehavior>();
        navMesh = this.GetComponent<NavMeshAgent>();
        aniCtl = Marionettea.GetComponent<Animator>();
        charaCtl = this.GetComponent<CharacterController>();
        stateCtl = this.GetComponent<StatusControl>();

        Transform weaponHead = this.transform.FindChild("Weapon");
        if (weaponHead != null)
        {
            wpCtrl = weaponHead.GetComponent<EnemyWeaponControl>();
        } 
        else
        {
            Debug.Log( this.name + " : not set weaponHead.");
        }

        aniCtl.SetBool( "isPatrol", true);
	}
	
	// Update is called once per frame
	void Update () 
    {
        IsDeadProcess();

        if (ai.PeekImmediate() == EXPLICIT.WALK)
        {
            Vector3 move = DirectionWithNomalize( navMesh.destination, speed, 0F, true);
        }
        if (ai.PeekImmediate() == EXPLICIT.WEAPONATTACK)
        {
            wpCtrl.WeaponFire();
        }
        if (ai.PeekAmbient() == AMBIENT.ANNIHILATION)
        {
            aniCtl.SetBool( "isPatrol", false);
        }

        float runNeedStamina = 1F * Time.deltaTime;
        if (ai.PeekImmediate() == EXPLICIT.RUN && stateCtl.isLeaveStamina(runNeedStamina))
        {
            aniCtl.SetBool( "isDash", true);
            navMesh.speed = 25F;
            stateCtl.StunReciver(runNeedStamina);
        } 
        else
        {
            aniCtl.SetBool( "isDash", false);
            navMesh.speed = 7F;
        }
	}
}
