﻿using UnityEngine;
using System.Collections;

public class MainCameraBehavior : MonoBehaviour {

    private bool requestUpdate = false;
    private Vector3 requestPos;
    private Vector3 requestLookAtVec;
    private bool onDistDump = false;
    private bool onPosDump = false;
    public float dump;

    const int QUALITY_LOW = 0;
    const int QUALITY_HIGH = 2;

	// Use this for initialization
	void Start () 
    {
        InitializeRequest();

        int quality = QualitySettings.GetQualityLevel();
        if (quality == QUALITY_LOW)
        {
            this.GetComponent<UnityStandardAssets.ImageEffects.BloomOptimized>().enabled = false;
        }
        if (quality == QUALITY_HIGH)
        {
            this.GetComponent<UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion>().enabled = true;
        }
	}

    void InitializeRequest()
    {
        requestPos = Vector3.zero;
        requestLookAtVec = Vector3.zero;
        requestUpdate = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
   
    void LateUpdate()
    {
        if (requestUpdate == false)
        {
            return;
        }

        if (requestPos != Vector3.zero)
        {
            if( onPosDump)
            {
                Vector3 pos = Vector3.Lerp( this.transform.position, requestPos, dump * Time.fixedDeltaTime);
                this.transform.position = pos;
            }
            else
            {
                this.transform.position = requestPos;
            }
        }

        if (requestLookAtVec != Vector3.zero)
        {
            if (onDistDump)
            {
                Vector3 lookAt = Vector3.Lerp( this.transform.localEulerAngles, requestLookAtVec, dump * Time.fixedDeltaTime);
                this.transform.LookAt(lookAt);
            } else
            {
                this.transform.LookAt(requestLookAtVec);
            }
        }

        InitializeRequest();
    }

    public void SetPos( Vector3 pos)
    {
        requestPos = pos;
        requestUpdate = true;
    }

    public void SetLookAtVec( Vector3 vec)
    {
        requestLookAtVec = vec;
        requestUpdate = true;
    }

    public void EnableDump()
    {
        onPosDump = true;
        //onDistDump = true;
    }

    public void DisableDump()
    {
        onPosDump = false;
        onDistDump = false;
    }


}
