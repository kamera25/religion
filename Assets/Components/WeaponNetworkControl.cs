﻿using UnityEngine;
using System.Collections;

public class WeaponNetworkControl : Photon.MonoBehaviour 
{

	private Vector3 nowPlayerPos = Vector3.zero;
	private Quaternion nowPlayerRot = Quaternion.identity;

	// Update is called once per frame
	void Update () 
	{
		if( !photonView.isMine)
        {
			transform.position = Vector3.Lerp( transform.position, nowPlayerPos, Time.deltaTime * 5F);
			transform.rotation = Quaternion.Lerp( transform.rotation, nowPlayerRot, Time.deltaTime * 5F);
		}
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)    
	{
		if( stream.isWriting)// Caster.
		{
			stream.SendNext( transform.position);
			stream.SendNext( transform.rotation);
		}
		else// Reciver
		{
			nowPlayerPos = (Vector3)stream.ReceiveNext();
			nowPlayerRot = (Quaternion)stream.ReceiveNext();
		}
	}

}
