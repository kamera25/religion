﻿using UnityEngine;
using System.Collections;

public enum POSE
{
	THIED=0,
	SHOULDER,
	SUBJECT
}

public class PoseModeControl : MonoBehaviour 
{
	private ShoulderShotCamControl shoulderCameraScript;
	private ThirdShotCamControl thirdCameraScript;
    private ScopeShotCamera scopeCameraScript;

    [SerializeField] private GameObject sholderSprite;
    [SerializeField] private GameObject thirdSprite;
    [SerializeField] private GameObject subjectSprite;

    public POSE PoseMode { get; private set;}
    private bool canChange = true;

	private GameObject player;


	// Use this for initialization
	void Start () 
	{

        SetPlayerComponentAtPool();
        if (player == null)
        {
            this.enabled = false;
        }

        EnableThiedScript();
    }
	
	// Update is called once per frame
	void Update () 
	{

		// if Time is Stop... 
		if( Time.timeScale == 0) return;

		if( Input.GetButtonDown("Change view") && canChange)
		{
			PoseMode++;
			//PoseMode = (POSE)Mathf.Repeat( (int)PoseMode, 3);
            PoseMode = (POSE)( (int)PoseMode % 3);

			switch(PoseMode)
			{
			case POSE.THIED:
				EnableThiedScript();
				break;
			case POSE.SHOULDER:
				EnableShoulderScript();
				break;
			case POSE.SUBJECT:
				EnableScopeScript();
				break;				
			}	
			
		}
	}

	void EnableThiedScript()
	{
        if (player == null)
        {
            Debug.LogWarning("PoseModeControl : Not register player!!");
            return;
        }

		scopeCameraScript.DisableScopeCamera();
		thirdCameraScript.enabled = true;
      //  thirdCameraScript.SetCameraToModelRoot();

        subjectSprite.SetActive(false);
        thirdSprite.SetActive(true);
	}

	void EnableShoulderScript()
	{
		thirdCameraScript.enabled = false;
		shoulderCameraScript.enabled = true;
		shoulderCameraScript.ChangeCamFromThird();
        Camera.main.transform.parent = null;

        thirdSprite.SetActive(false);
        sholderSprite.SetActive(true);
	}

	void EnableScopeScript()
	{
		shoulderCameraScript.enabled = false;
		scopeCameraScript.EnableScopeCamera();
        Camera.main.transform.parent = null;

        sholderSprite.SetActive(false);
        subjectSprite.SetActive(true);
	}

    // Pose change to third parson by compulsion.
    public void ForceSetThiedPose()
    {

        switch (PoseMode)
        {
            case POSE.SHOULDER:
                EnableScopeScript();
                EnableThiedScript();
                break;
            case POSE.SUBJECT:
            case POSE.THIED:
                EnableThiedScript();
                break;
        }

        PoseMode = POSE.THIED;
    }

    private void DisableAllCamera()
    {
        thirdCameraScript.enabled = false;
        shoulderCameraScript.enabled = false;
        scopeCameraScript.DisableScopeCamera();
    }

    public void EnablecanChange()
    {
        canChange = true;
        return;
    }

    public void DisablecanChange()
    {
        canChange = false;
        return;
    }

    public void RegisterPlayer( PlayerData data)
	{
		player = data.player;
		SetPlayerComponentAtPool();
        EnableThiedScript();
		this.enabled = true;
	}

    void SetPlayerComponentAtPool()
    {
        if (player == null)
        {
            player = GameObject.FindWithTag("Player");
            if( player == null)
            {
                return;
            }
        }

        shoulderCameraScript = player.GetComponent<ShoulderShotCamControl>();
        thirdCameraScript = player.GetComponent<ThirdShotCamControl>();
        scopeCameraScript = player.GetComponent<ScopeShotCamera>();
    }
}
