using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity_weapondata : ScriptableObject
{	
	public List<Sheet> sheets = new List<Sheet> ();

	[System.SerializableAttribute]
	public class Sheet
	{
		public string name = string.Empty;
		public List<Param> list = new List<Param>();
	}

	[System.SerializableAttribute]
	public class Param
	{
		
		public int id;
		public string name;
		public string itemkind;
		public string type;
		public float attack;
		public float minattack;
		public float rapid;
		public float range;
		public string recoil;
		public int ammo;
		public int magazine;
		public float charge_time;
		public string bullet_type;
		public string mag_type;
		public string equilibrium;
		public string note;
		public string brief;
		public int motion;
		public string Use_motion;
		public string reload_motion;
		public string equilibrium_motion;
	}
}

