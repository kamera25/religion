using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity_army : ScriptableObject
{	
	public List<Sheet> sheets = new List<Sheet> ();

	[System.SerializableAttribute]
	public class Sheet
	{
		public string name = string.Empty;
		public List<Param> list = new List<Param>();
	}

	[System.SerializableAttribute]
	public class Param
	{
		
		public int id;
		public string name;
		public string ja_name;
		public float stamina;
		public float maxhp;
		public float attack;
		public int skillslot;
		public string brief;
	}
}

