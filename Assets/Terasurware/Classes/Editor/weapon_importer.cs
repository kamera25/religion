using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

public class weapon_importer : AssetPostprocessor {
	private static readonly string filePath = "Assets/system/Resources/data/Weapon/weapon.xls";
	private static readonly string exportPath = "Assets/system/Resources/data/Weapon/weapon.asset";
	private static readonly string[] sheetNames = { "weapondata", };
	
	static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
	{
		foreach (string asset in importedAssets) {
			if (!filePath.Equals (asset))
				continue;
				
			Entity_weapondata data = (Entity_weapondata)AssetDatabase.LoadAssetAtPath (exportPath, typeof(Entity_weapondata));
			if (data == null) {
				data = ScriptableObject.CreateInstance<Entity_weapondata> ();
				AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath);
				data.hideFlags = HideFlags.NotEditable;
			}
			
			data.sheets.Clear ();
			using (FileStream stream = File.Open (filePath, FileMode.Open, FileAccess.Read)) {
				IWorkbook book = new HSSFWorkbook (stream);
				
				foreach(string sheetName in sheetNames) {
					ISheet sheet = book.GetSheet(sheetName);
					if( sheet == null ) {
						Debug.LogError("[QuestData] sheet not found:" + sheetName);
						continue;
					}

					Entity_weapondata.Sheet s = new Entity_weapondata.Sheet ();
					s.name = sheetName;
				
					for (int i=1; i< sheet.LastRowNum; i++) {
						IRow row = sheet.GetRow (i);
						ICell cell = null;
						
						Entity_weapondata.Param p = new Entity_weapondata.Param ();
						
					cell = row.GetCell(0); p.id = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(1); p.name = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(2); p.itemkind = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(3); p.type = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(4); p.attack = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(5); p.minattack = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(6); p.rapid = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(7); p.range = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(8); p.recoil = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(9); p.ammo = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(10); p.magazine = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(11); p.charge_time = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(12); p.bullet_type = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(13); p.mag_type = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(14); p.equilibrium = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(15); p.note = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(16); p.brief = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(17); p.motion = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(18); p.Use_motion = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(19); p.reload_motion = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(20); p.equilibrium_motion = (cell == null ? "" : cell.StringCellValue);
						s.list.Add (p);
					}
					data.sheets.Add(s);
				}
			}

			ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath, typeof(ScriptableObject)) as ScriptableObject;
			EditorUtility.SetDirty (obj);
		}
	}
}
