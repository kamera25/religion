using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

public class Ammo_importer : AssetPostprocessor {
	private static readonly string filePath = "Assets/system/Resources/data/Ammo/Ammo.xls";
	private static readonly string exportPath = "Assets/system/Resources/data/Ammo/Ammo.asset";
	private static readonly string[] sheetNames = { "ammo", };
	
	static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
	{
		foreach (string asset in importedAssets) {
			if (!filePath.Equals (asset))
				continue;
				
			Entity_ammo data = (Entity_ammo)AssetDatabase.LoadAssetAtPath (exportPath, typeof(Entity_ammo));
			if (data == null) {
				data = ScriptableObject.CreateInstance<Entity_ammo> ();
				AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath);
				data.hideFlags = HideFlags.NotEditable;
			}
			
			data.sheets.Clear ();
			using (FileStream stream = File.Open (filePath, FileMode.Open, FileAccess.Read)) {
				IWorkbook book = new HSSFWorkbook (stream);
				
				foreach(string sheetName in sheetNames) {
					ISheet sheet = book.GetSheet(sheetName);
					if( sheet == null ) {
						Debug.LogError("[QuestData] sheet not found:" + sheetName);
						continue;
					}

					Entity_ammo.Sheet s = new Entity_ammo.Sheet ();
					s.name = sheetName;
				
					for (int i=1; i< sheet.LastRowNum; i++) {
						IRow row = sheet.GetRow (i);
						ICell cell = null;
						
						Entity_ammo.Param p = new Entity_ammo.Param ();
						
					cell = row.GetCell(0); p.id = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(1); p.name = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(2); p.ammo = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(3); p.mag = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(4); p.bullet_type = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(5); p.brief = (cell == null ? "" : cell.StringCellValue);
						s.list.Add (p);
					}
					data.sheets.Add(s);
				}
			}

			ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath, typeof(ScriptableObject)) as ScriptableObject;
			EditorUtility.SetDirty (obj);
		}
	}
}
