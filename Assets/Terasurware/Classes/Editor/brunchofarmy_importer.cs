using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

public class brunchofarmy_importer : AssetPostprocessor {
	private static readonly string filePath = "Assets/system/Resources/data/Army/brunchofarmy.xls";
	private static readonly string exportPath = "Assets/system/Resources/data/Army/brunchofarmy.asset";
	private static readonly string[] sheetNames = { "army", };
	
	static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
	{
		foreach (string asset in importedAssets) {
			if (!filePath.Equals (asset))
				continue;
				
			Entity_army data = (Entity_army)AssetDatabase.LoadAssetAtPath (exportPath, typeof(Entity_army));
			if (data == null) {
				data = ScriptableObject.CreateInstance<Entity_army> ();
				AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath);
				data.hideFlags = HideFlags.NotEditable;
			}
			
			data.sheets.Clear ();
			using (FileStream stream = File.Open (filePath, FileMode.Open, FileAccess.Read)) {
				IWorkbook book = new HSSFWorkbook (stream);
				
				foreach(string sheetName in sheetNames) {
					ISheet sheet = book.GetSheet(sheetName);
					if( sheet == null ) {
						Debug.LogError("[QuestData] sheet not found:" + sheetName);
						continue;
					}

					Entity_army.Sheet s = new Entity_army.Sheet ();
					s.name = sheetName;
				
					for (int i=1; i< sheet.LastRowNum; i++) {
						IRow row = sheet.GetRow (i);
						ICell cell = null;
						
						Entity_army.Param p = new Entity_army.Param ();
						
					cell = row.GetCell(0); p.id = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(1); p.name = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(2); p.ja_name = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(3); p.stamina = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(4); p.maxhp = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(5); p.attack = (float)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(6); p.skillslot = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(7); p.brief = (cell == null ? "" : cell.StringCellValue);
						s.list.Add (p);
					}
					data.sheets.Add(s);
				}
			}

			ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath, typeof(ScriptableObject)) as ScriptableObject;
			EditorUtility.SetDirty (obj);
		}
	}
}
