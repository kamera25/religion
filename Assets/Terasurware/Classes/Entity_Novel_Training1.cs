using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity_Novel_Training1 : ScriptableObject
{	
	public List<Sheet> sheets = new List<Sheet> ();

	[System.SerializableAttribute]
	public class Sheet
	{
		public string name = string.Empty;
		public List<Param> list = new List<Param>();
	}

	[System.SerializableAttribute]
	public class Param
	{
		
		public double line;
		public string name;
		public string words;
		public string face;
	}
}

