%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Upbody
  m_Mask: 00000000000000000000000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: 115.!Root
    m_Weight: 1
  - m_Path: 115.!Root/0.!joint_Master
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/15.!joint_Neck
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/15.!joint_Neck/16.joint_Head
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/21.!joint_RightArmTwist
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/21.!joint_RightArmTwist/25.joint_migihijihojo
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/34.joint_RightThumb0
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/38.joint_RightPinky1
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/38.joint_RightPinky1/39.joint_RightPinky2
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/38.joint_RightPinky1/39.joint_RightPinky2/40.joint_RightPinky3
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/42.joint_RightRing1
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/42.joint_RightRing1/43.joint_RightRing2
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/42.joint_RightRing1/43.joint_RightRing2/44.joint_RightRing3
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/46.joint_RightFingers1
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/46.joint_RightFingers1/47.joint_RightFingers2
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/46.joint_RightFingers1/47.joint_RightFingers2/48.joint_RightFingers3
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/50.joint_RightIndex1
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/50.joint_RightIndex1/51.joint_RightIndex2
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/50.joint_RightIndex1/51.joint_RightIndex2/52.joint_RightIndex3
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/117.!joint_RightThumb0M
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/117.!joint_RightThumb0M/35.joint_RightThumb1
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/18.joint_RightShoulder/20.joint_RightArm/26.joint_RightElbow/31.joint_RightWrist/117.!joint_RightThumb0M/35.joint_RightThumb1/36.joint_RightThumb2
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/58.!joint_LeftArmTwist
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/58.!joint_LeftArmTwist/62.joint_hidarihijihojo
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/71.joint_LeftThumb0
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/75.joint_LeftPinky1
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/75.joint_LeftPinky1/76.joint_LeftPinky2
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/75.joint_LeftPinky1/76.joint_LeftPinky2/77.joint_LeftPinky3
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/79.joint_LeftRing1
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/79.joint_LeftRing1/80.joint_LeftRing2
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/79.joint_LeftRing1/80.joint_LeftRing2/81.joint_LeftRing3
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/83.joint_LeftFingers1
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/83.joint_LeftFingers1/84.joint_LeftFingers2
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/83.joint_LeftFingers1/84.joint_LeftFingers2/85.joint_LeftFingers3
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/87.joint_LeftIndex1
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/87.joint_LeftIndex1/88.joint_LeftIndex2
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/87.joint_LeftIndex1/88.joint_LeftIndex2/89.joint_LeftIndex3
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/116.!joint_LeftThumb0M
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/116.!joint_LeftThumb0M/72.joint_LeftThumb1
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/12.joint_Torso/13.joint_Torso2/55.!joint_LeftShoulder/57.joint_LeftArm/63.joint_LeftElbow/68.joint_LeftWrist/116.!joint_LeftThumb0M/72.joint_LeftThumb1/73.joint_LeftThumb2
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/96.joint_RightHip
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/96.joint_RightHip/97.joint_RightKnee
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/96.joint_RightHip/97.joint_RightKnee/98.joint_RightFoot
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/100.joint_LeftHip
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/100.joint_LeftHip/101.joint_LeftKnee
    m_Weight: 1
  - m_Path: 115.!Root/14.joint_HipMaster/100.joint_LeftHip/101.joint_LeftKnee/102.joint_LeftFoot
    m_Weight: 1
  - m_Path: U_Char
    m_Weight: 1
